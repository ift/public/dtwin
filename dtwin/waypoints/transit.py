# This file is part of dtwin.
#
# dtwin is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# dtwin is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Copyright (C) 2021 Max Planck Society
#
# dtwin is being developed at the Max Planck Institute for Astrophysics

from .waypoint_base import WaypointBase
from ..events.time import Time
from ..utils import checktype, dprint
from ..random.observable_base import ObservableBase

import networkx as nx

_time = Time()

class Transit(WaypointBase):
    ''' Transits containers.'''
    def __init__(self,name,next_queue=None,prev_queue=None,\
                 observables=None,begins_area=None,ends_area=None,\
                 interfaces=None):
        ''' Just transit the container to the next Collection.
        '''
        super().__init__()

        self.name = name
        self.next = next_queue
        self.prev = prev_queue
        self.begins = begins_area
        self.ends = ends_area
        self.interfaces = interfaces
        self.observables = observables

    def condition(self,container=None):
        if len(self.next) > 0:
            return self.next[0].check_capacity()
        else:
            return True

    async def condition_false(self,container=None):
        pass

    def decide(self,container=None):
        if container is None:
            return []
        else:
            return [0]*len(container)

    async def action(self,container):
        # container is a list of (inqueue_idx, container_object)
        # it should have a length of 1 for transit waypoints
        queue_idx, c = container[0]

        observables = await self.sample(container)
        observables = observables[0]

        outqueues = self.decide(container)

        await self.report({'type':'application',
                           'location':self.name,
                           'container':c.name,
                           'timestamp':_time.now,
                           'observables':observables}),

        return outqueues, container
