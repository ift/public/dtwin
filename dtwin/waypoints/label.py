# This file is part of dtwin.
#
# dtwin is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# dtwin is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Copyright (C) 2021 Max Planck Society
#
# dtwin is being developed at the Max Planck Institute for Astrophysics

from .waypoint_base import WaypointBase
from ..events.time import Time
from ..utils import checktype, dprint

import networkx as nx

_time = Time()

class Label(WaypointBase):
    ''' Waypoint that adds a label depending on other observables '''
    def __init__(self,name,model,next_queue=None,prev_queue=None,\
                 observables=None,begins_area=None,ends_area=None,\
                 interfaces=None,destroy=False,report_all=False,\
                 ):
        ''' Create Containers according to a distribution.
        '''
        super().__init__()

        self.name = name
        self.next = next_queue
        self.prev = prev_queue
        self.begins = begins_area
        self.ends = ends_area
        self.interfaces = interfaces
        self.observables = model
        self._destroy = destroy
        self._report_all = report_all

    #@property
    #def model(self):
    #    return self._model

    #@model.setter
    #def model(self,model):
    #    from ..random.observable_base import ObservableBase
    #    self._model = checktype(model,ObservableBase,'model') 
    #    model.parent = self

    def decide(self,container=None):
        return 0

    def condition(self,container=None):
        if len(self.next) > 0:
            return self.next[0].check_capacity()
        else:
            return True

    async def condition_false(self,container=None):
        pass

    async def action(self,container):
        # container is a list of (queue_idx, container_object)
        # it should have a length of 1 for this type of waypoint
        outqueues = []
        out_container = []
        for queue_idx, c in container:

            observables = await self.sample([(queue_idx,c)])
            observables = observables[0]

            if self._report_all:
                # report all observables associated with the container
                await self.report({'type':'application',
                                  'location':self.name,
                                  'container':c.name,
                                  'timestamp':_time.now,
                                  'observables':c.observables})
            else:
                # only report observables applied here
                await self.report({'type':'application',
                                   'location':self.name,
                                   'container':c.name,
                                   'timestamp':_time.now,
                                   'observables':observables})

        # FIXME: we need to delete containers and references to them
        #        otherwise there will be memory leaks!

        if self._destroy or not self.next:
            # report movement - this will not be handled in the base
            # execute method, as there are no containers returned here
            msg_dic = {'type':'movement',
                       'container':c.name,
                       'waypoint':self.name,
                       'timestamp':_time.now}
            await self.report(msg_dic)

            # report container deletion
            msg_dic = {'type':'deletion',\
                       'container':c.name,\
                       'waypoint':self.name,\
                       'timestamp':_time.now,\
                      }
            await self.report(msg_dic)

            # no outgoing queues and no outgoing containers
            return [], []
        else:
            return self.decide(container), container
