# This file is part of dtwin.
#
# dtwin is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# dtwin is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Copyright (C) 2021 Max Planck Society
#
# dtwin is being developed at the Max Planck Institute for Astrophysics

from .waypoint_base import WaypointBase
from ..events.temporalevent import TemporalEvent
from ..events.conditionalevent import ConditionalEvent
from ..events.eventqueue import EventQueue
from ..events.time import Time
from ..random.constant import Constant
from ..utils import dprint, checktype

_eventqueue = EventQueue()
_time = Time()

class RandomCreation(WaypointBase):
    ''' Waypoint that creates Containers. '''
    def __init__(self,name,time_dist,next_queue=None,observables=None,\
                 begins_area=None,interfaces=None,timestamp_visible=True,\
                 decision_function=lambda q,c: 0,class_observable=None):
        ''' Create Containers according to a distribution.
        '''
        super().__init__()
        # the distribution of time differences between creation events
        from ..distributions.distribution_base import DistributionBase
        if not isinstance(time_dist,DistributionBase):
            raise TypeError()
        self._distribution = time_dist

        self.decision_function = decision_function

        self.name = name
        self.next = next_queue
        # no previous queue

        if begins_area is None:
            self.begins = set()
        else:
            self.begins = begins_area
        self.ends = set() # no areas are supposed to end here

        self.interfaces = interfaces

        self.observables = observables
        self.class_observable = class_observable
    
    @property
    def class_observable(self):
        return self._class_observable

    @class_observable.setter
    def class_observable(self,value):
        from ..random.observable_base import ObservableBase

        class_feat = checktype(value,(str,type(None),ObservableBase),\
                               'class_observable',allow_iterable=False)

        if isinstance(class_feat,type(None)):
            class_feat = ''
        if isinstance(class_feat,str):
            class_feat = Constant(f'{self.name}_class',\
                                  value=class_feat, parent=self)

        self._class_observable = class_feat
    
    def condition(self,container=None):
        return self.next[0].check_capacity()

    def decide(self,container=None):
        if container is None:
            container = []
        outqueues = [self.decision_function(q,c) for q,c in container]
        return outqueues

    async def condition_false(self,container=None):
        msg_dic = {'timestamp':_time.now,\
                   'type':'warning',\
                   'location':self.name,\
                   'msg':'queue out of capacity'}
        await self.report(msg_dic)

    async def action(self,container=None):
        dprint(f'Creation {self.name}: method action called',level=10)

        # define class via distinct class observable
        #if self.class_observable:
        #    class_ = self.class_observable.sample()
        #else:
        #    class_ = None

        # create Container
        from ..containers.container import Container
        c = Container()
        c.class_ = await self.class_observable.sample()

        # report container creation
        msg_dic = {'type':'creation',\
                   'container':c.name,\
                   'class':c.class_,\
                   'waypoint':self.name,\
                   'timestamp':_time.now,\
                  }
        await self.report(msg_dic)

        # sample other observables, sample expects list of (inqueue,container)
        feats = await self.sample([(None,c)])
        feats = feats[0]

        # report observable application
        msg_dic = {'type':'application',\
                   'container':c.name,\
                   'location':self.name,\
                   'timestamp':_time.now,\
                   'observables':feats,\
                  }
        await self.report(msg_dic)

        # redefine container variable
        container = [(0,c)]

        # decide on outqueues
        outqueues = self.decide(container)

        dprint(f'Creation {self.name}: created {c}',level=5)

        for a in self.begins:
            #if self.timestamp is not None:
            #    c.apply_observables({f'{a.name}_begin':self.timestamp.sample()})
            await a.at_entry(c)

        #self.next[0].push(c)

        next_dt = self._distribution.sample()
        temp_event = TemporalEvent(time  = _time.now + next_dt,\
                                   action= self.execute)
        _eventqueue.push(temp_event)

        return outqueues, container
