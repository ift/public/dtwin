# This file is part of dtwin.
#
# dtwin is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# dtwin is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Copyright (C) 2021 Max Planck Society
#
# dtwin is being developed at the Max Planck Institute for Astrophysics

from .waypoint_base import WaypointBase
from ..utils import checktype, dprint
from ..events.time import Time

_time = Time()

class Stack(WaypointBase):
    ''' Waypoint that adds a observable depending on other observable '''
    def __init__(
            self,name,parent_queue=None,children_queue=None,n_children=None,\
            decision_function=lambda *x: [0],next_queue=None,\
            observables=None,begins_area=None,ends_area=None,\
            interfaces=None,\
            ):
        ''' Stacks Containers from one Collection onto one from another.
        '''
        super().__init__()

        self.name = name
        self.next = next_queue
        self.begins = begins_area
        self.ends = ends_area
        self.observables = observables
        self.interfaces = interfaces
        self._decision_fct = decision_function

        self._n_children = n_children
        self.children_queue = children_queue
        self.parent_queue   = parent_queue

    @property
    def parent_queue(self):
        return self._parent_queue

    @parent_queue.setter
    def parent_queue(self,new):
        from ..queues.abstract import Collection
        par = checktype(new,(Collection,type(None)),force_list=True)
        if new is not None:
            if not new in self.prev:
                self.prev += par
            self.parent_idx = self.prev.index(new)
        self._parent_queue = new

    @property
    def children_queue(self):
        return self._children_queue

    @children_queue.setter
    def children_queue(self,new):
        from ..queues.abstract import Collection
        ch = checktype(new,(Collection,type(None)),force_list=True)
        if new is not None:
            if not new in self.prev:
                self.prev += ch
            self.children_idx = self.prev.index(new)
        self._children_queue = new

    @property
    def n_children(self):
        return self._n_children

    @n_children.setter
    def n_children(self,new):
        self._n_children = checktype(new,(int,type(None)))

    def decide(self,inq_cont_list=None):
        ''' The decision relies on the parent container, only. 
        '''
        # inq_cont_list is a list of (queue,container)
        outqueues = self._decision_fct(inq_cont_list)
        return outqueues

    def condition(self,inq_cont_list=None):
        ''' Checks requirements for stacking children onto parents
            * there must be at least one container in the parent collection
            * there must be at least self.n_children in the children collection
            * the next queue must have capacity for the stacked container
        '''
        # check whether there are enough children 
        if len(self.children_queue) < self.n_children:
            return False

        # check whether there are enough parents
        if len(self.parent_queue) < 1:
            return False

        if inq_cont_list is None or len(inq_cont_list) == 0:
            # forecast the next popped container
            inq_cont_list = [(self.parent_idx,self.parent_queue.forecast(n=1))]

        # get outgoing queue for list of containers
        outqueues = self.decide(inq_cont_list)

        # initialise a dictionary
        n_out   = {q:0 for q in range(len(self.next))}

        #print(f'Stack {self.name}: container is \n{inq_cont_list}')

        for idx,(inqueue,c) in enumerate(inq_cont_list):
            outq = outqueues[idx]
            n_out[outq] += 1
            
        for outq,n in n_out.items():
            if not self.next[outq].check_capacity(n):
                return False

        return True

    async def condition_false(self,inq_cont_list=None):
        pass

    async def action(self,inq_cont_list=None):
        ''' Action method, can be called either with or without an argument.
        If the container argument is given, it is assumed to be the parent and
        the children will be gather from self.children_queue.
        If not, the waypoint gathers both ingredients from self.parent_queue 
        and self.children_queue.
        '''
        if inq_cont_list:
            if len(inq_cont_list) > 1:
                # this waypoint can only handle one incoming container
                raise NotImplementedError
            p_inq, parent = inq_cont_list[0]
        else:
            p_inq, parent = self.parent_queue.pop()

        parent_tuples   = [(p_inq,parent)]

        children_tuples = [self.children_queue.pop()\
                           for i in range(self.n_children)]

        children = [ct[1] for ct in children_tuples]

        # apply observables to parent and children
        observables = await self.sample(parent_tuples+children_tuples)

        # add children to parent's list of children
        n_off = len(parent.children)
        parent.children.extend(children)

        outqueues = self.decide([(p_inq,parent)])

        # report stacking relation for each child
        for i,child in enumerate(children):
            await self.report({'type':'stacking',\
                               'parent':parent.name,\
                               'child':child.name,\
                               'stack':True,\
                               'location':self.name,\
                               'position':n_off + i,\
                               'timestamp':_time.now,\
                              })

        return outqueues, [(p_inq,parent)]
