# This file is part of dtwin.
#
# dtwin is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# dtwin is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Copyright (C) 2021 Max Planck Society
#
# dtwin is being developed at the Max Planck Institute for Astrophysics

from .waypoint_base import WaypointBase
from ..utils import checktype, dprint
from ..events.time import Time

_time = Time()

class Unstack(WaypointBase):
    ''' Unstacks stacked containers '''
    def __init__(self,name,decision_function,next_queue=None,\
                 observables=None,begins_area=None,ends_area=None,\
                 interfaces=None,prev_queue=None):
        ''' Create Containers according to a distribution.
        '''
        super().__init__()

        self.name = name
        self.next = next_queue
        self.prev = prev_queue
        self.begins = begins_area
        self.ends = ends_area
        self.observables = observables
        self.interfaces = interfaces
        self._decision_fct = decision_function

    def decide(self,container=None):
        # container is a list of (queue,container)
        outqueues = [self._decision_fct(q,c) for q,c in container]
        return outqueues

    def condition(self,container=None):
        # get outgoing queues for list of containers
        outqueues = self.decide(container)

        # initialise a dictionary
        n_out   = {q:0 for q in range(len(self.next))}

        for idx,(inqueue,c) in enumerate(container):
            outq = outqueues[idx]
            n_out[outq] += 1
            
        for outq,n in n_out.items():
            if not self.next[outq].check_capacity(n):
                return False

        return True

    async def condition_false(self,container=None):
        pass

    async def action(self,container):

        # sampling is applied to the parent container
        # BEFORE unstacking
        samples = await self.sample(container)

        inqueue = container[0][0]
        parent  = container[0][1]
        children = parent.children

        parent.children = []
        for c in children:
            c.parent = None

        inq_cont = [(inqueue,parent)] + [(inqueue,c) for c in children]
        outqueues = self.decide(inq_cont)

        for i,child in enumerate(children):
            await self.report({'type':'stacking',\
                               'parent':parent.name,\
                               'child':child.name,\
                               'stack':False,\
                               'location':self.name,\
                               'position':None,\
                               'timestamp':_time.now,\
                              })

        return outqueues, inq_cont
