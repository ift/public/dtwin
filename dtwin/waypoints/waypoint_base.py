# This file is part of dtwin.
#
# dtwin is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# dtwin is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Copyright (C) 2021 Max Planck Society
#
# dtwin is being developed at the Max Planck Institute for Astrophysics

from abc import abstractproperty, abstractmethod, ABCMeta
from ..containers.container_base import ContainerBase
from ..patterns import Multiplet
from ..utils import checktype, dprint
from ..events.time import Time

_time = Time()

import networkx as nx

class WaypointBase(Multiplet,metaclass=ABCMeta):
    ''' Base class for waypoints.
    '''

    # Indicates whether check_consistency has been called successfully
    _checked = False

    def __init__(self,timestamp=True):
        # initiate hidden attributes
        self._name = None
        self._next = []
        self._prev = []
        self._begins = set()
        self._ends = set()
        self._interfaces = set()
        self._subgraph = None
        self.observables = None
        self._abstract = False
        if timestamp:
            from ..random.timestamp import Timestamp
            self.timestamp = Timestamp(parent=self)

    @property
    def abstract(self):
        ''' Defines abstract objects.
        Abstract objects are not checked against consistency and
        take not part in the environment's graphs
        '''
        return self._abstract

    @abstract.setter
    def abstract(self,new):
        if not isinstance(new,bool):
            raise TypeError
        self._abstract = new

    @property
    def abstract(self):
        ''' Defines abstract waypoints.
        Abstract waypoints  are not checked against consistency and
        take not part in the environment's graphs
        '''
        return False

    @abstract.setter
    def abstract(self,new):
        if not isinstance(new,bool):
            raise TypeError
        self._abstract = new

    @property
    def name(self) -> str:
        return self._name

    @name.setter
    def name(self,new):
        checktype(new,str,'name')
        self._name = new
        if self.timestamp:
            self.timestamp.name = f'{self.name}_timestamp'

    @property
    def next(self) -> list:
        return self._next

    @next.setter
    def next(self,new):
        from ..queues.abstract import Collection
        new = checktype(new,(Collection,type(None)),'next',\
                allow_iterable=True,force_list=True)
        for n in new:
            if not self is n.prev:
                n.prev = self

    @property
    def prev(self):
        return self._prev

    @prev.setter
    def prev(self,new):
        from ..queues.abstract import Collection
        new = checktype(new,(Collection,type(None)),'prev',\
                allow_iterable=True,force_list=True)
        for n in new:
            if not self is n.next:
                n.next = self

    @property
    def observables(self):
        return self._observables

    @observables.setter
    def observables(self,value):
        from ..random.observable_base import ObservableBase
        obs = checktype(value,(type(None),ObservableBase),'observables',\
                        allow_iterable=True,force_set=True)
        for o in obs:
            o.parent = self
        self._observables = obs

    @property
    def begins(self):
        ''' List of Areas that begin at this waypoint.
        '''
        return self._begins

    @begins.setter
    def begins(self,area):
        ''' List of Areas that begin at this waypoint.
        '''
        from ..areas.area_base import AreaBase
        area = checktype(area,(AreaBase,type(None)),
                         force_set=True,
                         allow_iterable=True)
        self._begins = area

    @property
    def ends(self):
        ''' List of Areas that end at this waypoint.
        '''
        return self._ends

    @ends.setter
    def ends(self,area):
        ''' List of Areas that begin at this waypoint.
        '''
        from ..areas.area_base import AreaBase
        area = checktype(area,(AreaBase,type(None)),
                         force_set=True,
                         allow_iterable=True)
        self._ends = area

    @property
    def timestamp(self):
        return self._timestamp

    @timestamp.setter
    def timestamp(self,ts):
        from ..random.timestamp import Timestamp
        checktype(ts,(type(None),Timestamp))
        self._timestamp = ts
        if ts is not None and ts.parent is not self:
            ts.parent = self

    @abstractmethod
    def decide(self,inq_cont_list=None):
        ''' Decides the outgoing queues for a given list of (inqueue,container)

        '''
        pass

    async def report(self,dic):
        ''' Report events to all interfaces '''
        if 'location' not in dic.keys():
            dic['location'] = self.name
        for intf in self.interfaces:
            await intf.send(dic)

    @property
    def interfaces(self):
        ''' List of Interfaces to report events.
        '''
        return self._interfaces

    @interfaces.setter
    def interfaces(self,new):
        from ..interfaces.interface_base import InterfaceBase
        newi = checktype(new,(InterfaceBase,type(None)),
                         force_set=True,
                         allow_iterable=True)
        self._interfaces = newi

    @abstractmethod
    def condition(self,inq_cont_list=None):
        ''' Define a condition for processing containers.
        '''
        pass

    @abstractmethod
    def condition_false(self,inq_cont_list=None):
        '''Define what happens if the condition is not met.
        '''
        pass

    @abstractmethod
    async def action(self,inq_cont_list=None):
        ''' Define an action done by this waypoint.
        The action must include:
        * Sampling of observables (except timestamps)
        * Communication with interfaces (except 'movement')

        Parameters:
        -----------
        inq_cont_list: list of (inq:int,c:ContainerBase),
            `inq` refers to item of self._prev (which is a Collection)
            `cont` is the container being processed

        Returns:
        --------
        outq, inq_cont_list: tuple of lists
            `outq` must be a list of integers
            `inq_cont_list` must be a list of inqueue,container tuples
        '''
        pass

    def _create_subgraph(self):
        from ..graphs.data_model import DataModel
        graph = DataModel().graph
        subgraph = nx.subgraph(graph,self.observables)
        sorted_graph = nx.topological_sort(subgraph)
        # this returns a generator, which can be iterated over once, only
        # so cast it into a list as we will iterate over it repeatedly
        self._subgraph = list(sorted_graph)
        return self._subgraph

    async def sample(self,inq_cont_list=None,**kwargs):
        ''' Samples observables and applies them to the container
        '''
        #print(f'{self.name}: sampling...')
        #print(f' * containers: {inq_cont_list}')
        if self._subgraph is None:
            self._create_subgraph()

        # one dictionary for each container
        samples = [{}]*len(inq_cont_list) 
        for i in self._subgraph:
            # sample observables and apply them subsequently
            for idx,(inq,c) in enumerate(inq_cont_list):
                val = await i.sample(c,**kwargs)
                sample = {i.name:val}
                samples[idx].update(sample)
                c.apply_observables(sample)
                for idx,child in enumerate(c.children):
                    await self.sample([(inq,child)],position=idx)
        return samples

    async def execute(self,inq_cont_list=None):
        ''' Execute the waypoint's methods.

        This follows the following pattern:
        1) Check if the condition to pass the respective container(s) is True
        2) Sample timestamp if available and apply to container(s)
        3) Run ending area's outgoing action (at_exit method)
        4) Sample own observables and apply them to container(s)
        5) Run own action
        6) run next area's ingoing action (at_entry method)
        7) Put the container(s) in the appropriate outgoing queue(s)

        Parameters:
        -----------
        inq_cont_list: None or list of (int,ContainerBase)
            the integers denotes the index of the ingoing queue in self.prev
            the ContainerBase objects is the container being handled
            this will be passed to the methods condition and action
        '''

        n_prev = len(self.prev) if self.prev is not None else 0
        n_next = len(self.next) if self.next is not None else 0

        # inq_cont_list is supposed to be a 
        # list of indices of ingoing queues and container,
        # regardless of the input
        if inq_cont_list is None:
            inq_cont_list = []
        elif len(inq_cont_list) == 2\
             and isinstance(inq_cont_list[0],int)\
             and isinstance(inq_cont_list[1],ContainerBase):
            # if inq_cont_list is a tuple, put it in a list
            inq_cont_list = [inq_cont_list]

        # if ingoing containers do not satisfy this waypoint's conditions,
        # create a trigger checking for this condition in the future
        if not self.condition(inq_cont_list): 
            from ..events.conditionalevent import ConditionalEvent

            cond_event = ConditionalEvent(\
                condition=lambda x=inq_cont_list: self.condition(x),\
                action=self.execute,\
                push=True)

            # condition_false handles additional logic, if the waypoints
            # cannot be executed (e.g. error messages etc)
            await self.condition_false(inq_cont_list)

            return 1

        # if the conditions for handling the ingoing containers are 
        # satisfied, execute this waypoint's action
        else:
            if self.timestamp:
                ts = await self.timestamp.sample()

            #if self.ends:
            for area in self.ends:
                for _, cont in inq_cont_list:
                    tmp2 = await area.at_exit(cont,waypoint=self)

            # communication with interfaces must be included in action method
            outqueues, inq_cont_list = await self.action(inq_cont_list)

            for inq, c in inq_cont_list:
                msg_dic = {'type':'movement',
                           'container':c.name,
                           'timestamp':_time.now,
                           'waypoint':self.name}
                await self.report(msg_dic)

            for idx, outq in enumerate(outqueues):
                if outq is None:
                    continue
                inq, cont = inq_cont_list[idx]
                self.next[outq].push(cont)

            #if self.begins:
            for area in self.begins:
                for _, cont in inq_cont_list:
                    tmp2 = await area.at_entry(cont,waypoint=self)

            return 0

    @classmethod
    def check_consistency(cls,verbose=False):
        '''
        * check whether names are unique
        '''

        # get a list of all waypoints including subclasses
        waypoints = WaypointBase.get_instances(\
                include_subclasses=True,\
                flatten=True,\
                return_set=True)

        if verbose:
            print('| Creating subgraphs')
        for waypoint in waypoints:
            waypoint._create_subgraph()

        if len(set([w.name for w in waypoints])) != len(waypoints):
            from dtwin.errors import UniquenessError
            raise UniquenessError(f'Waypoint names are not unique')
        
        cls._checked = True
        return True
