# This file is part of dtwin.
#
# dtwin is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# dtwin is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Copyright (C) 2021 Max Planck Society
#
# dtwin is being developed at the Max Planck Institute for Astrophysics

from .container_base import ContainerBase

class Container(ContainerBase):
    ''' Implementation of ContainerBase
    '''

    def __init__(self, name=None, class_=None, parent=None,\
                 applied_observables={}, capacity=None,\
                 children_classes=None,parent_classes=None):
        '''Initialises a Container object.

        Parameters:
        -----------
        name : str or None, optional
            The name of the Container. Falls back to instance id, if not given
        class_ : None or str, optional
            The container's class
        parent : ContainerBase or None, optional
            The parent Container, if any.
        applied_observables : dict of Observable:value, optional
            A dictionary of observables initially applied to this Container.
        capacity : None or int, optional
            The container can only carry that many direct children.
        children_classes : None, str or iterable, optional
            The container only accepts those classes as children.
        parent_classes : None, str or iterable, optional
            The container only accepts those classes as parent.

        '''
        super(Container,self).__init__()

        self.class_ = class_
        if name is None:
            self.name = str(self._instance_id)
        else:
            self.name = name
        self.parent = parent
        self.capacity = capacity
        self.apply_observables(applied_observables)

        # contains a list/set of acceptable classes for children/parents
        self._accepted_children_classes = children_classes
        self._accepted_parent_classes = parent_classes

    @property
    def observables(self):
        return self._applied_observables

    def apply_observables(self,feat_dic):
        ''' Adds key:value pairs of feat_dic to container's observables.
            Does the same with its children, recursively.
        '''

        for c in self.children:
            c.apply_observables(feat_dic)

        self._applied_observables.update(feat_dic)
