# This file is part of dtwin.
#
# dtwin is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# dtwin is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Copyright (C) 2021 Max Planck Society
#
# dtwin is being developed at the Max Planck Institute for Astrophysics

from abc import ABCMeta, abstractmethod, abstractproperty
from ..patterns import Multiplet
from ..utils import checktype

###############################################################################
class ContainerBase(Multiplet):
    def __init__(self):
        ''' Hierarchical structure for moving and processed parts.

            Containers carry a dictionary with observable:value pairs. Those 
            can be accessed by waypoints / areas, e.g. for sampling.
            
            Containers can be stacked by altering the containers `children`.
            Each container can have a class attribute, as well as allowed 
            parent and children classes. 
            
        '''

        self.name       = None
        self.class_     = None
        self.capacity   = None
        self._accepted_children_classes = set()
        self._accepted_parent_classes  = set()
        self._parent     = None
        self._children   = []
        self._applied_observables = {}
    
    @property
    def name(self):
        return self._name

    @name.setter
    def name(self,new):
        checktype(new,(str,type(None)),'name')
        self._name = new

    @property
    def class_(self):
        return self._class

    @class_.setter
    def class_(self,new):
        checktype(new,(str,type(None)),'class_')
        self._class = new

    @property
    def capacity(self):
        return self._capacity

    @capacity.setter
    def capacity(self,new):
        checktype(new,(int,type(None)),'capacity')
        self._capacity = new

    @property
    def accepted_children(self):
        return self._accepted_children_classes

    @accepted_children.setter
    def accepted_children(self,new):
        new = checktype(new,(str,ContainerBase,type(None)),\
                'accepted_children',allow_iterable=True,force_set=True)
        self._accepted_child_classes = new

    @property
    def accepted_parents(self):
        return self._accepted_parent_classes

    @accepted_parents.setter
    def accepted_parents(self,new):
        new = checktype(new,(str,ContainerBase,type(None)),\
                'accepted_parents',allow_iterable=True,force_set=True)
        self._accepted_parent_classes = new

    @classmethod
    def _check_class(cls,child,parent):
        cap = child.accepted_parents 
        pac = parent.accepted_children
        if cap is not None and parent.class_ not in cap:
            raise TypeError('child does not accept parents of this type')
        if pac is not None and child.class_ not in pac:
            raise TypeError('parent does not accept children of this type')

    @property
    def children(self):
        '''Container(s) carried by this Container.'''
        return self._children

    @children.setter
    def children(self,value):
        val = checktype(value,(ContainerBase,type(None)),'children',\
                allow_iterable=True,force_list=True)
        for v in val:
            ContainerBase._check_class(v,self)
        n = len(val)
        if self.capacity is not None and n > self.capacity:
            raise CapacityError
        self._children = value

    @property
    def parent(self):
        '''The parent Container, if any.'''
        return self._parent

    @parent.setter
    def parent(self,value):
        if not self._parent == value:
            self._parent = checktype(value,(ContainerBase,type(None)),var='parent')
        if value is not None:
            ContainerBase._check_class(self,value)

        if value is not None and self not in self._parent.children:
            self._parent.children += [self]

    @abstractmethod
    def apply_observables(self,observable_dictionary):
        ''' Defines how observables are applied to the container.
        '''
        pass

    def __repr__(self):
        ''' Representation '''
        return f'<ContainerBase {self._instance_id}>'

    @classmethod
    def check_consistency(cls):
        import networkx as nx
        from ..errors import HierarchyError
        g = nx.DiGraph()
    
        cs = ContainerBase.get_instances(include_subclasses=True,\
                                         flatten=True,\
                                         return_set=True)
    
        for c in cs:
            if c.parent is not None:
                g.add_edge(c,c.parent)
    
        if not nx.is_directed_acyclic_graph(g):
            s = (f'Containers parent/children relationship must be acyclic')
            raise HierarchyError(s)

