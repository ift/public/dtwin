# This file is part of dtwin.
#
# dtwin is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# dtwin is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Copyright (C) 2021 Max Planck Society
#
# dtwin is being developed at the Max Planck Institute for Astrophysics

import numpy.random as rand
from abc import abstractmethod, abstractproperty, ABCMeta

class DistributionBase(metaclass=ABCMeta):
    '''Base class for distributions.
    '''

    @abstractmethod
    def sample(self,n=1):
        pass

    @abstractproperty
    def dtype(self):
        pass

class NumericalDistributionBase(DistributionBase):
    '''Convenience base class for numerical distributions.'''
    pass

class PositiveDistributionBase(NumericalDistributionBase):
    '''Convenience base class for positive numerical distributions.'''
    pass

class CategoricalDistributionBase(DistributionBase):
    '''Convenience base class for categorical distributions.'''
    pass
