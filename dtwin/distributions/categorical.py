# This file is part of dtwin.
#
# dtwin is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# dtwin is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Copyright (C) 2021 Max Planck Society
#
# dtwin is being developed at the Max Planck Institute for Astrophysics

from .distribution_base import CategoricalDistributionBase
from functools import partial
import numpy.random as rand

class Categorical(CategoricalDistributionBase):
    '''Categorical distribution.'''
    def __init__(self,categories,p=None,replace=True):
        '''A categorical distribution.

        Parameters:
        -----------
        categories : list or numpy.array
            The list of categories to choose from.
        p : list or numpy.array or None
            The list/array of probabilities 
        '''
        self.name = 'Discrete Choice'
        self.target = categories
        self.categories = categories
        self._distribution = partial(rand.choice,a=categories,p=p,\
                                     replace=replace)

    def sample(self,size=None):
        return self._distribution(size=size)
    
    @property
    def dtype(self):
        return self.__class__
