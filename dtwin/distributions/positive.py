# This file is part of dtwin.
#
# dtwin is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# dtwin is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Copyright (C) 2021 Max Planck Society
#
# dtwin is being developed at the Max Planck Institute for Astrophysics

import numpy.random as rand
from functools import partial
from .distribution_base import PositiveDistributionBase

class PositiveConstant(PositiveDistributionBase):
    '''Positive constant.'''
    def __init__(self,offset=0.):
        '''Constant distribution.'''
        if offset < 0.:
            raise ValueError('offset must be positive')
        self._off = offset

    def sample(self,size=None):
        return self._off

    @property
    def dtype(self):
        return float

class Exponential(PositiveDistributionBase):
    '''Exponential distribution.'''
    def __init__(self,scale=1.,offset=0.):
        '''Exponential distribution.

        Parameters:
        -----------
        scale : float
        offset : float
        '''
        self.name = 'Exponential'
        self._distribution = partial(rand.exponential,scale=scale)
        self._off = offset

    def sample(self,size=None):
        '''Sample from the distribution.'''
        return self._off + self._distribution(size=size)

    @property
    def dtype(self):
        return float

class Gamma(PositiveDistributionBase):
    '''Gamma distribution.'''
    def __init__(self,scale=1.,shape=1.,offset=0.):
        '''Gamma distribution.

        Parameters:
        -----------
        scale : float
        shape : float
        '''
        self.name = 'Gamma'
        self._distribution = partial(rand.gamma,shape=shape,scale=scale)
        self._off = offset

    def sample(self,size=None):
        '''Sample from the distribution.'''
        return self._off + self._distribution(size=size)

    @property
    def dtype(self):
        return float

class LogNormal(PositiveDistributionBase):
    '''Lognormal distribution.'''
    def __init__(self,mean=0.,sigma=1.,offset=0.):
        '''Lognormal distribution.

        Parameters:
        -----------
        mean : float
        sigma : float
        offset : float
        '''
        self.name = 'LogNormal'
        self._distribution = partial(rand.lognormal,mean=mean,sigma=sigma)
        self._off = offset

    def sample(self,size=None):
        '''Sample from the distribution.'''
        return self._off + self._distribution(size=size)

    @property
    def dtype(self):
        return float

