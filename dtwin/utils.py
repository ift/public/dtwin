# This file is part of dtwin.
#
# dtwin is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# dtwin is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Copyright (C) 2021 Max Planck Society
#
# dtwin is being developed at the Max Planck Institute for Astrophysics

from .patterns import Singleton
import inspect

###############################################################################
# Utilities
###############################################################################

class Debugger(Singleton):
    def __init__(self,debug=False,add_fct_name=False,level=0):
        self._debug = debug
        self._add_fct_name = add_fct_name
        self._level = level

    @property
    def debug(self):
        return self._debug

    @property
    def level(self):
        return self._level

    @property
    def add_fct_name(self):
        return self._add_fct_name

    def set_level(self, level=0):
        if isinstance(level,int):
            self._level= level
        else:
            raise TypeError('level must be integer.')

    def set_debugging(self, debug=True):
        if isinstance(debug,bool):
            self._debug = debug
        else:
            raise TypeError('debug must be Boolean.')

    def fct_name(self, add_fct_name=True):
        if isinstance(add_fct_name,bool):
            self._add_fct_name= add_fct_name
        else:
            raise TypeError('add_fct_name must be Boolean.')

# instantiate the Debugger or, if it already exists, reference it locally
dbg = Debugger()

def dprint(s,level=0,**kwargs):
    ''' print if and only if debugging is activated above the threshold
    
    Parameters:
    ==========
    s : str
        The string to be printed
    level : int
        The debugging level of this statement. Only if the debugging level
        is smaller than the debugger's level, the statement is printed.

    additional keywords are passed directly to Python's print
    '''
    global dbg
    # return early if level is too large compared to set debugging level
    if dbg.level < level:
        return
    try:
        if dbg.debug:
            if dbg.add_fct_name:
                fct_list = inspect.stack()[1:-2]
                fct_str  = '/'.join([i.function for i in fct_list[::-1]])
                dbg_s = ' ** DEBUGGER @ '+fct_str+' ** '
                dbg_s += '\n'+'>'*len(dbg_s)+'\n'+s+'\n'+'<'*len(dbg_s)+'\n'
            else:
                dbg_s = s

            print(dbg_s,**kwargs)
    except:
        pass

def tprint(s):
    ''' prints [system / internal system / synchronised simulation time] s '''
    import time
    from .events.time import Time
    _time = Time()
    s = (f'[{time.time():.5f} / '
         f'{_time.real_time:.5f} / '
         f'{_time.now_synced:.5f}] {s}')
    print()

def testwrapper(func):
    '''
    prints the name of the decorated function if and only if debugging is on.
    '''
    s = func.__name__
    lw = 78

    def inner(*args,**kwargs):
        string  = " "+lw*"_"+'\n'
        string += "|"+lw*" "+"|"+'\n'
        string += '| '+s+(lw-1-len(s))*" "+'|'+'\n'
        string += "|"+"_"*lw+"|"+'\n'
        dprint(string,level=0)

        return func(*args,**kwargs)

    return inner

def checktype(obj,datatype,var='argument',allow_iterable=False,
              force_list=False,force_set=False,force_tuple=False):
    ''' Checks type(s) of objects/iterables, can cast to list/set/tuple
    Raises TypeError if specified type(s) aren't matched.
    Otherwise, returns the object.
    If one of force_list/force_set/force_tuple is True, the object(s) are 
    put in the respective iterable, which is returned. Only one of these 
    options may be set.

    Parameters:
    ==========
    obj: object or iterable of objects
        The object to be checked
    datatype: type/class or iterable thereof
        The allowed datatypes
    var: str, optional
        The name of the variable to be checked. Will be used in TypeError,
        if raised. Useful for debugging.
    allow_iterable: bool, default: False
        Whether iterables are allowed. If so, the elements of iterables are
        checked against allowed types. If the iterable is a dictionary, the
        values are checked.
    force_list: bool, optional
        Returns always a list, if True.
    force_set: bool, optional
        Returns always a set, if True.
    force_tuple: bool, optional
        Returns always a tuple, if True.

    Returns:
    =======
    The object itself or the specified iterable if one of force_x is True.
    '''

    if sum([force_tuple,force_list,force_set]) > 1:
        raise RuntimeError(('Only one of force_set, force_tuple '
                            'and force_list may be True'))

    if allow_iterable:
        if isinstance(obj,dict):
            for item in obj.values():
                if not isinstance(item,datatype):
                    err = (f'Argument must be of type {datatype}'
                            'or iterable of such.')
                    raise TypeError(err)
        elif isinstance(obj,(list,tuple,set)):
            for item in obj:
                if not isinstance(item,datatype):
                    err = (f'Argument must be of type {datatype} '
                            'or iterable of such.')
                    raise TypeError(err)
        else:
            if not isinstance(obj,datatype):
                err = (f'Argument must be of type {datatype} '
                        'or iterable of such.')
                raise TypeError(err)
    else:
        if not isinstance(obj,datatype):
            raise TypeError(f'Argument must be of type {datatype}')

    if force_tuple: 
        if type(obj) in [set,list,tuple]:
            return tuple(obj)
        elif type(obj) is dict:
            return tuple(obj.values())
        elif obj is None:
            return tuple()
        else:
            return (obj)

    if force_list: 
        if type(obj) in [set,list,tuple]:
            return list(obj)
        elif type(obj) is dict:
            return list(obj.values())
        elif obj is None:
            return list()
        else:
            return [obj]

        return list(obj) if obj is not None else list()

    if force_set:  
        if type(obj) in [set,list,tuple]:
            return set(obj)
        elif type(obj) is dict:
            return set(obj.values())
        elif obj is None:
            return set()
        else:
            return {obj}

    return obj
