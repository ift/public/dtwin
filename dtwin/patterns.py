# This file is part of dtwin.
#
# dtwin is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# dtwin is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Copyright (C) 2021 Max Planck Society
#
# dtwin is being developed at the Max Planck Institute for Astrophysics

from abc import ABC, ABCMeta, abstractproperty, abstractmethod
from weakref import WeakSet, WeakValueDictionary

class Singleton:
    '''
    The Singleton design pattern.
    Inherit to allow only for one instance of a class.
    '''

    # __new__ is called before __init__
    def __new__(cls, *args, **kwargs):
        # if there is no instance yet, create it using the super.__init__
        # then, add it as a class attribute so the information about its
        # existence persists in the class
        if not hasattr(cls, '_instance'):
            orig = super(Singleton, cls)
            # object.__new__ accepts no additional arguments
            cls._instance = orig.__new__(cls)

        # return the unique instance if it already exists
        return cls._instance
        # after that, the __init__ method will be called
        # it will be called for EVERY instantiation, even if the instance
        # doesn't change, so make sure not to overwrite information in the
        # __init__ method which is supposed to be persistent

class Multiplet:
    '''
    A class keeping weak references to its instances.

    Note:
    Using this in IPython causes trouble as IPython's In and Out are creating 
    references to objects, too. Hence, entries in Multiplet._instances might 
    not be deconstructed as expected.

    Inherit to be able to get instances of classes (and their subclasses, etc.)
    '''

    _instances = WeakValueDictionary()
    _next_instance_id=0

    def __new__(cls, *args, **kwargs):
        # create the new instance
        orig = super(Multiplet, cls)
        new_instance = orig.__new__(cls)

        # Careful, the statement "hasattr(cls, '_instances')" is true if any 
        # inherited class has that attribute! But we do not want to mix up 
        # inherited classes and subclasses here. Hence:
        if not '_instances' in cls.__dict__:
            cls._instances = WeakValueDictionary()
            cls._next_instance_id=0
            
        # give the instance a reference ID, unique with respect to this class
        new_instance._instance_id = cls._next_instance_id

        # attach a link to the list of weak references in this class
        cls._instances[cls._next_instance_id] = new_instance

        # useful for debugging
        '''
        print((f'Created new instance of class {cls.__name__} with id '
               f'{new_instance._instance_id} at {hex(id(new_instance))}'))
        '''

        # increase the instance counter
        cls._next_instance_id += 1

        return new_instance

    @classmethod
    def get_instances(cls,include_subclasses=False,flatten=True,\
            weak=False,_filter=lambda x:True,cast=None,\
            return_set=False):
        '''
        Return all instances of the class (and, possibly, subclasses) 

        _filter:
            A function mapping any instance to either True or False.
            Only those which return True will be inserted in the resulting set.

        Returns:
        --------
        * if flatten: list of instances
          else: dict w/ keys 'class name', 'instances', 'subclasses'
        * if include_subclasses: search through subclasses recursively
        * weak: return weak reference
        '''

        if not include_subclasses:
            # returns a list
            return cls._instances

        # filter instances and return dictionary
        instances = {k:v for k,v in dict(cls._instances).items() if _filter(v)}

        if weak:
            instances = WeakValueDictionary(instances)

        if not flatten:
            result = {"class_name":cls.__name__,
                      "instances":instances,
                      "subclasses":[]}

            for sub in cls.__subclasses__():
                sub_inst = sub.get_instances(include_subclasses=True,\
                                             flatten=False,\
                                             weak=weak,\
                                             _filter=_filter)
                result["subclasses"].append(sub_inst)

        else:

            result = {}

            for k,v in instances.items():
                result[f'{cls.__name__}:{k}'] = v

            for sub in cls.__subclasses__():
                sub_inst = sub.get_instances(include_subclasses=True,\
                                             flatten=True,\
                                             weak=weak,\
                                             _filter=_filter)
                result.update(sub_inst)

        # result is a dictionary with items like name:instance
        if return_set:
            result = set(result.values())

        return result

    def __str__(self):
        try:
            s = f'{self.__class__.__name__} "{self.name}"'
        except:
            s = repr(self)
        return s

    def __repr__(self):
        try:
            s = (f'<{self.__class__.__name__} '
                 f'#{self._instance_id} ("{self.name}")>')
        except:
            s = f'<{self.__class__.__name__} #{self._instance_id} (no name)>'
        return s

# https://stackoverflow.com/questions/3603502
class FrozenClass(object):
    ''' Inherit and call _freeze at some point to prohibit new attributes '''
    _isfrozen = False
    def __setattr__(self, key, value):
        if self._isfrozen and not hasattr(self, key):
            raise TypeError(f"{self} is a frozen class")
        object.__setattr__(self, key, value)

    def _freeze(self):
        self._isfrozen = True
