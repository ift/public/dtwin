# This file is part of dtwin.
#
# dtwin is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# dtwin is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Copyright (C) 2021 Max Planck Society
#
# dtwin is being developed at the Max Planck Institute for Astrophysics

from .parameter_base import ParameterBase
from ..errors import FixedParameterError

class FloatParameter(ParameterBase):
    ''' Base class for parameters. '''

    def __init__(self,name,value,interface=None,adjustable=True,visible=True):
    
        super(FloatParameter,self).__init__()

        self.name = name
        self.adjustable = True # necessary to set value
        self.value = value
        self.adjustable = adjustable
        self.visible = visible
        self.interface = interface

    @property
    def dtype(self):
        return float

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self,new):
        if not self.adjustable:
            raise FixedParameterError(f'The Parameter {self.name} is NOT adjustable')
        elif isinstance(new,self.dtype):
            self._value = new
        else:
            raise TypeError(f'value needs to be of type {self.dtype}')
