# This file is part of dtwin.
#
# dtwin is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# dtwin is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Copyright (C) 2021 Max Planck Society
#
# dtwin is being developed at the Max Planck Institute for Astrophysics

from ..patterns import Multiplet
from ..utils import checktype
from abc import ABCMeta, abstractproperty, abstractmethod
from ..interfaces.interface_base import InterfaceBase

class ParameterBase(Multiplet,metaclass=ABCMeta):
    ''' Base class for parameters.
    '''
    def __init__(self):
        self._abstract = False
        self._name = None
        self._visible = True
        self._adjustable = True
        self._interface = None
        self._dtype = None

    @property
    def abstract(self):
        ''' Defines abstract objects.
        Abstract objects are not checked against consistency and
        take not part in the environment's graphs
        '''
        return self._abstract

    @abstract.setter
    def abstract(self,new):
        if not isinstance(new,bool):
            raise TypeError
        self._abstract = new
    
    @property
    def name(self):
        return self._name

    @name.setter
    def name(self,new):
        if not isinstance(new,str):
            raise TypeError('name must be of type string')
        self._name = new

    @property
    def adjustable(self):
        return self._adjustable

    @adjustable.setter
    def adjustable(self,new):
        self._adjustable = checktype(new,bool,'adjustable')

    @property
    def visible(self):
        return self._visible

    @visible.setter
    def visible(self,new):
        self._visible = checktype(new,bool,'visible')

    @property
    def interface(self):
        '''Read/Write-Interfaces to read/set the parameter'''
        return self._interface

    @interface.setter
    def interface(self,new):
        '''Read/Write-Interfaces to read/set the parameter'''
        self._interface = checktype(new,(type(None),InterfaceBase),'interface')

    @abstractproperty
    def dtype(self):
        '''The parameter's data type

        isinstance(self.value,self.dtype) must return True
        '''
        pass

    @abstractproperty
    def value(self):
        '''The current value'''
        pass

    @classmethod
    def check_consistency(cls,verbose=False):
        ''' Check consistency of parameters
        '''
        from ..graphs.environment_graph import EnvironmentGraph
        from ..errors import Inconsistency, InconsistencyWarning,\
                UniquenessError
        import warnings

        g = EnvironmentGraph().get_subgraph(type=['p','o','i'])

        names = set()
        count = 0

        for node, outdeg in g.out_degree:
            if isinstance(node,ParameterBase):
                count += 1
                names.add(node.name)
                if outdeg == 0:
                    s = f'{node} does not influence any observable'
                    warnings.warn(s,InconsistencyWarning)

        if len(names) < count:
            s = f'Parameters must have unique names'
            raise UniquenessError(s)

        for node, indeg in g.in_degree:
            if isinstance(node,ParameterBase) and indeg == 0:
                s = f'{node} has no interface'
                warnings.warn(s,InconsistencyWarning)
