# This file is part of dtwin.
#
# dtwin is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# dtwin is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Copyright (C) 2021 Max Planck Society
#
# dtwin is being developed at the Max Planck Institute for Astrophysics

from .observable_base import ObservableBase

class RandomObservable(ObservableBase):
    ''' Random observable with no external dependencies.
    '''

    def __init__(self,name,distribution,parent=None,visible=True):

        super(RandomObservable,self).__init__()

        self.name = name
        self._dist = distribution
        self.parent = parent
        self.visible = visible
        self.needs = []
        self.parameters = []

    @property
    def dtype(self):
        return self._dist.dtype

    async def sample(self,*args,**kwargs):
        return self._dist.sample()
