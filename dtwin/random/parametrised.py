# This file is part of dtwin.
#
# dtwin is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# dtwin is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Copyright (C) 2021 Max Planck Society
#
# dtwin is being developed at the Max Planck Institute for Astrophysics

from .observable_base import ObservableBase
from ..parameters.parameter_base import ParameterBase
from ..waypoints.waypoint_base import WaypointBase
from ..areas.area_base import AreaBase
from numpy.random import normal
from ..utils import checktype

class ParametrisedNormal(ObservableBase):
    ''' 1D normal distribution with external parameters
    '''

    def __init__(self,name,parent=None,mu=None,sigma=None,visible=True):

        super(ParametrisedNormal,self).__init__()

        self.name = name
        self.parent = parent
        self.mu = mu
        self.sigma = sigma
        self.needs = [self.mu,self.sigma]
        self.visible = visible
        self.parameters = [self.mu,self.sigma]

    @property
    def needs(self):
        return self._needs

    @needs.setter
    def needs(self,new):
        needs = checktype(new,(ObservableBase,ParameterBase),\
                allow_iterable=True,\
                force_set = True)
        self._needs = new

    @property
    def parameters(self):
        return self._parameters

    @parameters.setter
    def parameters(self,new):
        self._parameters = new

    @property
    def dtype(self):
        return float

    @property
    def parent(self):
        return self._parent

    @parent.setter
    def parent(self,new):
        if not isinstance(new,(type(None),WaypointBase,AreaBase)):
            tmp = 'parent of Observable must be of type WaypointBase or AreaBase'
            raise HierarchyError(tmp)
        self._parent = new

    async def sample(self,*args,**kwargs):
        sample = normal(self.mu.value,self.sigma.value)
        return sample
