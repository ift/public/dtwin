# This file is part of dtwin.
#
# dtwin is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# dtwin is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Copyright (C) 2021 Max Planck Society
#
# dtwin is being developed at the Max Planck Institute for Astrophysics

from abc import abstractmethod, ABCMeta, abstractproperty
from ..errors import HierarchyError, UniquenessError, CyclicGraphError

from ..patterns import Multiplet
from ..utils import dprint, checktype


class ObservableBase(Multiplet,metaclass=ABCMeta):
    '''Base class for Observables. 
    '''

    def __init__(self):
        # initialise hidden variables
        self._name       = NotImplemented
        self._visible    = True # observables are visible by default
        self._parent     = None
        self._needs      = [] 
        self._parameters = []
        self._abstract   = False

    @property
    def abstract(self):
        ''' Defines abstract objects.
        Abstract objects are not checked against consistency and
        take not part in the environment's graphs
        '''
        return self._abstract

    @abstract.setter
    def abstract(self,new):
        if not isinstance(new,bool):
            raise TypeError
        self._abstract = new

    @property
    def name(self):
        '''Unique name of this instance
        '''
        return self._name

    @name.setter
    def name(self,new):
        if not isinstance(new,str):
            raise TypeError('name must be string')
        self._name = new

    @property
    def visible(self):
        ''' Whether or not this Observable is visible.
        '''
        return self._visible

    @visible.setter
    def visible(self,new):
        self._visible = checktype(new,bool,'visible')

    @property
    def needs(self):
        '''List of dependencies on other Observables.
        Those need to be either already applied to the container or available
        in the current context (Area / Waypoint)
        '''
        return self._needs

    @needs.setter
    def needs(self,new):
        self._needs = checktype(new,ObservableBase,'needs',\
                allow_iterable=True,force_list=True)

    @property
    def parameters(self):
        '''List of dependencies on Parameters.
        '''
        return self._parameters

    @parameters.setter
    def parameters(self,new):
        from ..parameters.parameter_base import ParameterBase
        self._parameters = checktype(new,(type(None),ParameterBase),'parameters',\
                allow_iterable=True,force_list=True)

    @abstractproperty
    def dtype(self):
        '''The data type of this Observable
        '''
        pass

    @property
    def parent(self):
        '''The context (Area / Waypoint) of this Observable
        '''
        return self._parent

    @parent.setter
    def parent(self,new):
        from ..waypoints.waypoint_base import WaypointBase
        from ..areas.area_base import AreaBase
        if not isinstance(new,(type(None),WaypointBase,AreaBase)):
            raise HierarchyError(('Observable needs to be child '
                                  'to either Waypoint or Area'))
        if new is not None and self not in new.observables:
            new.observables.add(self)
        self._parent = new

    @abstractmethod
    async def sample(self,container,start,end,**kwargs):
        '''Sample this Observable w.r.t. a given container, as well as entry
        and exit times of the current context (Area / Waypoint)
        '''
        pass

    @classmethod
    def check_consistency(cls,verbose=False):
        ''' Check consistency of observables
        * check whether names are unique across the environment
        * check whether all instances have parents
        * check whether there are loops in the needs hierarchy
        '''

        import networkx as nx
        from ..graphs.data_model import DataModel

        g = DataModel().graph

        obs = [n for n,d in g.nodes(data=True) if d['type'] == 'o']

        if len(set([o.name for o in obs])) != len(obs):
            from dtwin.errors import UniquenessError
            raise UniquenessError(f'Observable names are not unique')
        elif verbose:
            print('│ ✓ Observables have unique names')

        if not nx.is_directed_acyclic_graph(g):
            if verbose:
                print('│ ! Data model is not globally acyclic')
            from ..graphs.environment_graph import EnvironmentGraph
            eg = EnvironmentGraph()
            awop = eg.get_subgraph(type=['a','w','o','p'])
            ao = eg.get_subgraph(type=['a','o'])
            wo = eg.get_subgraph(type=['w','o'])
            a = eg.filter_nodes(type='a')
            w = eg.filter_nodes(type='w')
            o = eg.filter_nodes(type='o')

            a_clusters = []
            w_clusters = []

            for area in a:
                obs = list(ao.successors(area))
                area_subgraph = g.subgraph(obs)
                if not nx.is_directed_acyclic_graph(area_subgraph):
                    cycle = list(nx.find_cycle(area_subgraph))
                    from dtwin.errors import CyclicDataModel
                    s = (f'Observables have cyclic dependencies '
                         f'in area {area}. '
                         f'One cycle is given by {cycle}.')
                    raise CyclicDataModel(s)

            for waypoint in w:
                obs = list(wo.successors(waypoint))
                wp_subgraph = g.subgraph(obs)
                if not nx.is_directed_acyclic_graph(wp_subgraph):
                    cycle = list(nx.find_cycle(wp_subgraph))
                    from dtwin.errors import CyclicDataModel
                    s = (f'Observables have cyclic dependencies '
                         f'in waypoint {waypoint}. '
                         f'One cycle is given by {cycle}.')
                    raise CyclicDataModel(s)

            if verbose:
                print('│   ✓ Data model is locally acyclic')

        elif verbose:
            print('│ ✓ Data model is globally acyclic')

