# This file is part of dtwin.
#
# dtwin is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# dtwin is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Copyright (C) 2021 Max Planck Society
#
# dtwin is being developed at the Max Planck Institute for Astrophysics

from .observable_base import ObservableBase
from ..waypoints.waypoint_base import WaypointBase
from ..areas.area_base import AreaBase
from ..events.time import Time
from ..events.temporalevent import TemporalEvent 
from ..utils import checktype
from ..errors import HierarchyError
import numpy as np
#from scipy import sparse as sps

_time = Time()
import time

class CachedGaussianProcess1D(ObservableBase):
    ''' Gaussian Process in 1D
    '''
    def __init__(self, name, mean=lambda x:0., kernel=lambda x,y:float(x==y),\
            parent=None,cachesize=1000,sample_period=1.,history=None,\
            vectorised_kernel=None,regular=False,visible=False,\
            noise_amplitude=1e-9\
            ):

        super(CachedGaussianProcess1D,self).__init__()

        # TODO: implement sparse kernels

        self.name = name
        self.visible = visible

        self._mean = mean

        if isinstance(self._mean,ObservableBase):
            self.needs = [self._mean]
        else:
            self.needs = []

        self._kernel = kernel
        self._vectorised_kernel = vectorised_kernel

        self._noise_amp = noise_amplitude

        self._regular = regular

        self._cachesize = cachesize
        self._cache = np.zeros((cachesize,2))
        self._dt    = sample_period

        self._tmp   = None
        self._tmp2  = None

        if history is None:
            #print(f'{self.name}: Generating history...')
            self._n_samples = 0
            _t  = _time.now
            _dt = self._dt
            _timestamps = np.arange(_t-self._cachesize*_dt,_t,_dt)
            _values     = self._sample(_timestamps,given=None)
            self._n_samples = self._cachesize
            #print(f'{self.name}: ...done')
        else:
            self._add_to_cache(history)
            self._n_samples = len(history)

        # discretise the mean function
        self._mu = self._mean(self._cache[:self._n_samples,0])

        # discretise the kernel
        self._sigma = self._discretise_kernel(self._cache[:self._n_samples,0])
        #self._sigma[self._sigma < 1e-9*self._sigma[0,0]] = 0 
        #self._sigma = sps.dia_matrix(self._sigma)

        # precision matrix
        #self._preci = np.linalg.inv(self._sigma)

        _ = self._sample(_time.now,add=False) # initialises tmp and tmp2

        self._parent = parent

        # schedule a recurring event 
        event = TemporalEvent(\
                action=self.sample,\
                time=_time.now + self._dt,\
                recurring_period=self._dt,\
                push=True,\
                )

    def _discretise_kernel(self,indices,indices_2=None,noise=None):
        ''' Discretises the kernel on the cartesian product of given indices.
            Adds numerical noise to render the resulting matrix SPD.
        '''

        if noise is None:
            noise = self._noise_amp
        #print(f'{self.name}: Discretising Kernel...')
        #t0 = time.time()
        if indices_2 is None:
            indices_2 = indices
        if self._vectorised_kernel is not None:
            discrete = self._vectorised_kernel(indices,indices_2)
        else: 
            l1 = len(indices)
            l2 = len(indices_2)
            cartesian = np.transpose(\
                    [np.tile(indices_2, l1), np.repeat(indices, l2)])
            cartesian = cartesian.reshape(l1,l2,2)
            discrete  = np.apply_along_axis(self._kernel,axis=2,arr=cartesian)

        # add numerical noise to covariance so it's semipositive definite
        m = np.min(discrete.shape)
        n = noise*np.diag(np.ones(m))
        discrete[:m,:m] += n
        t5 = time.time()

        #print(f'{self.name}: ...done')
        return discrete

    @property
    def dtype(self):
        return float

    @property
    def parent(self):
        return self._parent

    @parent.setter
    def parent(self,new):
        if not isinstance(new,(WaypointBase,AreaBase)):
            err = 'Observable needs to be child to either Waypoint or Area'
            raise HierarchyError(err)
        self._parent = new

    @property
    def parameters(self):
        return []

    @property
    def needs(self):
        return self._needs

    @needs.setter
    def needs(self,args):
        args = checktype(args,(ObservableBase,str,type(None)),'needs',
                         allow_iterable=True,force_set=True)
        self._needs = args

    def _sample(self,t,given=None,add=True):
        ''' Sample the process on the indices `t` with given data `given`
            * t must be a 1D numpy array
            * given, if not None, must be a 2D numpy array
                * axis 0 contains the indices 
                * axis 1 contains the values at those indices
            * if given is None, take the current cache as given

            implementation of Bishop 2.81 and 2.82
        '''

        if given is None:
            given = self._cache[:self._n_samples]

        if isinstance(t,(float,int)):
            t = np.array([t])
        elif isinstance(t,type(None)):
            t = np.array([_time.now])

        n = self._n_samples
        
        idx_a = t

        idx_b = given[:n,0]
        x_b = given[:n,1]

        n_a = len(idx_a)
        n_b = len(idx_b)

        mu_a = self._mean(idx_a)
        
        min_ab = min(n_a,n_b)
        max_ab = max(n_a,n_b)

        if self._regular and n >= n_a+n_b:
            sig_aa = self._sigma[:n_a,:n_a]
        else:
            sig_aa = self._discretise_kernel(idx_a)

        if n_b > 0:
            mu_b = self._mean(idx_b)
            
            if self._regular and n >= max_ab:

                if n_a == 1:
                    if self._tmp is not None:
                        mu_a_given_b = mu_a+self._tmp.dot((x_b-mu_b)[:n_b-n_a])
                        sig_a_given_b = self._tmp2
                    else:
                        sig_ab = self._sigma[:n_a,n_a:n]
                        sig_ba = self._sigma[n_a:n,:n_a]
                        sig_bb = self._sigma[n_a:n,n_a:n]
                        sig_bb_inv = np.linalg.inv(sig_bb)

                        self._tmp = sig_ab.dot(sig_bb_inv)
                        mu_a_given_b = mu_a+self._tmp.dot((x_b-mu_b)[:n_b-n_a])

                        self._tmp2 = sig_aa - self._tmp.dot(sig_ba)
                        sig_a_given_b = self._tmp2
                else:
                    #sig_ab = self._sigma[:n_a,n_a:n]
                    #sig_bb = self._sigma[n_a:n,n_a:n]
                    #sig_bb_inv = np.linalg.inv(sig_bb)
                    #mu_a_given_b = mu_a+sig_ab.dot(sig_bb_inv).dot((x_b-mu_b)[:n_b-n_a])
                    #sig_a_given_b = np.linalg.inv(self._preci[:n_a,:n_a])

                    # TODO: This is not tested yet!
                    sig_ab = self._sigma[:n_a,n_a:n]
                    sig_ba = np.transpose(sig_ab)
                    sig_bb = self._sigma[n_a:n_b,n_a:n_b]
                    sig_bb_inv = np.linalg.inv(sig_bb)
                    tmp = sig_ab.dot(sig_bb_inv)
                    mu_a_given_b = mu_a+tmp.dot((x_b-mu_b)[:n_b-n_a])
                    sig_a_given_b = sig_aa - tmp.dot(sig_ba)
                    
            else:
                sig_ab = self._discretise_kernel(idx_a,idx_b)
                sig_ba = np.transpose(sig_ab)
                sig_bb = self._discretise_kernel(idx_b) 
                
                sig_bb_inv = np.linalg.inv(sig_bb)

                mu_a_given_b = mu_a + sig_ab.dot(sig_bb_inv).dot(x_b - mu_b)

                sig_a_given_b = sig_aa - sig_ab.dot(sig_bb_inv).dot(sig_ba)

            # Note: numpy implementation is slow but robust (does SVD of cov)
            #samples = np.random.multivariate_normal(\
            #        mean=mu_a_given_b,\
            #        cov=sig_a_given_b)

            # this is faster but fails if the covariance matrix is not SPD
            samples =  np.random.standard_normal(mu_a_given_b.size)
            samples =  np.linalg.cholesky(sig_a_given_b) @ samples
            samples += mu_a_given_b

        else:
            # Note: numpy implementation is slow but robust (does SVD of cov)
            #samples = np.random.multivariate_normal(\
            #        mean=mu_a,\
            #        cov=sig_aa)

            # this is faster but fails if the covariance matrix is not SPD
            samples =  np.random.standard_normal(mu_a.size)
            samples =  np.linalg.cholesky(sig_aa) @ samples
            samples += mu_a

        if add:
            new_history = np.transpose(np.array([idx_a,samples]))
            self._add_to_cache(new_history)

        return samples

    def _add_to_cache(self,history):
        # sort history by first column, decending
        sort_hist = history[np.argsort(history[:,0])][::-1]
        lh = len(sort_hist)

        if lh >= self._cachesize:
            self._cache = sort_hist[:self._cachesize]
        else:
            self._cache = np.roll(self._cache,shift=lh,axis=0)
            self._cache[:lh] = sort_hist
            
    async def sample(self,container=None,**kwargs):
        sam = self._sample(t=_time.now)
        return sam

class ProcessStatistic(ObservableBase):
    available_statistics = {\
            'int':'integral',\
            'mean':'mean',\
            'min':'minimum',\
            'max':'maximum',\
            'amp':'max-min',\
            'std':'standard deviation',\
            'dmean':'derivative mean',\
            'dmin':'derivative minimum',\
            'dmax':'derivative maximum',\
            'damp':'dmax-dmin',\
            'point':'point sample',\
            }
        
    def __init__(self,process,statistic,visible=True):

        super(ProcessStatistic,self).__init__()

        self.name = f'{process.name}_{statistic}'
        self.process = process
        self.statistic = statistic
        self.needs = [process]
        self.parent = self.process.parent
        self.visible = visible

    @property
    def dtype(self):
        return self.process.dtype

    @property
    def parent(self):
        return self._parent

    @parent.setter
    def parent(self,new):
        if not isinstance(new,(AreaBase)):
            err = 'Statistic needs to be child of an Area'
            raise HierarchyError(err)
        self._parent = new

    @property
    def parameters(self):
        return []

    @property
    def needs(self):
        return self._needs

    @needs.setter
    def needs(self,args):
        args = checktype(args,(ObservableBase,str,type(None)),'needs',
                         allow_iterable=True,force_set=True)
        self._needs = args

    @property
    def statistic(self):
        return self._statistic

    @statistic.setter
    def statistic(self,new):
        ''' Returns a function which draws statistics from arrays
        '''
        if new == 'int':
            def stat(arr):
                if len(arr) > 1:
                    res = np.trapz(y=arr[:,1],x=arr[:,0])
                else:
                    res = np.nan
                return res
        elif new == 'mean':
            def stat(arr):
                if len(arr) > 1:
                    res =  np.trapz(y=arr[:,1],x=arr[:,0])
                    res = res / (arr[-1,0] - arr[0,0])
                else:
                    res = np.nan
                return res
        elif new == 'min':
            def stat(arr):
                return np.min(arr[:,1])
        elif new == 'max':
            def stat(arr):
                return np.max(arr[:,1])
        elif new == 'std':
            def stat(arr):
                return np.std(arr[:,1])
        elif new == 'amp':
            def stat(arr):
                amp = arr[:,1].max() - arr[:,1].min()
                return amp
        elif new == 'dmean':
            def stat(arr):
                ''' use mean derivative thm - assumes differentiability '''
                res = arr[-1,1] - arr[0,1]
                res = res / (arr[-1,0] - arr[0,0])
                return res
        elif new == 'dmin':
            def stat(arr):
                darr = np.gradient(arr[:,1],arr[:,0])
                return np.min(darr)
        elif new == 'dmax':
            def stat(arr):
                darr = np.gradient(arr[:,1],arr[:,0])
                return np.max(darr)
        elif new == 'damp':
            def stat(arr):
                darr = np.gradient(arr[:,1],arr[:,0])
                damp = darr.max() - darr.min()
                return damp
        elif new == 'point':
            def stat(arr):
                return arr[-1,1] # last value
        elif callable(new):
            stat = new
            self.name = stat.__name__
        else:
            raise TypeError('statistic must be string or callable.')

        self._statistic = stat

    async def sample(self,container,start,end,**kwargs):
        c = self.process._cache
        c = c[(c[:,0]>=start) & (c[:,0]<=end)]

        if len(c) > 1:
            stat = self.statistic(c)
        else:
            stat = np.nan

        return stat
