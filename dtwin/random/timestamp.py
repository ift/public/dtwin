# This file is part of dtwin.
#
# dtwin is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# dtwin is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Copyright (C) 2021 Max Planck Society
#
# dtwin is being developed at the Max Planck Institute for Astrophysics

from .observable_base import ObservableBase
from ..errors import HierarchyError
from ..events.time import Time

_time = Time()

class Timestamp(ObservableBase):
    ''' Returns the current time. 
    '''

    def __init__(self,parent=None,visible=True):

        super(Timestamp,self).__init__()

        self.parent = parent
        if parent is not None and parent.name is not None:
            self.name = f'{self.parent.name}_timestamp'
        else:
            self.name = f'Timestamp_{self._instance_id}'
        self.visible = visible

    @property
    def needs(self):
        return []

    @property
    def parameters(self):
        return []

    @parameters.setter
    def parameters(self,new):
        raise AttributeError("RandomObservable cannot have parameters")

    @property
    def dtype(self):
        return float

    @property
    def parent(self):
        return self._parent

    @parent.setter
    def parent(self,new):
        from ..waypoints.waypoint_base import WaypointBase
        from ..areas.area_base import AreaBase
        if not isinstance(new,(type(None),WaypointBase,AreaBase)):
            errs = 'Observable needs to be child to either Waypoint or Area'
            raise HierarchyError(errs)
        self._parent = new
        if new is not None and new.name is not None:
            self.name = f'{self.parent.name}_timestamp'

    async def sample(self,*args,**kwargs):
        return _time.now
