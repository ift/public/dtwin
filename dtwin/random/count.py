# This file is part of dtwin.
#
# dtwin is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# dtwin is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Copyright (C) 2021 Max Planck Society
#
# dtwin is being developed at the Max Planck Institute for Astrophysics

from .observable_base import ObservableBase

class Count(ObservableBase):
    ''' Increments an container specific integer at every sampling.
    '''

    def __init__(self,name,parent=None,visible=True,\
            reset_value=0,increment=1,threshold=None):

        super(Count,self).__init__()

        self.name = name
        self.parent = parent
        self.visible = visible
        self._threshold = threshold
        self._increment = increment
        self._reset_value = reset_value

    @property
    def needs(self):
        return []

    @property
    def parameters(self):
        return []

    @parameters.setter
    def parameters(self,new):
        pass

    @property
    def dtype(self):
        return int

    def value(self,container,**kwargs):
        '''Get container's current value'''
        try:
            return container.observables[self.name]
        except KeyError:
            return None

    def reset(self,container,**kwargs):
        '''Reset container's value'''
        container.observables[self.name] = self._reset_value

    async def sample(self,container,**kwargs):
        '''Increment container's value and return the result'''
        # define default if the container doesn't carry this observable yet
        former = container.observables.get(self.name,self._reset_value)
        res = former + self._increment
        if self._threshold and res >= self._threshold:
            res = self._reset_value
        return res
