# This file is part of dtwin.
#
# dtwin is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# dtwin is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Copyright (C) 2021 Max Planck Society
#
# dtwin is being developed at the Max Planck Institute for Astrophysics

from .graph_base import GraphBase

class AreaHierarchy(GraphBase):
    ''' Graphs visualising the hierarchy of Area objects
    '''
    def __init__(self,draw=False,verbose=False):
        ''' Graphs visualising the hierarchy of Area objects

        Parameters:
        ==========
        draw : bool, default: False
            If true, draw the graph via pygraphviz
        verbose : bool, default: False
            If true, give more verbose output
        '''

        # check if the graph already exists, otherwise build it
        try:
            self._graph
            if verbose:
                print('│ Area hierarchy graph is already built')
        except AttributeError:
            if verbose:
                print('│ Building Area hierarchy graph')
            self.build_graph()

        if draw:
            if verbose:
                print('│ Drawing Area hierarchy graph')
            self.draw()

    def build_graph(self):
        from .environment_graph import EnvironmentGraph
        eg = EnvironmentGraph()
        self._graph = eg.get_subgraph(type='a')
