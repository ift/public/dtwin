# This file is part of dtwin.
#
# dtwin is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# dtwin is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Copyright (C) 2021 Max Planck Society
#
# dtwin is being developed at the Max Planck Institute for Astrophysics

from ..patterns import Singleton
from abc import abstractmethod

class GraphBase(Singleton):
    ''' Base class for graphs 

    node data:
        'type' : node type
        'visible' : True|False if node type is 'o' or 'p'
        'adjustable' : True|False if node type is 'p'

    edge data:
        'type' : edge type

    possible node types:
        'w' : instances of WaypointBase or inherited classes
        'c' : instances of Collection or inherited classes
        'a' : instances of AreaBase or inherited classes
        'i' : instances of InterfaceBase or inherited classes
        'o' : instances of ObservableBase or inherited classes
        'p' : instances of ParameterBase or inherited classes

    possible edge types:
        'wc' : preceding Waypoint to Collection/Queue
        'cw' : Collection/Queue to succeeding Waypoint
        'wa' : Waypoint (Area's entry) to Area
        'aw' : Area to Waypoint (Area's exit)
        'aa' : Area to its parent Area
        'ai' : Area to associated interface
        'wi' : Waypoint to associated interface
        'oo' : Observable to dependent Observable
        'po' : Parameter to dependent Observable
        'ip' : Interface to setable Parameter
        'ao' : Area to associated Observable
        'wo' : Waypoint to associated Observable

    '''
    @property
    def graph(self):
        return self._graph

    @abstractmethod
    def build_graph(self):
        pass

    def filter_nodes(self,graph=None,**kwargs):
        ''' Filter the graph by keywords, return list of matching nodes.
        '''
        filters = [] # will be list of (key,list)-tuples

        if graph is None:
            graph=self._graph

        for key,value in kwargs.items():
            if not isinstance(value,(set,list,tuple)):
                value = [value]
            filters.append((key,value))

        nodes = [n for n,d in graph.nodes(data=True)\
                 if all([d[k] in v for k,v in filters])]

        return nodes

    def get_subgraph(self,graph=None,**kwargs):
        ''' Return the subgraph consisting of nodes matching filter.
        '''

        if graph is None:
            graph=self._graph

        nodes = self.filter_nodes(graph=graph,**kwargs)
        return graph.subgraph(nodes)

    def draw(self,graph=None,add_clusters=False,out_path=None):
        import networkx as nx
        try:
            import pygraphviz as pgv
        except ImportError:
            print('pygraphviz is not installed, cannot draw graph')
            return

        if graph is None:
            graph=self._graph

        if len(graph) == 0:
            return

        if out_path is None:
            out_path = f'{self.__class__.__name__}.pdf'

        agraph = nx.nx_agraph.to_agraph(graph)

        agraph.graph_attr.update({'newrank':'true'})

        if add_clusters:
            w = self.filter_nodes(type='w')
            c = self.filter_nodes(type='c')
            a = self.filter_nodes(type='a')
            i = self.filter_nodes(type='i')
            o = self.filter_nodes(type='o')
            p = self.filter_nodes(type='p')

            wc_sub = agraph.add_subgraph(w+c,\
                    style='filled',\
                    color='lightgrey',\
                    name='cluster_wc',\
                    rank='same',\
                    label='waypoints and collections')
            a_sub  = agraph.add_subgraph(a,\
                    style='filled',\
                    color='lightgrey',\
                    name='cluster_a',\
                    rank='same',\
                    label='areas')
            op_sub = agraph.add_subgraph(o+p,\
                    style='filled',\
                    color='lightgrey',\
                    name='cluster_op',\
                    label='data model')
            i_sub  = agraph.add_subgraph(i,\
                    style='filled',\
                    color='lightgrey',\
                    name='cluster_i',\
                    rank='same',\
                    label='interfaces')

        try:
            agraph.layout(prog='dot',args='-Gsplines=ortho')
            agraph.draw(path=out_path)
        except OSError:
            # due to splines=ortho, the following error occurs sometimes
            # (but not always)
            # OSError: newtrap: Trapezoid-table overflow 301
            import warnings
            s = f"│ ! {self.__class__.__name__} could not draw its graph"
            warnings.warn(s,RuntimeWarning)
