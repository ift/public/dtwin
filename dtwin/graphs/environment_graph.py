# This file is part of dtwin.
#
# dtwin is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# dtwin is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Copyright (C) 2021 Max Planck Society
#
# dtwin is being developed at the Max Planck Institute for Astrophysics

from .graph_base import GraphBase

class EnvironmentGraph(GraphBase):
    ''' The graph of the whole environment, including everything.
    '''
    def __init__(self,verbose=False,draw=False):
        ''' The graph of the whole environment, including everything.

        Parameters:
        ==========
        draw : bool, default: False
            If true, draw the graph via pygraphviz
        verbose : bool, default: False
            If true, give more verbose output
        '''

        try:
            self._graph
            if verbose:
                print('│ Environment graph is already built')
        except:
            if verbose:
                print('│ Building environment graph')
            self.build_graph()

        if draw:
            if verbose:
                print('│ Drawing environment graph')
            self.draw()
    
    def build_graph(self):
        from ..waypoints.waypoint_base import WaypointBase
        from ..queues.abstract import Collection
        from ..areas.area_base import AreaBase
        from ..interfaces.interface_base import InterfaceBase
        from ..random.observable_base import ObservableBase
        from ..parameters.parameter_base import ParameterBase

        import networkx as nx

        w = WaypointBase.get_instances(include_subclasses=True,\
                                       flatten=True,return_set=True,\
                                       _filter=lambda x: not x.abstract)

        c = Collection.get_instances(include_subclasses=True,\
                                     flatten=True,return_set=True,\
                                     _filter=lambda x: not x.abstract)

        a = AreaBase.get_instances(include_subclasses=True,\
                                   flatten=True,return_set=True,\
                                   _filter=lambda x: not x.abstract)

        i = InterfaceBase.get_instances(include_subclasses=True,\
                                        flatten=True,return_set=True,\
                                        _filter=lambda x: not x.abstract)

        o = ObservableBase.get_instances(include_subclasses=True,\
                                         flatten=True,return_set=True,\
                                         _filter=lambda x: not x.abstract)

        p = ParameterBase.get_instances(include_subclasses=True,\
                                        flatten=True,return_set=True,\
                                        _filter=lambda x: not x.abstract)
    
        self._graph = nx.DiGraph()

        # add all instances as nodes
        for waypoint in w:
            self._graph.add_node(waypoint,\
                    type='w',\
                    shape='ellipse',\
                    style='filled',\
                    fillcolor='pink')

        for interface in i:
            self._graph.add_node(interface,\
                    type='i',\
                    shape='rpromoter',\
                    style='filled',\
                    fillcolor='yellow')

        for area in a:
            self._graph.add_node(area,\
                    type='a',\
                    shape='house',\
                    style='filled',\
                    fillcolor='skyblue')

        for collection in c:
            self._graph.add_node(collection,\
                    type='c',\
                    shape='cds',\
                    style='filled',\
                    fillcolor='peachpuff')

        for observable in o:
            visibility = observable.visible
            color = 'aquamarine' if visibility else 'coral'
            self._graph.add_node(observable,\
                    type='o',\
                    shape='doubleoctagon',\
                    style='filled',\
                    fillcolor=color,\
                    visible=visibility)

        for parameter in p:
            visibility = parameter.visible
            adjustability = parameter.adjustable
            leftcolor = 'aquamarine' if visibility else 'coral'
            rightcolor = 'aquamarine' if adjustability else 'coral'
            fillcolor = f'{leftcolor};0.5:{rightcolor}'
            self._graph.add_node(parameter,\
                    type='p',\
                    style='filled',\
                    shape='octagon',\
                    gradientangle='45',\
                    #color=color,\
                    fillcolor=fillcolor,\
                    visible=visibility,\
                    adjustable=adjustability)
    
        # add edges between waypoints and collections
        for coll in c:
            self._graph.add_edge(coll.prev,coll,\
                    type='wc')
            self._graph.add_edge(coll,coll.next,\
                    type='cw')
    
        # add edges between waypoints and areas
        for area in a:
            for entry in area.begins_at:
                self._graph.add_edge(entry,area,\
                        type='wa',\
                        style='dashed',\
                        arrowhead='empty')
            for exit in area.ends_at:
                self._graph.add_edge(area,exit,\
                        type='aw',\
                        style='dashed',\
                        arrowhead='empty')
            for observable in area.observables:
                self._graph.add_edge(area,observable,\
                        type='ao',\
                        arrowhead='dot',\
                        style='dashed')
            for interface in area.interfaces:
                self._graph.add_edge(area,interface,\
                        type='ai',\
                        arrowhead='odot',\
                        style='dashed')
            if area.parent is not None:
                self._graph.add_edge(area,parent,\
                        type='aa',\
                        )

        # add edges between waypoints, observables and interfaces
        # timestamps are NOT stored in waypoint's observables attribute
        for waypoint in w:
            self._graph.add_edge(waypoint,waypoint.timestamp,\
                        type='wo',\
                        arrowhead='dot',\
                        style='dashed')
            for observable in waypoint.observables:
                self._graph.add_edge(waypoint,observable,\
                        type='wo',\
                        arrowhead='dot',\
                        style='dashed')
            for interface in waypoint.interfaces:
                self._graph.add_edge(waypoint,interface,\
                        type='wi',\
                        arrowhead='odot',\
                        style='dashed')

        # edges between parameters, observables and their parents
        for observable in o:
            for parameter in observable.parameters:
                self._graph.add_edge(parameter,observable,\
                        type='po')
            for dependency in observable.needs:
                self._graph.add_edge(dependency,observable,\
                        type='oo')
        for parameter in p:
            if parameter.interface is not None:
                self._graph.add_edge(parameter.interface,parameter,\
                        type='pi',\
                        arrowhead='odot',\
                        style='dashed')

        # remove None node if existent
        if None in self._graph.nodes:
            self._graph.remove_node(None)

