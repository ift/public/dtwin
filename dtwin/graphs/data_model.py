# This file is part of dtwin.
#
# dtwin is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# dtwin is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Copyright (C) 2021 Max Planck Society
#
# dtwin is being developed at the Max Planck Institute for Astrophysics

from .graph_base import GraphBase

class DataModel(GraphBase):
    ''' Graphs visualising the data model as graphical model
    '''
    def __init__(self,draw=False,verbose=False):
        ''' Graphs visualising the data model as graphical model

        Parameters:
        ==========
        draw : bool, default: False
            If true, draw the graph via pygraphviz
        verbose : bool, default: False
            If true, give more verbose output
        '''

        try:
            self._graph
            if verbose:
                print('│ Graphical model is already built')
        except AttributeError:
            if verbose:
                print('│ Building graphical model')
            self.build_graph()

        if draw:
            if verbose:
                print('│ Graphical model is already built')
            self.draw()

    def build_graph(self):
        from .environment_graph import EnvironmentGraph
        eg = EnvironmentGraph()

        # the data model consists of Observables and Parameters, only
        self._graph = eg.get_subgraph(type=['o','p'])
