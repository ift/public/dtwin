# This file is part of dtwin.
#
# dtwin is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# dtwin is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Copyright (C) 2021 Max Planck Society
#
# dtwin is being developed at the Max Planck Institute for Astrophysics

import sys
import pickle

from .interface_base import RInterfaceBase, WInterfaceBase

from ..events.time import Time

_time = Time()

class StdOut(WInterfaceBase):
    '''
    Pipes the information to stdout.
    '''
    def __init__(self,mode='str'):
        '''
        Parameters:
        -----------
        mode : 'str' | 'pickle' 
            How the data shall be represented in stdout.
            'str' applies Python's str function for conversion to string.
            'pickle' applies pickle.dumps for conversion to bytes.
        '''
        self.mode = mode

        if mode == 'str':
            self.encrypt = str
            self.channel = sys.stdout
            self.EOT     = '\n'
        elif mode == 'pickle':
            self.encrypt = pickle.dumps
            self.channel = sys.stdout.buffer
            self.EOT     = b'\n\x04'
        else:
            raise AttributeError("mode must either be 'str' or 'pickle'")
            
        self.connect()

    def connect(self):
        # no connection needed
        pass

    def send(self,msg):
        msg = self.encrypt(msg)
        self.channel.write(msg)
        self.channel.write(self.EOT)
        self.channel.flush()

class StdErr(WInterfaceBase):
    '''
    Pipes the information to stderr.
    '''
    def __init__(self,mode='str'):
        '''
        Parameters:
        -----------
        mode : 'str' | 'pickle' 
            How the data shall be represented in stdout.
            'str' applies Python's str function for conversion to string.
            'pickle' applies pickle.dumps for conversion to bytes.
        '''
        self.mode = mode

        if mode == 'str':
            self.encrypt = str
            self.channel = sys.stderr
            self.EOT     = '\n'
        elif mode == 'pickle':
            self.encrypt = pickle.dumps
            self.channel = sys.stderr.buffer
            self.EOT     = b'\n\x04'
        else:
            raise AttributeError("mode must either be 'str' or 'pickle'")
            
        self.connect()

    def connect(self):
        # no connection needed
        pass

    def send(self,msg):
        msg = self.encrypt(msg)
        self.channel.write(msg)
        self.channel.write(self.EOT)
        self.channel.flush()

class StdIn(RInterfaceBase):
    '''
    Reads from stdin.
    '''
    def __init__(self,mode='str',demodulation=None):
        '''
        Parameters:
        -----------
        mode : 'str' | 'pickle' 
            How the data shall be represented in stdout.
            'str' applies Python's str function for conversion to string.
            'pickle' applies pickle.dumps for conversion to bytes.
        demodulation : function or None
            If mode=='str': a function which can process the incoming strings.
            If mode=='pickle': ignored (demodulation via pickle.loads)
            Default: None (is mapped to identity for strings)
        '''
        self.mode = mode

        if mode == 'str':
            self.channel = sys.stdin
            if demodulation is None:
                self.demod = lambda x: x
            else:
                self.demod = demodulation
        elif mode == 'pickle':
            self.demod = pickle.loads
            self.channel = sys.stdin.buffer
        else:
            raise AttributeError("mode must either be 'str' or 'pickle'")
            
        self.connect()

    def connect(self):
        # no connection needed
        pass

    def recv(self):
        msg = self.channel.read()
        msg = self.demod(msg)
        return msg

