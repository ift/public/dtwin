# This file is part of dtwin.
#
# dtwin is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# dtwin is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Copyright (C) 2021 Max Planck Society
#
# dtwin is being developed at the Max Planck Institute for Astrophysics

from sqlalchemy import Column, Boolean, Float, Integer, String, DateTime,\
        ForeignKey, create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from ..patterns import Singleton, FrozenClass

import datetime, sys

Base = declarative_base()

class DatabaseMaps(Singleton,FrozenClass):
    ''' Refers python objects to their respective database objects '''
    def __init__(self):
        # hasattr doesn't work here, as it also checks class attributes
        if '_isfrozen' not in self.__dict__:
            self.container_name_to_dbobj = {}
            self.area_name_to_dbobj = {}
            self.waypoint_name_to_dbobj = {}
            self.name_is_waypoint = {}
            self.collection_name_to_dbobj = {}
            self.observable_name_to_dbobj = {}
            self.parameter_name_to_dbobj = {}
            self.datatype_name_to_dbobj = {}
            self.category_name_to_dbobj = {}
            self.process_name_to_dbobj = {}
            self.statistic_name_to_dbobj = {}
            self.warning_desc_to_dbobj = {}

            self._freeze()

class ReprMixin:
    ''' Some __repr__ method for SQLAlchemy classes'''
    def __repr__(self):
        if hasattr(self,'name'):
            n = str(self.name)
        else:
            n = '(unnamed)'
        return f'<{self.__class__.__name__} {n}>'

###############################################################################
# Structure storage

class ContainerTable(Base,ReprMixin):
    ''' Table for available waypoints '''
    __tablename__ = 'containers'

    id         = Column(Integer, primary_key=True)
    name       = Column(String(64),unique=True)
    class_     = Column(String(64))

    created_at = Column(Float)
    created_in = Column(Integer, ForeignKey('waypoints.id'))
    deleted_at = Column(Float)
    deleted_in = Column(Integer, ForeignKey('waypoints.id'))

    def __init__(self,**kwargs):
        super(ContainerTable,self).__init__(**kwargs)
        DatabaseMaps().container_name_to_dbobj[self.name] = self

class StackingTable(Base,ReprMixin):
    ''' Keeps track of (un)stacking of containers '''
    __tablename__ = 'stacking'

    id        = Column(Integer, primary_key=True)
    parent    = Column(Integer, ForeignKey('containers.id'), nullable=False)
    child     = Column(Integer, ForeignKey('containers.id'), nullable=False)
    stack     = Column(Boolean, nullable=False)
    location  = Column(Integer, ForeignKey('waypoints.id'), nullable=False)
    position  = Column(Integer)
    timestamp = Column(Float, nullable=False)

class AreaWaypointMap(Base):
    ''' Maps entry/exit waypoints and areas '''
    __tablename__ = 'area_waypoint_map'

    id       = Column(Integer, primary_key=True)
    waypoint = Column(Integer, ForeignKey('waypoints.id'))
    area     = Column(Integer, ForeignKey('areas.id'))
    isEntry  = Column(Boolean) # true if waypoint is entry to area

class WaypointTable(Base,ReprMixin):
    ''' Table for available waypoints '''
    __tablename__ = 'waypoints'
    
    id     = Column(Integer, primary_key=True)
    name   = Column(String(64),unique=True)

    def __init__(self,**kwargs):
        super(WaypointTable,self).__init__(**kwargs)
        DatabaseMaps().waypoint_name_to_dbobj[self.name] = self
        DatabaseMaps().name_is_waypoint[self.name] = True

class CollectionTable(Base,ReprMixin):
    ''' Table for available collections '''
    __tablename__ = 'collections'
    
    id    = Column(Integer, primary_key=True)
    name  = Column(String(64),unique=True)
    next  = Column(Integer, ForeignKey('waypoints.id'))
    prev  = Column(Integer, ForeignKey('waypoints.id'))

    def __init__(self,**kwargs):
        super(CollectionTable,self).__init__(**kwargs)
        DatabaseMaps().collection_name_to_dbobj[self.name] = self

class AreaTable(Base,ReprMixin):
    ''' Table for available areas '''
    __tablename__ = 'areas'
    
    id        = Column(Integer, primary_key=True)
    name      = Column(String(64),unique=True)
    parent    = Column(Integer, ForeignKey('areas.id'))

    def __init__(self,**kwargs):
        super(AreaTable,self).__init__(**kwargs)
        DatabaseMaps().area_name_to_dbobj[self.name] = self
        DatabaseMaps().name_is_waypoint[self.name] = False


###############################################################################
# Parameters and Observables

class ParameterTable(Base,ReprMixin):
    ''' Table for available parameters '''
    __tablename__ = 'parameters'

    id       = Column(Integer, primary_key=True)
    name     = Column(String(128),unique=True)
    dtype    = Column(Integer, ForeignKey('datatypes.id'))

    def __init__(self,**kwargs):
        super(ParameterTable,self).__init__(**kwargs)
        DatabaseMaps().parameter_name_to_dbobj[self.name] = self

class ObservableTable(Base,ReprMixin):
    ''' Table for available observables '''
    __tablename__ = 'observables'

    id       = Column(Integer, primary_key=True)
    name     = Column(String(128),unique=True)
    dtype    = Column(Integer, ForeignKey('datatypes.id'))
    waypoint = Column(Integer, ForeignKey('waypoints.id'))
    area     = Column(Integer, ForeignKey('areas.id'))

    def __init__(self,**kwargs):
        super(ObservableTable,self).__init__(**kwargs)
        DatabaseMaps().observable_name_to_dbobj[self.name] = self


###############################################################################
# Data storage

class MovementTable(Base, ReprMixin):
    __tablename__ = 'movements'

    id          = Column(Integer, primary_key=True)
    container   = Column(Integer, ForeignKey('containers.id'))
    waypoint    = Column(Integer, ForeignKey('waypoints.id'))
    timestamp   = Column(Float, nullable=False)

class WarningTable(Base,ReprMixin):
    ''' Table for warnings '''
    __tablename__ = 'warnings'

    id          = Column(Integer, primary_key=True)
    timestamp   = Column(Float)
    location    = Column(String(128))
    description = Column(String(256))

    def __init__(self,**kwargs):
        super(WarningTable,self).__init__(**kwargs)
        DatabaseMaps().warning_desc_to_dbobj[self.description] = self

class DataTypeTable(Base,ReprMixin):
    ''' Table for available data types '''
    __tablename__ = 'datatypes'
    
    id    = Column(Integer, primary_key=True)
    name  = Column(String(32),unique=True)

    def __init__(self,**kwargs):
        super(DataTypeTable,self).__init__(**kwargs)
        DatabaseMaps().datatype_name_to_dbobj[self.name] = self

class ParameterValueTable(Base,ReprMixin):
    ''' Table for values of parameters '''
    __tablename__ = 'parametervalues'

    id      = Column(Integer, primary_key=True)
    par     = Column(Integer, ForeignKey('parameters.id'))
    vfloat  = Column(Float)
    vint    = Column(Integer)
    vstring = Column(String(128))
    vcat    = Column(Integer, ForeignKey('categories.id'))

class CategoryTable(Base,ReprMixin):
    ''' Table for available values of categorical variables '''
    __tablename__='categories'
    
    id      = Column(Integer,primary_key=True)
    name    = Column(String(256))

    def __init__(self,**kwargs):
        super(CategoryTable,self).__init__(**kwargs)
        DatabaseMaps().category_name_to_dbobj[self.name] = self

class ObservableValueTable(Base,ReprMixin):
    ''' Table for values of observables '''
    __tablename__ = 'observablevalues'

    id          = Column(Integer, primary_key=True)
    container   = Column(Integer, ForeignKey('containers.id'), nullable=False)
    observable  = Column(Integer, ForeignKey('observables.id'), nullable=False)

    # value may be float, int, str or Category
    vfloat      = Column(Float)
    vint        = Column(Integer)
    vstring     = Column(String(128))
    vcat        = Column(Integer, ForeignKey('categories.id'))

    # the time of application, or start and end (optional)
    start       = Column(Float, nullable=False)
    end         = Column(Float)

    def __init__(self,dic):
        super(ObservableValueTable,self).__init__()

        dbmap   = DatabaseMaps()
        obs     = dic['observable']
        db_obs  = dbmap.observable_name_to_dbobj[obs]
        val     = dic['value']
        cont    = dic['container']
        db_cont = dbmap.container_name_to_dbobj[cont]

        self.start = dic['timestamp']
        self.end   = dic['end']

        self.observable = db_obs.id
        self.container  = db_cont.id

        self.vfloat  = None  
        self.vint    = None
        self.vstring = None
        self.vcat    = None

        if db_obs.dtype == dbmap.datatype_name_to_dbobj['Category'].id:
            self.vcat = dbmap.category_name_to_dbobj[dic['value']].id
        elif db_obs.dtype == dbmap.datatype_name_to_dbobj['Float'].id:
            self.vfloat = val
        elif db_obs.dtype == dbmap.datatype_name_to_dbobj['Integer'].id:
            self.vint = val
        elif db_obs.dtype == dbmap.datatype_name_to_dbobj['String128'].id:
            self.vstring = val
        else:
            #for k,v in dbmap.datatype_name_to_dbobj.items():
            #    print(f'{k}:{v.id}')
            raise TypeError(f'No matching datatype for {db_obs.dtype}')

#class ProcessTable(Base,ReprMixin):
#    ''' Table for processes '''
#    __tablename__ = 'processes'
#
#    id      = Column(Integer, primary_key=True)
#    name    = Column(String(256))
#
#    def __init__(self,**kwargs):
#        super(ProcessTable,self).__init__(**kwargs)
#        DatabaseMaps().process_name_to_dbobj[self.name] = self

class ProcessStatisticTable(Base,ReprMixin):
    ''' Table for process statistics '''
    __tablename__ = 'statistic'

    id      = Column(Integer, primary_key=True)
    name    = Column(String(256))
    area    = Column(Integer, ForeignKey('areas.id'))
    process = Column(Integer, ForeignKey('observables.id'))

    def __init__(self,**kwargs):
        super(ProcessStatisticTable,self).__init__(**kwargs)
        DatabaseMaps().statistic_name_to_dbobj[self.name] = self

def initialise_database(dbconfig,verbose=False):
    ''' Initialises the database '''
    from ..containers.container_base import ContainerBase
    from ..waypoints.waypoint_base import WaypointBase
    from ..random.observable_base import ObservableBase
    from ..parameters.parameter_base import ParameterBase
    from ..areas.area_base import AreaBase
    from ..queues.abstract import Collection
    from ..distributions.categorical import Categorical

    # This must be called AFTER all structures have been created / linked

    engine = create_engine(dbconfig)
    session = sessionmaker(bind=engine,expire_on_commit=False)()

    Base.metadata.drop_all(engine)
    Base.metadata.create_all(engine)
    
    # insert data types
    if verbose: print('mapping data types')
    typs = ['Float','Integer','String128','Category']
    dtypes = {}
    for t in typs:
        dtypes[t] = DataTypeTable(name=t)
        session.add(dtypes[t])
    session.commit()

    # waypoints
    if verbose: print('mapping waypoints')
    objs = WaypointBase.get_instances(include_subclasses=True)
    wp_name_id = {}
    queue_next_wp = {}
    queue_prev_wp = {}
    for rep,obj in objs.items():
        dbobj = WaypointTable(name=obj.name)
        obj._dbid = dbobj.id
        wp_name_id[obj.name] = dbobj
        session.add(dbobj)
        for q in obj.next:
            queue_prev_wp[q.name] = {'name':obj.name,'dbobj':dbobj}
        for q in obj.prev:
            queue_next_wp[q.name] = {'name':obj.name,'dbobj':dbobj}
    session.commit()

    # collections
    if verbose: print('mapping collections')
    queue_name_id = {}
    objs = Collection.get_instances(\
            include_subclasses=True,\
            _filter = lambda x: not x.abstract, # exclude abstract queues
            )
    for rep,obj in objs.items():
        nid   = queue_next_wp[obj.name]['dbobj'].id
        pid   = queue_prev_wp[obj.name]['dbobj'].id
        dbobj = CollectionTable(name=obj.name,next=nid,prev=pid)
        queue_name_id[obj.name] = dbobj
        session.add(dbobj)
    session.commit()

    ## areas
    if verbose: print('mapping areas')
    area_name_id = {}
    objs = AreaBase.get_instances(include_subclasses=True)
    for rep,obj in objs.items():
        #begins_at = [wp.name for wp in area.begins_at]
        #ends_at   = [wp.name for wp in area.ends_at]
        dbobj = AreaTable(name=obj.name,parent=obj.parent)
        area_name_id[obj.name] = dbobj
        session.add(dbobj)
    session.commit()

    # make a map for dtypes
    dtype_map = {
            bool:       'Boolean',
            'bool':     'Boolean',
            'Boolean':  'Boolean'
            ,
            str:        'String128',
            'str':      'String128',
            'String128':'String128'
            ,
            int:        'Integer',
            'int':      'Integer',
            'Integer':  'Integer'
            ,
            float:      'Float',
            'float':    'Float',
            'Float':    'Float'
            ,
            'cat':      'Category',
            'category': 'Category',
            'Category': 'Category',
            Categorical:'Category',
            }

    ## parameters
    if verbose: print('mapping parameter')
    objs = ParameterBase.get_instances(include_subclasses=True)
    for rep,obj in objs.items():
        if dtype_map[obj.dtype] == 'Category':
            raise NotImplementedError
        dtype_id = dtypes[dtype_map[obj.dtype]].id
        dbobj    = ParameterTable(name=obj.name,dtype=dtype_id)
        session.add(dbobj)
    session.commit()

    ## observables
    if verbose: print('mapping observables')
    objs = ObservableBase.get_instances(include_subclasses=True)
    for rep,obj in objs.items():
        if dtype_map[obj.dtype] == 'Category':
            # insert any categorical values into the database
            for c in obj._dist.categories:
                cdbobj = CategoryTable(name=c)
                session.add(cdbobj)
        dtype=dtypes[dtype_map[obj.dtype]].id

        waypoint = None
        area = None

        if isinstance(obj.parent,WaypointBase):
            waypoint = wp_name_id[obj.parent.name].id
        elif isinstance(obj.parent,AreaBase):
            area = area_name_id[obj.parent.name].id
        else:
            raise TypeError('No parent identfied')

        dbobj = ObservableTable(\
                name=obj.name,\
                dtype=dtype,\
                waypoint=waypoint,\
                area=area)
        session.add(dbobj)
    session.commit()

    ## containers (if existent at beginning)
    objs = ContainerBase.get_instances(include_subclasses=True)
    for rep,obj in objs.items():
        dbobj = ContainerTable(\
                name=obj.name,\
                class_=obj.class_)
        session.add(dbobj)
    session.commit()

    if verbose:
        try:
            from eralchemy import render_er
            render_er(Base,'er_diagram.pdf')
        except ImportError:
            print('Could not draw ER diagram of database')
            pass

tables = [ContainerTable,StackingTable,WaypointTable,CollectionTable,AreaTable,
          ParameterTable,ObservableTable,MovementTable,WarningTable,
          DataTypeTable,ParameterValueTable,CategoryTable,ObservableValueTable]
tables = [(m.__tablename__,m) for m in tables]
