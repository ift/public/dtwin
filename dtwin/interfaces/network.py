# This file is part of dtwin.
#
# dtwin is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# dtwin is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Copyright (C) 2021 Max Planck Society
#
# dtwin is being developed at the Max Planck Institute for Astrophysics

import json
import asyncio
from .interface_base import RInterfaceBase, WInterfaceBase, RWInterfaceBase,\
        DBInterface
from ..events.time import Time
from ..errors import TimeTravel
import time

_time = Time()

class SocketClient(RWInterfaceBase):
    '''TCP/IP client
    '''
    def __init__(\
            self,callback,host='localhost',port='8888',\
            encoding='utf-8',connect=True,separator=b'\x00',\
            ):

        super(SocketClient,self).__init__()

        self._encoding = encoding
        self._host = host
        self._port = port
        self._callback = callback
        self._sep = separator
        self.n = 0

    async def connect(self):
        try:
            self._reader, self._writer = await asyncio.open_connection(\
                    self._host,self._port)
        except ConnectionRefusedError:
            print(f'Connection to {self._host}:{self._port} refused. Check configuration.')
            return
        print(f'opened connection to {self._host}:{self._port}')

        init_msg = {'type':'connect'}
        await self.send(init_msg)

        await self.send({'type':'get'})

        try:
            while True:
                data = await self.recv()
                await self.handle_message(data)
        except KeyboardInterrupt:
            print(f'Closing connection to {self._host}:{self._port}')
        except:
            print(f'Lost connection to {self._host}:{self._port}')

    async def handle_message(self,dic):
        assert dic['type'] in (\
                'get','application','movement','stacking',\
                'creation','deletion','warning','ack set')

        await self._callback(dic,self)

    async def send(self,dic):
        msg = self._encode(dic)
        self._writer.write(msg)
        self._writer.write(self._sep)
        await self._writer.drain()

    async def recv(self):
        msg = await self._reader.readuntil(self._sep)
        msg = msg[:-len(self._sep)]
        dic  = self._decode(msg)
        return dic

    def _decode(self,msg):
        return json.loads(msg.decode(self._encoding))

    def _encode(self,data):
        return json.dumps(data).encode(self._encoding)

    async def close(self):
        self._writer.close()
        await self._writer.wait_closed()

class SocketServer(RWInterfaceBase):
    '''TCP/IP server
    '''
    def __init__(self,callback=None,host='localhost',\
                       port='8888',encoding='utf-8',sep=b'\x00'):

        super(SocketServer,self).__init__()

        self._host = host
        self._port = port
        self._encoding = encoding
        if callback is None:
            def cb(reader,writer):
                pass
            self._callback = cb
        else:
            self._callback = callback
        self._sep = sep
        self._connections = []

    async def connect(self):
        await self.start_server()

    async def start_server(self):
        self._server = await asyncio.start_server(self._handle_client,\
                                                  self._host,\
                                                  self._port)
        addr = self._server.sockets[0].getsockname()
        print(f'Started TCP server listening at {addr!r}')
        async with self._server:
            await self._server.serve_forever()

    async def _handle_client(self,reader,writer):
        try:
            data = await reader.readuntil(self._sep)
            data = data[:-len(self._sep)]
            addr = writer.get_extra_info('peername')
            print(f'Incoming connection from {addr!r}')
        except:
            print('Lost connection to host')

        try:
            dic = self._decode(data)
            msg_type = dic['type']
        except:
            print('Incoming data could not be decoded.')
            return False

        if not msg_type == 'connect':
            writer.write('You must connect first'.encode())
            writer.write(self._sep)
            await writer.drain()
            writer.close()
            return False

        self._connections.append((reader,writer))

        try:
            while True:
                data = await reader.readuntil(self._sep)
                data = data[:-len(self._sep)]
                dic  = self._decode(data)
                await self._handle_data(dic,writer)
        except (ConnectionResetError,
                asyncio.streams.IncompleteReadError,
                BrokenPipeError):
            if (reader,writer) in self._connections:
                self._connections.remove((reader,writer))
            print(f'Connection to {addr!r} closed')

    async def _handle_data(self,dic,writer):
        msg_type = dic['type']

        if msg_type.lower() == 'connect':
            pass

        elif msg_type.lower() == 'get':

            from ..random.observable_base import ObservableBase
            obs = ObservableBase.get_instances(\
                    include_subclasses=True,\
                    flatten=True,\
                    _filter=lambda instance: instance.visible)

            # include visible parameters, only
            from ..parameters.parameter_base import ParameterBase
            par = ParameterBase.get_instances(\
                    include_subclasses=True,\
                    flatten=True,\
                    _filter=lambda instance: instance.visible \
                                         and instance.adjustable)

            res_obs = []
            res_par = []

            for k,v in obs.items():
                oo = {\
                    'name':v.name,\
                    'dtype':v.dtype.__name__,\
                    'parent':v.parent.name,\
                    }
                res_obs.append(oo)

            for k,v in par.items():
                pp = {\
                    'name':v.name,\
                    'dtype':v.dtype.__name__,\
                    }
                res_par.append(pp)

            res = {\
                    'type':'get',\
                    'transmission':_time.now_synced,\
                    'observables':res_obs,\
                    'parameters':res_par,\
                    }

            writer.write(self._encode(res))
            writer.write(self._sep)
            await writer.drain()

        elif msg_type.lower() == 'set':
            updates = dic['parameters']

            from ..parameters.parameter_base import ParameterBase
            par = ParameterBase.get_instances(
                    include_subclasses=True,
                    flatten=True)
            par = {v.name : v for v in par.values()}

            res_dic = dict()

            for k,v in updates.items():
                try:
                    par[k].value = v
                    res_dic.update({k:v})
                except:
                    res_dic.update({k:'error'})
            
            # give 
            res = {\
                'type':'ack set',\
                'transmission':_time.now_synced,\
                'parameters':res_dic,\
                }

            writer.write(self._encode(res))
            writer.write(self._sep)
            await writer.drain()

        elif msg_type.lower() == 'info':
            pass

    async def send(self,dic,writer=None):
        try:
            dic['transmission']
        except KeyError:
            dic['transmission'] = _time.now_synced
        msg = self._encode(dic)
        if writer is not None:
            writer.write(msg)
            writer.write(self._sep)
            await writer.drain()
        else:
            for c in self._connections:
                c[1].write(msg)
                c[1].write(self._sep)
                try:
                    await c[1].drain()
                except (ConnectionResetError,
                        asyncio.streams.IncompleteReadError,
                        BrokenPipeError) as e:
                    self._connections.remove(c)

    async def recv(self,reader,writer):
        data = await reader.readuntil(self._sep)
        data = data[:-len(self._sep)]
        dic  = self._decode(dic)
        return dic
    
    def _decode(self,msg):
        return json.loads(msg.decode(self._encoding))

    def _encode(self,data):
        return json.dumps(data).encode(self._encoding)

    def __repr__(self):
        return f'{self.__class__.__name__} @ {self._host}:{self._port}'
