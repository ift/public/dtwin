# This file is part of dtwin.
#
# dtwin is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# dtwin is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Copyright (C) 2021 Max Planck Society
#
# dtwin is being developed at the Max Planck Institute for Astrophysics

from .interface_base import WInterfaceBase, RWInterfaceBase, DBInterface
from .sqlalchemy_tables import WarningTable, ObservableValueTable,\
        DatabaseMaps, ContainerTable, StackingTable, MovementTable

from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
import sqlalchemy

import json

class RWDatabase(WInterfaceBase,DBInterface):
    ''' Database interface via SQLAlchemy
    '''

    def __init__(self, db_flavor, database, user, pw, url, port='5432',\
                 **kwargs):

        super(RWDatabase,self).__init__()

        self._url = url
        self._port = port
        self._db_spec = f'{db_flavor}://{user}:{pw}@{url}:{port}/{database}'
        self._engine = None
        self.connect()

    def connect(self):
        self.engine = create_engine(self._db_spec)
        self.session = sessionmaker(bind=self.engine,expire_on_commit=False)()

    @property
    def engine(self):
        return self._engine

    @engine.setter
    def engine(self,new):
        if not isinstance(new,sqlalchemy.engine.base.Engine):
            raise TypeError('engine must an instance of sqlalchemy.Engine')
        if self.engine:
            self.engine.close()
        self._engine = new

    async def send(self,msg_dic):
        if self._engine is None:
            self.connect()

        #print(json.dumps(msg_dic,indent=2))

        if msg_dic['type'] == 'warning':
            dbobj = WarningTable(timestamp=msg_dic['timestamp'],\
                                 location=msg_dic['location'],\
                                 description=msg_dic['msg'])
            self.session.add(dbobj)

        elif msg_dic['type'] == 'application':
            #print(json.dumps(msg_dic,indent=2))
            if 'end' in msg_dic:
                end = msg_dic['end']
            else:
                end = None

            for f,v in msg_dic['observables'].items():
                dic = { 'container':str(msg_dic['container']),\
                        'observable':f,\
                        'value':v,\
                        'timestamp':msg_dic['timestamp'],\
                        'end':end,\
                      }
                dbobj = ObservableValueTable(dic)
                self.session.add(dbobj)

        elif msg_dic['type'] == 'creation':
            name  = msg_dic['container']
            class_= msg_dic['class']
            time  = msg_dic['timestamp']
            wp    = msg_dic['waypoint']
            wp    = DatabaseMaps().waypoint_name_to_dbobj[wp].id
            dbobj = ContainerTable(\
                        name=str(name),\
                        class_=class_,\
                        created_at=time,\
                        created_in=wp)
            self.session.add(dbobj)

        elif msg_dic['type'] == 'deletion':
            c     = str(msg_dic['container'])
            t     = msg_dic['timestamp']
            wp    = msg_dic['waypoint']
            wpid  = DatabaseMaps().waypoint_name_to_dbobj[wp].id

            dbobj = DatabaseMaps().container_name_to_dbobj.pop(c)
            #print(dbobj.name)
            #print(dbobj.created_at)
            dbobj.deleted_at = t
            dbobj.deleted_in = wpid

            self.session.add(dbobj)

        elif msg_dic['type'] == 'stacking':
            p    = msg_dic['parent']
            p_db = DatabaseMaps().container_name_to_dbobj[p]

            c    = msg_dic['child']
            c_db = DatabaseMaps().container_name_to_dbobj[c]

            l    = msg_dic['location']
            l_db = DatabaseMaps().waypoint_name_to_dbobj[l]

            p    = msg_dic['position']

            s    = msg_dic['stack']
            t    = msg_dic['timestamp']

            dbobj = StackingTable(parent=p_db.id,child=c_db.id,stack=s,\
                                  timestamp=t,position=p,location=l_db.id)
            self.session.add(dbobj)

        elif msg_dic['type'] == 'movement':
            c    = msg_dic['container']
            c_db = DatabaseMaps().container_name_to_dbobj[c]

            l    = msg_dic['location']
            l_db = DatabaseMaps().waypoint_name_to_dbobj[l]

            t    = msg_dic['timestamp']

            dbobj= MovementTable(container=c_db.id,\
                                 waypoint=l_db.id,\
                                 timestamp=t)
            self.session.add(dbobj)

        else:
            pass
            #print(f'{self}: received message of unknown type')

        self.session.commit()
        #session.close()

    def __repr__(self):
        return f'{self.__class__.__name__} @ {self._url}:{self._port}'
