# This file is part of dtwin.
#
# dtwin is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# dtwin is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Copyright (C) 2021 Max Planck Society
#
# dtwin is being developed at the Max Planck Institute for Astrophysics

from abc import ABCMeta, abstractmethod, abstractproperty
from ..patterns import Multiplet

class InterfaceBase(Multiplet,metaclass=ABCMeta):
    '''
    Abstract interface
    '''

    def __init__(self):
        self._abstract = False

    @property
    def abstract(self):
        ''' Defines abstract objects.
        Abstract objects are not checked against consistency and
        take not part in the environment's graphs
        '''
        return self._abstract

    @abstract.setter
    def abstract(self,new):
        if not isinstance(new,bool):
            raise TypeError
        self._abstract = new

    @abstractmethod
    def connect(self):
        pass
    
    def add_to_objects(self,filter_=lambda x: True):
        ''' Adds the interface to objects matching the filter '''
        from ..waypoints.waypoint_base import WaypointBase as WB
        from ..areas.area_base import AreaBase as AB
        from ..queues.abstract import Collection as C
        from ..random.observable_base import ObservableBase as OB
        from ..parameters.parameter_base import ParameterBase as PB
        
        obj_list = []
        for cls in [WB,AB,C,OB,PB]:
            cls_objs = cls.get_instances(include_subclasses=True)
            for k,obj in cls_objs.items():
                obj_list.append(obj)
        
        obj_list = [o for o in obj_list if filter_(o)]
        
        for obj in obj_list:
            obj.interfaces = set((self,)).union(obj.interfaces)

    @classmethod
    def check_consistency(cls,verbose=False):
        dbifs = cls.get_instances(\
                  include_subclasses=True,\
                  return_set=True,\
                  _filter=lambda obj: isinstance(obj,DBInterface))

        # initialise the database, if any database interface is created
        for dbif in dbifs:
            if verbose:
                print('| Initialising databases')
            from .sqlalchemy_tables import initialise_database
            initialise_database(dbif._db_spec)

    def __repr__(self):
        return f'{self.__class__.__name__}'

class DBInterface:
    ''' convenience base class for database interfaces '''
    pass

class RInterfaceBase(InterfaceBase,metaclass=ABCMeta):
    '''
    Abstract interface able to read data only.
    '''

    @abstractmethod
    async def recv(self):
        pass

class WInterfaceBase(InterfaceBase,metaclass=ABCMeta):
    '''
    Abstract interface able to send data only.
    '''

    @abstractmethod
    async def send(self):
        pass

class RWInterfaceBase(InterfaceBase,metaclass=ABCMeta):
    '''
    Abstract interface able to read and write.
    '''

    @abstractmethod
    async def send(self):
        pass

    @abstractmethod
    async def recv(self):
        pass
