# This file is part of dtwin.
#
# dtwin is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# dtwin is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Copyright (C) 2021 Max Planck Society
#
# dtwin is being developed at the Max Planck Institute for Astrophysics

from collections import deque
from .abstract import ReservoirBase
from ..distributions.distribution_base import DistributionBase
from ..waypoints.waypoint_base import WaypointBase
from ..events.time import Time
from ..events.temporalevent import TemporalEvent
from ..utils import dprint

_time = Time()

class LIFO(ReservoirBase):
    ''' First In First Out Queue.
    '''
    def __init__(self,name,capacity=None,next_waypoint=None,
                 prev_waypoint=None):
        super(LIFO,self).__init__()
        self._deque = deque()

        self._name = name

        self.next = next_waypoint

        self.prev = prev_waypoint
        
        assert isinstance(capacity,int) or capacity is None
        self._capacity = capacity

    @property
    def name(self):
        return self._name

    @property
    def capacity(self):
        return self._capacity

    @property
    def members(self):
        return list(self._deque)

    @property
    def next(self):
        return self._next_tp

    @next.setter
    def next(self,new):
        assert isinstance(new,WaypointBase) or new is None
        self._next_tp = new
        if new is not None:
            new.prev += [self]

    @property
    def prev(self):
        return self._prev_tp

    @prev.setter
    def prev(self,new):
        assert isinstance(new,WaypointBase) or new is None
        self._prev_tp = new
        if new is not None:
            new.next += [self]

    def _check_pop_conditions(self):
        if len(self) == 0:
            return False
        return True

    def forecast(self,n=1):
        l = len(self._deque)
        n_none = n - l
        if n_none > 0:
            return list(self._deque)[:l] + [None]*n_none
        else:
            return list(self._deque)[:n]

    def pop(self):
        if not self._check_pop_conditions():
            raise IndexError(f'{self._name}: no containers to pop')
        arrival, container = self._deque.popleft()
        try:
            queue_idx = self.next.prev.index(self) 
        except:
            queue_idx = None
        return queue_idx, container

    def push(self,container):
        dprint(f'FIFO {self.name}: {container} is being pushed',level=10)
        self._deque.appendleft((_time.now,container))
        self.last = container

    async def push_next(self):
        dprint(f'FIFO {self.name}: push_next method called',level=10)
        if self.next.condition:
            dprint(f'FIFO {self.name}: Pushing to {self.next}',level=5)
            container = self.popleft()
            self.next.execute(container)
    
    def __len__(self):
        return(len(self._deque))

