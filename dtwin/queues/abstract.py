# This file is part of dtwin.
#
# dtwin is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# dtwin is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Copyright (C) 2021 Max Planck Society
#
# dtwin is being developed at the Max Planck Institute for Astrophysics

from ..patterns import Multiplet
from ..utils import checktype, dprint
from abc import abstractmethod, abstractproperty, ABCMeta

class Collection(Multiplet,metaclass=ABCMeta):
    ''' Base class for queues and reservoirs.
    '''
    def __init__(self):
        self._next = None
        self._prev = None
        self._capacity = None
        self._abstract = False

    @property
    def abstract(self):
        ''' Defines abstract objects.
        Abstract objects are not checked against consistency and
        take not part in the environment's graphs
        '''
        return self._abstract

    @abstract.setter
    def abstract(self,new):
        if not isinstance(new,bool):
            raise TypeError
        self._abstract = new
    
    @abstractproperty
    def name(self):
        ''' The queue's name. '''
        pass 
        
    @abstractproperty
    def capacity(self):
        ''' The capacity of this queue. '''
        pass

    @abstractproperty
    def members(self):
        raise NotImplementedError

    def check_capacity(self,n=1):
        ''' Check whether there's enough capacity for n new Containers. '''
        if self.capacity is None:
            return True
        else:
            return self._capacity >= n + len(self._deque)

    @abstractmethod
    def push(self,container):
        ''' What happens upon pushing a container. '''
        pass

    @abstractmethod
    def forecast(self,n=1):
        ''' Return a list of length(n) of containers which are popped next.
        The first container to be popped is the first container in the list.
        This must NOT remove the containers from the queue. 
        If there are less than n containers in the collection, fill the list 
        with None.
        '''
        pass

    @abstractmethod
    def pop(self):
        ''' What happens upon popping a container. '''
        pass

    @property
    def next(self):
        ''' The next waypoint. '''
        return self._next

    @next.setter
    def next(self,new):
        from ..waypoints.waypoint_base import WaypointBase
        checktype(new,(WaypointBase,type(None)),'next')
        self._next = new
        if new is not None and self not in new.prev:
            new.prev += [self]

    @property
    def prev(self):
        ''' The previous waypoint. '''
        return self._prev

    @prev.setter
    def prev(self,new):
        from ..waypoints.waypoint_base import WaypointBase
        checktype(new,(WaypointBase,type(None)),'prev')
        self._prev = new
        if new is not None and self not in new.next:
            new.next += [self]

    @classmethod
    def check_consistency(cls,verbose=False):
        '''
        * check whether names are unique
        * check whether collections have one entry and one exit
        '''
        collections = cls.get_instances(include_subclasses=True,\
                                        flatten=True,\
                                        return_set=True,\
                                        _filter = lambda x: not x.abstract,\
                                        )

        if len(set([c.name for c in collections])) != len(collections):
            from dtwin.errors import UniquenessError
            raise UniquenessError(f'Collection names are not unique')

        for c in collections:
            if c.next is None or c.prev is None:
                from dtwin.errors import Inconsistency
                err = 'Collections must have one previous and one next waypoint'
                raise Inconsistency(err)

        cls._checked = True
        return True

    @abstractproperty
    def __len__(self):
        ''' Number of containers currently in this Collection. '''
        pass

class QueueBase(Collection):
    ''' Base class for Queues.
    '''
    @abstractproperty
    def distribution(self):
        ''' The time it takes to pass this queue. '''
        pass

    @abstractmethod
    def sample_dt(self,container):
        ''' How time differences are sampled. '''
        pass

class ReservoirBase(Collection):
    ''' Base class for reservoirs.

    Reservoirs are storage places for Containers from which
    multiple containers can be fetched at the same time.
    '''

    pass
