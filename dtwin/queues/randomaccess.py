# This file is part of dtwin.
#
# dtwin is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# dtwin is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Copyright (C) 2021 Max Planck Society
#
# dtwin is being developed at the Max Planck Institute for Astrophysics

from collections import deque
from .abstract import ReservoirBase
from ..distributions.distribution_base import DistributionBase
from ..waypoints.waypoint_base import WaypointBase
from ..events.time import Time
from ..events.temporalevent import TemporalEvent
from ..utils import checktype
import random as _rand

_time = Time()

class RandomAccess(ReservoirBase):
    ''' First In First Out Queue.
    '''
    def __init__(self,name,capacity=None,next_waypoint=None,
                 prev_waypoint=None):
        super(RandomAccess,self).__init__()

        self._deque = deque()

        self._name = name
        self._forecast = [] #indices of forecase containers

        self.next = next_waypoint

        self.prev = prev_waypoint
        
        assert isinstance(capacity,int) or capacity is None
        self._capacity = capacity

    @property
    def name(self):
        return self._name

    @property
    def capacity(self):
        return self._capacity

    @property
    def members(self):
        return [x[1] for x in self._deque]

    def _check_pop_conditions(self):
        if len(self) == 0:
            return False
        return True

    def forecast(self,n=1):
        ''' Forecasts the next n containers to be popped.
        To be consistent with subsequent calls to 'pop', this method stores 
        the forecast containers internally.
        This is required e.g. to check a waypoint's condition on the same 
        containers that are actually sent to them afterwards.
        '''
        f = len(self._forecast)
        q = len(self._deque)

        # number of containers that can be returned
        nn = min(n,q)

        # number of None values returned
        n_none = n-nn

        # number of new samples needed
        l = nn-f

        idx = list(range(len(self._deque)))

        if f>0 and l<=0:
            # there are more containers already forecast than required
            return_idx = self._forecast[:nn]
        elif f>0 and l>0:
            # first remove already forecast containers from index set
            for i in self._forecast:
                idx.remove(i)
            # sample additional containers from the remaining indices
            new_idx = _rand.sample(idx,l)
            # add them to the set of forecast containers
            self._forecast.extend(new_idx)
        else:
            # there are no forecast containers, so sample as much as required
            new_idx = _rand.sample(idx,nn)
            self._forecast.extend(new_idx)

        return [self._deque[i][1] for i in self._forecast[:nn]] + [None]*n_none

    def pop(self):
        if not self._check_pop_conditions():
            raise IndexError(f'{self._name}: no containers to pop')
        f = len(self._forecast)
        if f > 0:
            # if there are forecast containers, take the next one
            nxt = self._forecast.pop(0) #pop FIRST element
            container = self._deque[nxt]
            # adjust forecast indices
            self._forecast = [x if x<nxt else x-1 for x in self._forecast]
        else:
            # sample returns a list, so take its first entry
            container = _rand.sample(self._deque,1)[0]

        self._deque.remove(container)

        try:
            queue_idx = self.next.prev.index(self) 
        except:
            queue_idx = None

        return queue_idx, container[1]

    def push(self,container):
        self._deque.appendleft((_time.now,container))
        self.last = container

        # reset forecast when pushing (otherwise new containers might not
        # be popped equally likely as previously held ones)
        self._forecast = []

    def push_next(self):
        if self.next.condition:
            container = self.pop()[0]
            self.next.execute(container)
    
    def __len__(self):
        return(len(self._deque))

