# This file is part of dtwin.
#
# dtwin is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# dtwin is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Copyright (C) 2021 Max Planck Society
#
# dtwin is being developed at the Max Planck Institute for Astrophysics

from collections import deque
from .abstract import QueueBase
from ..distributions.distribution_base import DistributionBase
from ..waypoints.waypoint_base import WaypointBase
from ..events.time import Time
from ..events.temporalevent import TemporalEvent
from ..events.conditionalevent import ConditionalEvent

_time = Time()

class FIFO(QueueBase):
    ''' First in, first out queue.
    '''
    def __init__(self,name,distribution=None,capacity=None,next_waypoint=None,
                 prev_waypoint=None):
        # the internal queue, holding pairs of (arrival time, container)
        super(FIFO,self).__init__()
        self._deque = deque()

        self._name = name

        self.next = next_waypoint
        self.prev = prev_waypoint
        
        assert isinstance(capacity,int) or capacity is None
        self._capacity = capacity

        assert isinstance(distribution,DistributionBase)
        self._distribution = distribution

    @property
    def name(self):
        return self._name

    @property
    def capacity(self):
        return self._capacity

    @property
    def members(self):
        '''Returns a list of containers currently in this queue
        '''
        return [t[1] for t in self._deque]

    @property
    def distribution(self):
        return self._distribution

    def _schedule_next(self):
        '''Schedules next pop event if possible
        '''
        if not len(self._deque) == 0:
            dt = self._distribution.sample()
            t = _time.now + dt
            a = self.push_next
            event = TemporalEvent(time=t, action=a, push=True)
            return 1
            # beware: the following ensures that time differences between
            # pushing and popping is consistent with the distributions,
            # BUT will occasionally schedule events in the past:
            # arrival = self._deque[-1][0]
            # t = arrival + dt 
        else:
            return 0

    def _check_pop_conditions(self):
        if len(self) == 0:
            return False
        return True

    def forecast(self,n=1):
        l = len(self._deque)
        n_none = n - l
        if n_none > 0:
            return list(self._deque)[-l:][::-1] + [None]*n_none
        else:
            return list(self._deque)[-n:][::-1]

    def pop(self):
        if not self._check_pop_conditions():
            raise IndexError(f'{self._name}: no containers to pop')
        arrival, container = self._deque.pop()
        self._schedule_next()
        try:
            queue_idx = self.next.prev.index(self) 
        except:
            queue_idx = None
        return queue_idx, container

    def push(self,container):
        ''' Pushes containers into this queue
        If there are no containers in this queue yet, this method schedules
        a 'push_next' event according to its temporal distribution.
        '''
        if len(self) == 0:
            dt    = self.sample_dt()
            time  = _time.now + dt
            action= self.push_next
            event = TemporalEvent(action=action,time=time,push=True)
        self._deque.appendleft((_time.now,container))

    async def push_next(self):
        ''' Pops the next container to the next waypoint 
        If the next waypoint cannot operate, a trigger will be set.
        '''

        try:
            arrival, container = self._deque[-1]
        except IndexError:
            import warnings
            warnings.warn(\
                    f'{self.__repr__()}: No container in this queue!',\
                    RuntimeWarning
                    )
            raise RuntimeError
            return

        try:
            inqueue = self.next.prev.index(self)
            canproceed = self.next.condition([(inqueue,container)])
        except IndexError:
            canproceed = False

        if canproceed:
            inqueue_container = self.pop()
            await self.next.execute([inqueue_container])
        else:
            action = self.push_next
            condition = lambda x=container:\
                    self.next.condition([(inqueue,container)])
            trigger = ConditionalEvent(\
                    action=action,condition=condition,push=True)

    def sample_dt(self,size=None):
        return self._distribution.sample(size=size)
    
    def __len__(self):
        return(len(self._deque))

