# This file is part of dtwin.
#
# dtwin is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# dtwin is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Copyright (C) 2021 Max Planck Society
#
# dtwin is being developed at the Max Planck Institute for Astrophysics

'''
Collection of various exceptions and warnings used for try/except statements
'''

class CollectionException(Exception):
    ''' Base class for exceptions in collections '''

class PopError(CollectionException):
    ''' Container cannot be popped from Collection.
    '''
    pass

class PushError(CollectionException):
    ''' Container cannot be pushed to Collection.
    '''
    pass

class NoEventError(IndexError):
    ''' No more future event.
    '''
    pass

class Inconsistency(Exception):
    ''' Base class for exceptions in consistency checks '''
    pass

class AreaFlowError(Inconsistency):
    ''' Area has beginning but no end or vice versa '''
    pass

class UniquenessError(Inconsistency):
    ''' Names are not unique.
    '''
    pass

class CyclicGraphError(Inconsistency):
    ''' Graph is unexpectedly not DAG.
    '''
    pass

class CyclicDataModel(CyclicGraphError):
    ''' Data model has local cyclic dependencies.
    '''
    pass

class HierarchyError(Inconsistency):
    ''' Child/parent relation is missing.
    '''
    pass

class FixedParameterError(Exception):
    ''' A nonadjustable parameter is set a new value.
    '''
    pass

class InconsistencyWarning(Warning):
    ''' Minor inconsistencies which do not always justify an exception.
    '''
    pass

class PerformanceWarning(Warning):
    ''' Simulation cannot keep up with intented speed.
    '''
    pass

class TimeTravel(RuntimeError):
    ''' Indicates attempt to advance simulation time backwards.
    '''
    pass
