# This file is part of dtwin.
#
# dtwin is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# dtwin is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Copyright (C) 2021 Max Planck Society
#
# dtwin is being developed at the Max Planck Institute for Astrophysics

from abc import abstractproperty
from ..patterns import Multiplet

class ConditionalEventBase(Multiplet):
    ''' Base class for events depending on a condition.
    '''
    @abstractproperty
    def action(self):
        ''' The action which is to be triggered when the event occurs.
        '''
        pass

    @abstractproperty
    def condition(self):
        ''' The condition which triggers the action, if True.
        '''
        pass

    @abstractproperty
    def is_persistent(self):
        ''' Whether this trigger keeps active after execution.
        '''
        pass

    def check(self):
        return self._condition()

    def register(self):
        from .triggerlist import TriggerList
        _triggerlist = TriggerList()
        _triggerlist.push(self)

    async def execute(self):
        ''' Execute action.
        '''
        await self.action()
