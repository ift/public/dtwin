# This file is part of dtwin.
#
# dtwin is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# dtwin is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Copyright (C) 2021 Max Planck Society
#
# dtwin is being developed at the Max Planck Institute for Astrophysics

from .temporalevent_base import TemporalEventBase
from .eventqueue import EventQueue

# instantiate the singlet for usage below
_eventqueue = EventQueue()

class TemporalEvent(TemporalEventBase):
    def __init__(self,action,time,recurring_period=None,push=False):
        ''' Implementation of TemporalEventBase.

        Parameters:
        ==========
        action : callable 
            The action to be executed at the scheduled time
        time : float
            Simulation time at which this event is to be executed.
        recurring_period : float or None
            If given, this event will schedule another events after being
            executed. That event will also be recurring with the same period.
        push : bool
            If true, this event will be pushed to the EventQueue after init.
        '''
        self._action = action
        self._time   = time
        self._period = recurring_period
        if push:
            _eventqueue.push(self)

    async def action(self):
        ''' Executes the event's action 
            If recurring_period is set, another (recurring) event with the 
            same action will be scheduled after that period.
        '''
        await self._action()
        if isinstance(self._period,(int,float)) and self._period>0.:
            e = TemporalEvent(\
                    action=self._action,\
                    time=self._time + self._period,\
                    recurring_period = self._period,\
                    push=True)

    @property
    def action_parent_and_name(self):
        ''' Returns the parent and name of the action, if action is bound.
        '''
        method = self._action
        try:
            # if method is bound, get the object directly
            parent = method.__self__
            name   = method.__name__
        except AttributeError:
            # if not, try whether functools.partial was used
            try:
                parent = method.func.__self__
                name   = method.__name__
            except AttributeError:
            # otherwise, parent and name are unknown
                parent = "Unknown"
                name = "Unknown"
        return parent, name

    @property
    def time(self):
        return self._time
