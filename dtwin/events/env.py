# This file is part of dtwin.
#
# dtwin is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# dtwin is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Copyright (C) 2021 Max Planck Society
#
# dtwin is being developed at the Max Planck Institute for Astrophysics

from ..utils import Singleton, checktype
from ..errors import NoEventError
from .triggerlist import TriggerList
from .eventqueue import EventQueue
from .time import Time
import time
import sys
import json

import asyncio

_eventqueue = EventQueue()
_triggers   = TriggerList()
_time       = Time()

class AsyncEnvironment(Singleton):
    ''' Asynchronous environment.
    '''
    def __init__(self,interfaces=None):
        ''' Asynchronous environment, keeping track of the simulation's state.

            This controls the simulation flow, initialises classes and checks
            their consistency before starting the simulation.

            Parameters:
            ----------
            interfaces : InterfaceBase or iterable thereof
                The interfaces connected to the environment as a whole.
        '''

        self._server = None
        self.interfaces = interfaces
        self._checked = False
        self._initialised = False

    def initialise(self,verbose=False,ignore_flag=False,flag=True,\
            draw_graphs=False):
        ''' Initialises graphs

        Parameters:
        ==========
        verbose : bool
        ignore_flag : bool
            If false: returns early if already initialised.
        flag : bool
            If true: remembers that the environment has been initialised.
        draw_graphs : bool
            If true, draws graphs using pygraphviz. This might result in
            warnings if the required packages/backends are not installed.
        '''
        if self._initialised and not ignore_flag:
            if verbose:
                print(f'Environment is initialised')
            return
        elif verbose:
            print(f'\nInitialising Environment...')

        from ..graphs.environment_graph import EnvironmentGraph
        from ..graphs.area_hierarchy import AreaHierarchy
        from ..graphs.waypoint_network import WaypointNetwork
        from ..graphs.data_model import DataModel

        # the Singletons build their graph when called the first time
        for G in [EnvironmentGraph,AreaHierarchy,WaypointNetwork,DataModel]:
            if verbose:
                s = f'╭ {G.__name__}'
                print(s)
            G(draw=draw_graphs,verbose=verbose)
            if verbose:
                s = f'╰ {G.__name__} is initialised.'
                print(s)

        if flag:
            self._initialised = True

    def check_consistency(self,verbose=False,ignore_flag=False,flag=True):
        ''' Check consistency of the setup.

        Parameters:
        ==========
        verbose : bool
            If true, you'll get more verbose information about consistency.
        ignore_flag : bool
            If false: returns early if already checked.
        flag : bool
            If true: remembers that consistency has been checked.
        '''

        self.initialise(verbose=verbose,ignore_flag=ignore_flag,flag=flag,\
                        draw_graphs=verbose)

        if self._checked and not ignore_flag:
            if verbose:
                print(f'Environment is already checked to be consistent.')
            return
        elif verbose:
            print(f'\nStarting consistency checks...')

        from ..parameters.parameter_base import ParameterBase
        from ..waypoints.waypoint_base import WaypointBase
        from ..random.observable_base import ObservableBase
        from ..containers.container_base import ContainerBase
        from ..queues.abstract import Collection
        from ..areas.area_base import AreaBase
        from ..interfaces.interface_base import InterfaceBase
        import warnings

        # format warnings for this section
        def warning_on_one_line(msg, c, f, l, file=None, line=None):
            return f'│ ! {c.__name__}: {msg}\n'
        warnings.formatwarning = warning_on_one_line

        for c in [ObservableBase,WaypointBase,Collection,
                  AreaBase,ParameterBase,InterfaceBase]:
            try:
                if verbose:
                    s = f'╭ {c.__name__}'
                    print(s)
                c.check_consistency(verbose=verbose)
                if verbose:
                    s = f'╰ {c.__name__} appears consistent.'
                    print(s)
            except NotImplementedError:
                if verbose:
                    print((f'│ {c.__name__} threw a NotImplementedError. '
                           f'│ This will be ignored'))

        # format warnings for this section
        def warning_on_one_line(msg, c, f, l, file=None, line=None):
            return f'! {c.__name__}: {msg}\n'
        warnings.formatwarning = warning_on_one_line

        if flag:
            self._checked = True

        if verbose:
            print(f'\nConsistency check successfull')

    @property
    def interfaces(self):
        ''' List of Interfaces for reporting events.
        '''
        return self._interfaces

    @interfaces.setter
    def interfaces(self,new):
        from ..interfaces.interface_base import InterfaceBase
        newi = checktype(new,(InterfaceBase,type(None)),
                         force_set=True,
                         allow_iterable=True)
        self._interfaces = newi

    @property
    def lock(self):
        ''' Keeps the environment from continuing '''
        return self._lock

    @lock.setter
    def lock(self,new):
        checktype(new,(asyncio.Event,type(None)),'lock')
        self._lock = new

    @property
    def state(self):
        ''' Returns the current state of the environment.
        
        The state of the system consists of:
        1) All containers, their positions, their child/parent relations 
           and applied observables
        2) All queues and waypoints, as well as their next/prev relations, 
           time distributions etc.
        3) All observables and parameters, as well as their dependencies
        4) All interfaces
        '''

        #TODO: Implement the rest of the state

        from ..queues.abstract import Collection

        state = {}

        collections = Collection.get_instances(include_subclasses=True)\
                                .values()
        state['Collection'] = [{\
                'id':c._instance_id,\
                'name':c.name,\
                'class':c.__class__.__name__,\
                'members':[repr(m) for m in c.members],\
                } for c in collections]

        state['EventQueue'] = [{\
                'time':e.time,\
                'parent':e.action_parent_and_name[0].name,\
                'method':e.action_parent_and_name[1],\
                } for e in _eventqueue.queue]

        state['TriggerList'] = [{\
                'parent':t.action_parent_and_name[0].name,\
                'method':t.action_parent_and_name[1],\
                } for t in _triggers.triggers]

        #containers = ContainerBase.get_instances(include_subclasses=True)
        #state['ContainerBase'] = {c:c.members for c in containers}

        return state

    @classmethod
    def load_state(cls,save_name):
        '''Load the state of the program from file.
        '''
        # TODO: implement this
        raise NotImplementedError

    @classmethod
    def save_state(cls,save_name):
        '''Save the state of the program to file.
        '''
        # TODO: implement this
        raise NotImplementedError

    async def report(self,dic):
        ''' Reports dictionary to all registered interfaces '''
        for intf in self.interfaces:
            await intf.send(dic)

    @property
    def server(self):
        return self._server

    @server.setter
    def server(self,new):
        from ..interfaces.interface_base import InterfaceBase
        checktype(self.server,(type(None),InterfaceBase),'server')
        self._server = new

    async def serve(self, host=None, port=None, interface=None):
        ''' Open server at host:port or use interface to emit state updates.

            When starting the simulation, this must be awaited separately.

            Parameters:
            ==========
            host : None or str
                The host of the SocketServer
            port : None or str
                The port of the SocketServer
            interface : InterfaceBase
                The interface to be used.
        '''

        if not ((bool(host) and bool(port)) ^ bool(interface)):
            s = f'Specify either host:port or interface.'
            raise ValueError(s)

        if not interface:
            from ..interfaces.network import SocketServer
            self.server = SocketServer(host=host,port=port)
        else:
            self.server = interface

        if not self.server in self._interfaces:
            self._interfaces.add(self.server)

        await self.server.connect()

    async def step(self, verbose=False):
        ''' Simulates the environment up to the next step

            1)  The next scheduled event is obtained.
                If there are no scheduled events, the simulation halts and
                listens for incoming i/o.
            2)  The scheduled execution time is awaited asynchronously
                to allow for intermediate i/o operations.
            3)  After executing the event, TriggerList is checked for any 
                conditional events. If any condition is met, the associated 
                event is executed immediately.
        '''

        try:
            next_event = _eventqueue.pop()
            next_time  = next_event.time
            if verbose:
                print((f' step {self._stp}, sim time: {_time.now:.2f}, '
                       f'next event @ {next_time}'),end='\r')
        except NoEventError:
            if verbose:
                print(f'\nThere are no more future events scheduled. ')
            if self.server is None:
                if verbose:
                    s=f'There is no interface connected to the environment. '
                    print(s)
                return False
            else: 
                if verbose:
                    print(f"Awaiting signals via interface.")
                self.lock = asyncio.Event()
                await self.lock.wait()

        # uncomment for debugging
        '''
        print('='*79)
        print(f'time: {_time.now}')
        print(f'next event: {next_event.action_parent_and_name}')
        print('state:')
        print(json.dumps(self.state,indent=2))
        '''

        # wait for the next event to happen
        # meanwhile, the environment listens on registered interfaces
        await _time.elapse_until(next_time)

        # execute the next scheduled event
        await next_event.execute()

        # check all triggers and execute accordingly
        await _triggers.check_execute()

        return True

    async def simulate(self, speed_target=1.,n_steps=None,until=None,\
                       check=True,verbose_check=False,\
                       verbose_simulation=False):
        ''' Simulates the environment.

        Parameters:
        ----------
        speed_target : float, default: 1.
            The targeted speed of the simulation. If you set this to 10, it tries
            to simulation 10x faster than the system clock.
        n_steps : int, None, default: None
            If int: The maximum number of steps to simulate.
        until : float, None, default: None
            If float: Simulate until simulation time is larger than `until`
        check : bool, default: True
            Check consistency of setup before starting to simulation.
        verbose_check : bool, default: False
            If true, you'll get more verbose information about consistency.
            If pygraphviz is installed, graphs of the environment are drawn.
        verbose_simulation : bool, default: True
            If true, you'll get a status line about the progress of the simulation
            and some performance stats after the simulation has ended.
        '''

        self._speed_target = speed_target

        t0 = time.time()

        if not self._initialised:
            self.initialise(verbose=verbose_check,draw_graphs=verbose_check)
        if check and not self._checked:
            self.check_consistency(verbose=verbose_check)

        _time.set_speed_target(speed_target)
        _time.tare(set_zero_point=True)

        if verbose_simulation:
            s = ''
            if n_steps:
                s += f'for {n_steps} steps '
            if until:
                s += f'until t={until}'
            if not until and not n_steps:
                s = f'forever'
            print(f'\nRunning simulation {s} at target speed {speed_target}')

        self._stp = 0 
        state = True

        while state:
            # simulate a step and wait for the next event to occur
            if until and _time.now > until:
                break
            if n_steps and n_steps < self._stp:
                break
            self._stp += 1
            state = await self.step(verbose=verbose_simulation)
        if verbose_simulation:
            self.print_stats()

    def print_stats(self):
        print(f'\nSimulation stopped')
        print(f'  Steps:           {self._stp}')
        print(f'  Real time:       {_time.real_time:.2f} seconds')
        print(f'  Simulation time: {_time.now:.2f}')
        print(f'  Target speed:    {_time._speed_target}')
        print(f'  Adjusted speed:  {_time.speed}')
        print(f'  Average speed:   {_time.now/_time.real_time:.2f}')
