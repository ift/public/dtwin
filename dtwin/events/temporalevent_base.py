# This file is part of dtwin.
#
# dtwin is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# dtwin is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Copyright (C) 2021 Max Planck Society
#
# dtwin is being developed at the Max Planck Institute for Astrophysics

from abc import abstractproperty
from .eventqueue import EventQueue
from ..patterns import Multiplet

# instantiate the singlet for usage below
_eventqueue = EventQueue()

class TemporalEventBase(Multiplet):
    '''
    Base class of an event scheduled at a certain time.
    '''

    @abstractproperty
    def action(self):
        ''' The action which is to be triggered when the event occurs.
        '''
        pass

    @abstractproperty
    def time(self):
        ''' The time at which the event occurs, in seconds since Unix epoch.
        '''
        pass

    async def execute(self):
        ''' Execute action.
        '''
        await self.action()

    def register(self):
        ''' Puts event into eventqueue.
        '''
        _eventqueue.push(self)

    def cancel(self):
        ''' Cancel this event, i.e. remove it from the EventQueue.
        '''
        _eventqueue.cancel(self)
        del self

    def __lt__(self,other):
        return self.time <  other.time

    def __le__(self,other):
        return self.time <= other.time

    def __gt__(self,other):
        return self.time >  other.time

    def __ge__(self,other):
        return self.time >= other.time

    def __eq__(self,other):
        return self.time == other.time

    def __ne__(self,other):
        return self.time != other.time
    
    def __repr__(self):
        return f'<Event @ {self.time}>'
