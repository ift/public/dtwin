# This file is part of dtwin.
#
# dtwin is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# dtwin is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Copyright (C) 2021 Max Planck Society
#
# dtwin is being developed at the Max Planck Institute for Astrophysics

import time
import asyncio
from ..patterns import Singleton
from ..errors import TimeTravel

class Time(Singleton):
    ''' Singleton monitoring the simulated time.
    '''
    def __init__(self):
        ''' Singleton monitoring the simulated time.
        '''
        try:
            self._instantiated
        except AttributeError:
            self._delay_tolerance = 10
            self._delays = 0

            # internal list of speed updates and according time stamps
            self._adjustments = []

            # last update of self._sim_time (timestamp)
            self._last = None

            self.tare(set_zero_point=True)

            self._instantiated = True

    @property
    def now(self):
        ''' Returns simulation time of currently handled event(s) '''
        return self._sim_time

    @property
    def now_synced(self):
        ''' Returns simulation time, synced to real time via current speed'''
        return self._sim_time + (time.time()-self._last)/self._speed

    @property
    def time(self):
        return self._sim_time

    @property
    def real_time(self):
        '''Real time since absolute zero point'''
        return time.time() - self._real_t0

    @property
    def _sim_dt(self):
        '''Simulation time since reference time'''
        return self._sim_time - self._sim_tare

    @property
    def _real_dt(self):
        '''Real time since reference time'''
        return time.time() - self._real_tare

    @property
    def speed(self):
        return self._speed

    @speed.setter
    def speed(self,new):
        if not isinstance(new,(int,float)):
            raise TypeError('speed must be int or float')
        self._speed = float(new)
        self._adjustments.append((time.time(),new))

    def tare(self,set_zero_point=False,sim_t0=0.,real_t0=None):
        ''' Set reference time (to be now).
        Parameters:
        ----------
        set_zero_point : bool
            If true, the absolute zero of the simulation time will be set.
            If false, an intermediate reference time will be set.
        sim_t0 : float
            The simulation time at the absolute zero.
        real_t0 : float or None
            The system time at the absolute zero. If None, the current system
            time will be used.
        '''
        if set_zero_point:
            self._real_t0  = time.time() if real_t0 is None else real_t0
            self._sim_t0   = sim_t0
            self._sim_time = self._sim_t0
            self._last     = time.time()
            self._real_tare= self._real_t0
            self._sim_tare = self._sim_time
        else:
            self._real_tare = time.time()
            self._sim_tare  = self._sim_time

    def set_delay_tolerance(self,new):
        ''' Sets the delay tolerance, i.e. the number of temporal events that 
            must be executed with delay (i.e. in the future of their scheduled 
            time) before the simulation speed is adjusted.
        '''
        if not isinstance(new,int):
            raise TypeError('Delay tolerance must be integer')
        self._delay_tolerance = new

    @property
    def delays(self):
        ''' Number of delayed time settings since last reset
        '''
        return self._delays

    @delays.setter
    def delays(self,new):
        self._delays = new
        if self._delays > self._delay_tolerance:
            from ..errors import PerformanceWarning
            import warnings
            # reset delay counter and adjust speed
            self._delays = 0
            self.speed = 4*self.speed/5.
            self.tare()
            s =(f"Delay tolerance exhausted, "
                f"reducing speed to {self.speed:.2f}")
            warnings.warn(s,PerformanceWarning)

    @property
    def mismatch(self):
        ''' The current mismatch of the simulation and real time.
            (in units of simulation time) 

        mismatch > 0: simulation is too slow
        mismatch < 0: simulation is too fast 
        '''
        t_sim   = self._sim_dt      # elapsed simulation time since last tare
        t_real  = self._real_dt     # elapsed real time since last tare
        t_real  = self.speed*t_real # adjust for current speed factor
        mismatch = t_real - t_sim   # difference
        return mismatch

    def sync(self,next_time=None,force=False):
        ''' Synchronise simulation time with real time.

        Parameters:
        -----------
        next_time: float or None
            Simulation time of next scheduled event
            If given, simulation time will be set at most to this next_time
        force: bool
            If True, synchronisation will be enforced.
        '''
        mismatch = self.mismatch
        if not next_time:
            if not force:
                s = f'sync must be called either with next_time or force set'
                raise RuntimeError(s)
            # bypasses any consistency check in set_time, etc.
            self._sim_time += mismatch
            self._last = time.time()
            return mismatch
        if mismatch > 0:
            if mismatch < (next_time - self._sim_time):
                self.set_time(self._sim_time + mismatch)
                self.delays = 0
                return mismatch
            else:
                self.set_time(next_time)
                self.delays += 1
                return mismatch
        else:
            # no need to adjust time, just reset delay counter
            self.delays = 0 
        return mismatch

    def set_time(self,t):
        ''' Set the current simulation time.
        Parameters:
        ==========
        t : float
            the simulation time to which we want to advance.
        '''
        if t < self._sim_time:
            raise TimeTravel('Time must not be set to past values')
        self._sim_time = t
        self._last = time.time()

    def set_speed_target(self,speed):
        ''' Set the speed factor of the simulation time.
            This can only be set once!
        '''
        try:
            self._speed_target
        except:
            self._speed_target = speed
            self.speed = speed

    async def elapse_until(self,next_time):
        ''' Do nothing (except async i/o) until next_time
        '''
        dt = (next_time-self._sim_tare)/self.speed - self._real_dt

        if dt < 0.:
            self.delays += 1
        else:
            self.delays = 0

        await asyncio.sleep(dt)
        # set simulation time AFTER waiting for real time to pass as handling
        # incoming signals from interfaces might advance time as well
        # nevertheless, this will be BEFORE handling the next scheduled event
        # when returning to the .step routine of the environment
        self.set_time(next_time)
