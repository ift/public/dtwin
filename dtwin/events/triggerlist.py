# This file is part of dtwin.
#
# dtwin is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# dtwin is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Copyright (C) 2021 Max Planck Society
#
# dtwin is being developed at the Max Planck Institute for Astrophysics

from ..patterns import Singleton
from ..utils import checktype
from .conditionalevent_base import ConditionalEventBase

class TriggerList(Singleton):
    ''' Global list of conditional events. 
        Events in this list will be checked in each step of the simulation
        against the status of their condition. If an event's condition is met,
        its action will be executed. The event will be removed or not subject
        to its `is_persistent` attribute.
    '''
    def __init__(self):
        try:
            self._triggers
        except:
            # will only happen during first instantiation
            self._triggers = set()

    @property
    def triggers(self):
        return self._triggers

    def push(self,trigger):
        self.append(trigger)

    def register(self,trigger):
        self.append(trigger)
    
    def append(self,trigger):
        ''' Add conditional event to list of triggers.
        Parameters:
        ----------
        trigger : ConditionalEventBase or iterable thereof
            The trigger to be added.
        '''
        trigger = checktype(trigger,ConditionalEventBase,'trigger',\
                             force_set=True, allow_iterable=True)
        self._triggers.update(trigger)
        #if isinstance(trigger,ConditionalEventBase):
        #    self._triggers.add(trigger)
        #elif isinstance(trigger,(tuple,set,list)):
        #    for t in triggers:
        #        self._triggers.append(t)
        #else:
        #    TypeError('trigger must be (a list) of type ConditionalEventBase')
    
    async def check_execute(self):
        ''' Checks registered conditional events and executes them accordingly. 
            Executes triggers as long as no more conditions are met.
        '''

        for cevent in self._triggers:
            if cevent.check():
                if not cevent.is_persistent:
                    self._triggers.remove(cevent)
                await cevent.execute()
                # as an event has been executed, conditions checked previously 
                # might be met by now. hence, start over and break THIS loop 
                # afterwards
                await self.check_execute() 
                break
    
    def __getitem__(self,i):
        return self._triggers[i]
