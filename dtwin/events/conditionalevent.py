# This file is part of dtwin.
#
# dtwin is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# dtwin is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Copyright (C) 2021 Max Planck Society
#
# dtwin is being developed at the Max Planck Institute for Astrophysics

from .conditionalevent_base import ConditionalEventBase
from .triggerlist import TriggerList

_triggerlist = TriggerList()

class ConditionalEvent(ConditionalEventBase):
    def __init__(self,condition,action,persistent=False,push=False):
        ''' An event triggering an action when a certain condition is met.

        Parameters:
        -----------
        condition : function
            The function to be checked. Its return value (boolean)
            determines whether the action will be executed.
        action : function
            The function to be executed if the condition is met.
        persistent : bool
            If true, this event will persist in the TriggerList.
        push : bool
            If true, the instantiated event will be registered in TriggerList
        '''

        self._condition = condition
        self._action = action
        if not isinstance(persistent,bool):
            raise TypeError('persistent must be Boolean.')
        self._persistent = persistent
        if push:
            _triggerlist.push(self)
    
    @property
    def action(self):
        return self._action
    
    @property
    def condition(self):
        return self._condition
    
    @property
    def is_persistent(self):
        return self._persistent

    @property
    def action_parent_and_name(self):
        ''' Returns parent object and name of action, if action is bound.
        '''
        method = self._action
        try:
            # if method is bound, get the object directly
            parent = method.__self__
            name   = method.__name__
        except AttributeError:
            # if not, try whether functools.partial was used
            try:
                parent = method.func.__self__
                name   = method.__name__
            except AttributeError:
                parent = "Unknown"
                name = "Unknown"
        return parent, name

    def __repr__(self):
        parent, name = self.action_parent_and_name
        s = f'<{self.__class__.__name__}:{self._instance_id} ({parent} -- {name})>'
        return s
