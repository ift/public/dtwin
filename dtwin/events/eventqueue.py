# This file is part of dtwin.
#
# dtwin is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# dtwin is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Copyright (C) 2021 Max Planck Society
#
# dtwin is being developed at the Max Planck Institute for Astrophysics

from ..patterns import Singleton
from ..errors import NoEventError
import heapq

from .time import Time
_time = Time()

class EventQueue(Singleton):
    ''' Global priority queue of future Events, sorted by time.
        At each step in the simulation, the next scheduled event is 
        obtained from this list.
    '''
    def __init__(self,queue=[]):
        self._queue = queue
        self._cache = [(None,None)]*100 # cache of popped events
        heapq.heapify(self._queue)

    @property
    def queue(self):
        return self._queue

    @property
    def next(self):
        ''' Returns next event without removing it from the queue '''
        try:
            return self._queue[0]
        except IndexError:
            raise NoEventError('No more events')

    def push(self,event):
        ''' Inserts an event into the priority queue.
        '''
        if _time.now > event.time:
            raise RuntimeError(f'{event} is scheduled in the past')
        heapq.heappush(self._queue,event)

    def append(self,event):
        ''' Alias for push method.
        '''
        self.push(event)

    def add(self,event):
        ''' Alias for push method.
        '''
        self.push(event)

    def pop(self):
        ''' Returns the next event and removes it from the queue. '''
        try:
            event = heapq.heappop(self._queue)
            time_scheduled = event.time
            time_popped    = _time.now
            self._cache[:-1] = self._cache[1:]
            self._cache[-1]  = (time_scheduled,time_popped)
            return event
        except IndexError:
            raise NoEventError('No more events')
    
    def cancel(self,event):
        ''' Cancel event, i.e. remove it from the queue.
        '''
        #TODO: test this
        que = self._queue
        # O(n) :(
        idx = que.index(event)
        que[idx] = que[-1]
        # pop the last element of this *list*
        que.pop()
        # the following is supposed to be an O(log n) implementation of
        # heapq.heapify(que)
        # which is O(n)
        if i < len(que):
            heapq._siftup(que,i)
            heapq._siftdown(que,0,i)

