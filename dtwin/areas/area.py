# This file is part of dtwin.
# 
# dtwin is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# dtwin is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# 
# Copyright (C) 2021 Max Planck Society
# 
# dtwin is being developed at the Max Planck Institute for Astrophysics

from .area_base import AreaBase
from ..utils import checktype
from ..events.time import Time

_time = Time()

class Area(AreaBase):
    def __init__(self, name, begins_at=None, ends_at=None, parent=None,\
            interfaces=None, observables=None, at_entry=None, at_exit=None):
        ''' Implementation of AreaBase.

        Parameters:
        ==========
        name : str
            The unique name of this area
        begins_at : None, dtwin.Waypoint or iterable, optional
            The entry nodes of the Area
        ends_at : None, dtwin.Waypoint or iterable, optional
            The exit nodes of the Area
        parent : None, AreaBase, optional
            The parent Area.
        interfaces :  None, InterfaceBase or iterable, optional
            Interfaces connected to this Area
        observables : None, ObservableBase or iterable, optional
            Observables applied in this Area. In contrast to observables
            associated with waypoints, these observables might also depend
            on the entry time at the area.
        '''

        super(Area,self).__init__()

        self.name = name
        self.begins_at = begins_at
        self.ends_at = ends_at
        self.parent = parent
        self.interfaces = interfaces
        self.observables = observables

        # internal list of containers with entry waypoint and time
        self._containers = {}

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self,new):
        checktype(new,str,'name')
        self._name = new

    @property
    def parent(self):
        return self._parent

    @parent.setter
    def parent(self,new):
        checktype(new,(type(None),AreaBase),'parent')
        self._parent = new

    @property
    def begins_at(self):
        return self._begins_at

    @begins_at.setter
    def begins_at(self,waypoint):
        from ..waypoints.waypoint_base import WaypointBase
        new = checktype(waypoint,(type(None),WaypointBase),'begins_at',
                        allow_iterable=True,
                        force_set=True)
        self._begins_at = new
        for n in new:
            if self not in n.begins:
                n.begins.add(self)

    @property
    def ends_at(self):
        return self._ends_at

    @ends_at.setter
    def ends_at(self,new):
        from ..waypoints.waypoint_base import WaypointBase
        new = checktype(new,(type(None),WaypointBase),'ends_at',
                        allow_iterable=True,
                        force_set=True)
        self._ends_at = new
        for n in new:
            if self not in n.ends:
                n.ends.add(self)

    @property
    def interfaces(self):
        return self._interfaces

    @interfaces.setter
    def interfaces(self,new):
        from ..interfaces.interface_base import InterfaceBase
        new = checktype(new,(InterfaceBase,type(None)),'interfaces',
                  allow_iterable=True,
                  force_set=True)
        self._interfaces = new

    async def report(self,dic):
        ''' Report observables to all interfaces '''
        if 'location' not in dic.keys():
            dic['location'] = self.name
        for intf in self.interfaces:
            await intf.send(dic)

    @property
    def observables(self):
        return self._observables

    @observables.setter
    def observables(self,new):
        from ..random.observable_base import ObservableBase
        new = checktype(new,(ObservableBase,type(None)),'observables',
                  allow_iterable=True,
                  force_set=True)
        self._observables.update(new)

    async def at_entry(self,container,waypoint=None):
        ''' Stores the container along with its entry time for convenience.
        '''
        if container:
            # keep entry waypoint and time in internal memory
            self._containers[container] = (waypoint,_time.now)
            # do the same recursively for any children
            for c in container.children:
                await self.at_entry(c,waypoint=waypoint)
        return _time.now

    async def at_exit(self,container,waypoint=None,_apply=True):
        ''' Applies observables and reports that action to connected interfaces
            The container is removed from the internal dictionary.
        '''
        if container:
            # get entry waypoint and time and stop tracking this container
            entry, start = self._containers.pop(container,None)
            # do the same recursively for any children
            # However, do NOT report them NOR apply observables to them
            for c in container.children:
                await self.at_exit(c,waypoint=waypoint,_apply=False)

            if _apply:
                end   = _time.now
                obs   = {}
                for o in self.observables:
                    obs[o.name] = await o.sample(container,start=start,end=end)

                container.apply_observables(obs)

                dic = {'type':'application',\
                       'container':container.name,\
                       'timestamp':start,\
                       'end':end,\
                       'observables':obs,\
                      }

                await self.report(dic)
                return obs
