# This file is part of dtwin.
# 
# dtwin is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# dtwin is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# 
# Copyright (C) 2021 Max Planck Society
# 
# dtwin is being developed at the Max Planck Institute for Astrophysics

from abc import abstractmethod, ABCMeta, abstractproperty
from ..patterns import Multiplet
from ..errors import Inconsistency

class AreaBase(Multiplet,metaclass=ABCMeta):
    ''' Base class for areas.

    Areas are defined by two sets of waypoints:
    Those at which they begin and those at which they end.

    If a container passes one of the entry (exit) nodes, the respective areas'
    `at_entry` (`at_exit`) method will be called on that container.

    Each area has to have a unique name. They can have one parent area.

    Areas can be connected to and report to interfaces, as well.

    '''

    def __init__(self):
        self._begins_at = set()
        self._ends_at = set()
        self._name = None
        self._parent = None
        self._interfaces = set()
        self._observables = set()
        self._abstract = False

    @property
    def abstract(self):
        ''' Defines abstract objects.
        Abstract objects are not checked against consistency and
        take not part in the environment's graphs
        '''
        return self._abstract

    @abstract.setter
    def abstract(self,new):
        if not isinstance(new,bool):
            raise TypeError
        self._abstract = new
        self._observables = set()

    @abstractproperty
    def name(self):
        pass

    @abstractproperty
    def parent(self):
        pass

    @abstractproperty
    def begins_at(self):
        pass

    @abstractproperty
    def ends_at(self):
        pass

    @abstractproperty
    def interfaces(self):
        pass

    @abstractproperty
    def observables(self):
        pass

    @abstractmethod
    async def at_entry(self,waypoint,container):
        ''' Called by entry node when a container enters the area.

        Parameters:
        -----------
        waypoint : WaypointBase
            the calling waypoint
        container: ContainerBase
            the container being processed

        Returns:
        --------
        time : float
            simulation time of entry

        '''
        pass

    @abstractmethod
    async def at_exit(self,waypoint,container):
        ''' Called by exit node when a container leaves the area.

        Parameters:
        -----------
        waypoint : WaypointBase
            the calling waypoint
        container : ContainerBase
            the container leaving the area

        Returns:
        --------
        observables: dict of name:value
            dictionary of applied observables (names and values)
        '''
        pass

    def report(self,dic):
        ''' Report observables to all interfaces '''
        if 'location' not in dic.keys():
            dic['location'] = self.name
        for intf in self.interfaces:
            intf.send(dic)
        
    @classmethod
    def check_consistency(cls,verbose=False):
        ''' Checks consistency of Area graph 
        
        An Inconsistency exception occurs if
        * there are paths in the waypoint network which enter an area 
          but do not leave it again.
        * there are paths which leave an area without entering it first.
        
        '''
        # check for areas with no entry xor exit
        areas = AreaBase.get_instances(include_subclasses=True,\
                                       flatten=True,\
                                       return_set=True)
        for a in areas:
            if len(a.begins_at) > 0:
                if len(a.ends_at) == 0:
                    raise Inconsistency(f'Area {a.name} an entry but no exit')
            elif len(a.ends_at) > 0:
                raise Inconsistency(f'Area {a.name} has an exit but no entry')
        
        # check for paths which enter an area but do not exit it
        from ..graphs.environment_graph import EnvironmentGraph
        import networkx as nx
        
        # contains all waypoints and areas, and their edges
        wa = EnvironmentGraph().get_subgraph(type=['w','a']).copy()
        
        # contains all waypoints and collections, and their edges
        wc = EnvironmentGraph().get_subgraph(type=['w','c']).copy()
        
        if nx.is_directed_acyclic_graph(wc):
            if verbose:
                print('│ ! Waypoint-collection network is not acyclic')
            # TODO: cut network at some waypoint?
        
        # waypoints with indegree 0
        starts = [] 
        for node, indeg in wc.in_degree():
            if indeg == 0:
                starts.append(node)
        
        # waypoints with indegree 0
        ends   = []
        for node, outdeg in wc.out_degree():
            if outdeg == 0:
                ends.append(node)
        
        cart_prod = [(s,e) for s in starts for e in ends]
        
        # list of paths from any start to any end
        paths = [list(nx.all_simple_paths(wc,s,e)) for s,e in cart_prod]
        paths = [item for sublist in paths for item in sublist]
        
        from ..queues.abstract import Collection
        for path in paths:
            entered = set()
            for waypoint in path:
                if isinstance(waypoint,Collection):
                    # Collection object are also in these paths
                    continue
                # add areas entered at this waypoint
                for a in wa.successors(waypoint):
                    entered.add(a)
                # remove areas exited at this waypoint
                for a in wa.predecessors(waypoint):
                    try:
                        entered.remove(a)
                    except KeyError:
                        s = (f'The setup contains a path in which an area is '
                             f'exited but never entered. '
                             f'The area is: {a.name} '
                             f'The path is {[n.name for n in path]}')
            
            if len(entered) > 0:
                s = (f'The setup contains a path in which an area is entered '
                     f'but not exited. '
                     f'The areas are: {[a.name for a in entered]} '
                     f'The path is {[n.name for n in path]}')
                raise Inconsistency(s)

