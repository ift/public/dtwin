'''
stacking
========

* Waypoint `Create` randomly creates containers with a observable `class`, which
  is always `part`.
* They are pushed into a random access Reservoir, `Created`.
* The next waypoint, `Stack`, gets N of such Containers and stacks them on
  another Container of class `carrier` from a different ingoing reservoir. 
  This waypoint can only execute if there are enough Containers available in
  the respective reservoirs.
* The stacked containers go through different Queues and Waypoints.
* They are unstacked again at Waypoint `Unstack`. The Containers are
  separated into different queues with respect to their class observable.
* The carriers have a observable called `rounds`. This maps dirt getting onto
  the carriers with each round and has an effect on the quality of the finished
  parts. In the carrier loop, the waypoint `W3` decides whether it's time to
  send the carrier through a cleaning area.
* Cleaning the carriers will comes with a cost but raises the chances of 
  the carried parts to have a good quality in the end.
* sketch:

  Create─┐        ┌─ Area A ─┐┌─ Area B ─┐
         ▼        │          ││          │ 
      Created──►Stack──►Q1──►W1──►Q2──►Unstack──►Finished──►Assessment
                  ▲                      │    
                  └──Q4◀─────W2◀───Q3◀───┘
                             ▲└────────┐
                             └──Clean◀─┘
'''

import dtwin as dtw

import asyncio
import time

from simpleobservables import QualityModel, Dust

#dbg = dtw.Debugger(debug=True,level=1)
_time = dtw.Time()

# the parameters
params = {\
        'mu':dtw.FloatParameter('mu',0.,adjustable=True,visible=True),\
        'sigma':dtw.FloatParameter('sigma',1.,adjustable=False,visible=False),\
        }

# create a waypoint that keeps creating Containers with some observables
create = dtw.RandomCreation(\
         name = 'Create',\
         time_dist = dtw.Exponential(5.,offset=2.),\
         class_observable='part',\
         )

# build a dictionary of random variables
feat_s = {}

# the categorical 'geometry' variable has a fixed distribution
feat_s['geometry'] = dtw.RandomObservable(\
        'geometry',\
        dtw.Categorical(['a','b','c'],[0.7,0.2,0.1]),\
        )

feat_s['size'] = dtw.ParametrisedNormal(\
        'size',\
        mu=params['mu'],\
        sigma=params['sigma'],\
        )

# register observables at 'Create' waypoint
create.observables = feat_s

created = dtw.RandomAccess(\
        'Created',\
        capacity=20,\
        prev_waypoint=create,\
        )

# set parent_queue=q4 later when it is defined (see below)
stack = dtw.Stack(\
        name = 'Stack',\
        children_queue = created,\
        n_children = 6,\
        )

q1 = dtw.FIFO(\
        name = 'Q1',\
        prev_waypoint = stack,\
        distribution = dtw.Exponential(1.,offset=0.5),\
        capacity = 2,\
        )

w1 = dtw.Transit(\
        name = 'W1',\
        prev_queue = q1,\
        )

q2 = dtw.FIFO(\
        name = 'Q2',\
        prev_waypoint = w1,\
        distribution = dtw.Exponential(1.,offset=0.5),\
        capacity = None,\
        )

#==============================================================================
# unstack

def decide(in_queue,container):
    if container.class_ != 'carrier':
        # parts take exit 0
        return 0
    else:
        # carriers take exit 1
        return 1

unstack = dtw.Unstack(\
        name = 'Unstack',\
        decision_function = decide,\
        prev_queue = q2,\
        )

#==============================================================================
# part path

finished = dtw.FIFO(\
        name='Finished',\
        prev_waypoint=unstack,\
        distribution=dtw.Exponential(5.,offset=2.),\
        capacity=5,\
        )

quality = QualityModel(\
        name='quality',\
        arguments=[feat_s['size']],\
        )

assessment = dtw.Label(\
        name = 'Assessment',\
        model = quality,\
        prev_queue = finished,\
        destroy=True,\
        )

#==============================================================================
# carrier path

q3 = dtw.FIFO(\
        name = 'Q3',\
        prev_waypoint = unstack,\
        distribution = dtw.Exponential(1.,offset=0.5),\
        capacity = None,\
        )

count_threshold = 10

def w2_decision(inqueue,container):
    cnt = container.observables.get('counter',0) # defaults to 0
    if cnt >= count_threshold:
        # send container to cleaning queue
        outq = 0
    else:
        outq = 1 
    return outq

# count observable
counter = dtw.Count(name='counter',reset_value=0,increment=1,
                    threshold=count_threshold)

# dust observable: this depends on the counter observable and will be applied
# to the children in stacked containers. Probability of dust==1 increases with
# counter value
dust = Dust(name='dust',int_observable=counter,mu=8.,parent=unstack)

w2 = dtw.Switch(\
        name = 'W2',\
        decision_function = w2_decision,\
        observables = counter
        )

clean = dtw.FIFO(\
        name = 'Clean',\
        prev_waypoint = w2,\
        distribution = dtw.Exponential(3.,offset=10.),\
        capacity = None,\
        )

w2.prev = [q3,clean]

#q4 = dtw.FIFO(\
#        name = 'Q4',\
#        prev_waypoint = w2,\
#        distribution = dtw.Exponential(1.,offset=0.5),\
#        capacity = None,\
#        )
q4 = dtw.RandomAccess(\
        name = 'Q4',\
        prev_waypoint = w2,\
        capacity = None,\
        )

# watch out that the indices here match with the decision logic above!
w2.next = [clean,q4]

# define q4 to be the queue providing parent Containers to stacking waypoint
stack.parent_queue = q4

#==============================================================================
# areas

a = dtw.Area(\
        name='A',\
        begins_at=stack,\
        ends_at=w1,\
        )

b = dtw.Area(\
        name='B',\
        begins_at=w1,\
        ends_at=unstack,\
        )

#==============================================================================
# initialise containers in carrier loop

import random

# put some carriers with random count numbers into the waiting queue
N_carriers = 5
for c in range(N_carriers):
    this_cnt = random.randint(0,10)
    q4.push(dtw.Container(class_='carrier',\
                          applied_observables={'counter':this_cnt}))

#==============================================================================
# initialise environment etc

# the initial event is to start creating parts in 'Create' at t=0.
# subsequent creating events will be scheduled according to its distribution
initial_event = dtw.TemporalEvent(\
        time=0.,\
        action=create.execute,\
        push=True,\
        )

# this implements a trigger: the stack waypoint shall stack containers 
# whenever its condition is satisfied (i.e. enough containers are available)
stack_trigger = dtw.ConditionalEvent(\
        condition=stack.condition,\
        action=stack.execute,\
        push=True,\
        persistent=True,\
        )

# reference the global environment
aenv = dtw.AsyncEnvironment()

# server interface for the environment
server = dtw.SocketServer(host='localhost',port='8888')
server.add_to_objects(filter_=lambda obj:\
                            isinstance(obj,(dtw.WaypointBase,dtw.AreaBase)))

# database interface
#db     = dtw.RWDatabase(\
#            db_flavor='postgres',database='testdb',user='testuser',\
#            pw='testpw',url='localhost',port='5432')
#
## add database interface to any Waypoint or Area
#db.add_to_objects(filter_=lambda obj:\
#                            isinstance(obj,(dtw.WaypointBase,dtw.AreaBase)))

# collect tasks: create a server for interaction and start the simulation
tasks = asyncio.gather(\
        aenv.serve(interface=server),\
        aenv.simulate(speed_target=1000.),\
        )

# uncomment to draw graphs and get verbose output during checks
#aenv.check_consistency(verbose=True)

# get event loop and starting time
loop = asyncio.get_event_loop()

try:
    loop.run_until_complete(tasks)
except KeyboardInterrupt:
    aenv.print_stats()
    tasks.cancel()
