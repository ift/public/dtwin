'''
DEMO 1
======

In this demo, we simulate the following system:

* Waypoint S randomly creates containers of class 'a','b','c'.
* Those are put into a queue Q.
* After some time, they arrive at waypoint E.
* An area A is defined to start at S and end at E.
* There is a set of observables, X, which are associated with the area A.
* Finally, waypoint E uses those variables to compute a label Y, given
  a model m = p(Y|X)

* sketch:

  ┌── A ──┐            
  S──►Q──►E

'''

import dtwin as d

import asyncio
import time
import numpy as np

from simpleobservables import QualityModel

#dbg = Debugger(debug=True,level=1)
_time = d.Time()

# the parameters
params = {\
        'mu':d.FloatParameter('mu',0.,adjustable=True,visible=True),\
        'sigma':d.FloatParameter('sigma',1.,adjustable=False,visible=False),\
        'xx':d.FloatParameter('xx',10.,adjustable=False,visible=True),\
        }

# create a waypoint that keeps creating Containers with some observables
s = d.RandomCreation(\
        name = 'S',\
        time_dist = d.Exponential(5.,offset=.5)
        )

# build a dictionary of random variables
feat_s = {}

# the categorical 'class' variable has a fixed distribution
feat_s['class'] = d.RandomObservable(\
        'class',\
        d.Categorical(['a','b','c'],[0.7,0.2,0.1]),\
        )

# the categorical 'color' variable has a fixed distribution
feat_s['color'] = d.RandomObservable(\
        'color',\
        d.Categorical(['red','blue'],[0.4,0.6]),\
        )

# the numberical 'size' variable depends on an adjustable parameter 'mu'
feat_s['size'] = d.ParametrisedNormal(\
        'size',\
        mu=params['mu'],\
        sigma=params['sigma'],\
        )

# name will be set automatically
s.timestamp = d.Timestamp()

# register observables at waypoint 'S'
s.observables = feat_s

# FIFO queue into which the created Containers are pushed after creation
q = d.FIFO(\
        name = 'Q',\
        prev_waypoint = s,\
        distribution = d.Exponential(10.),\
        capacity = 20,\
        )

# create a observable
quality = QualityModel(\
        name='quality',\
        arguments=[feat_s['size']],\
        )

# the second waypoint 'E' labels containers depending on their class,
# forwarding the result along with all observables to any listening 
# clients and deletes the containers from the simulation at the end
e = d.Label(
        name = 'E',
        model = quality,
        prev_queue = q,
        destroy = True,     # destroy containers at the end
        report_all = True,  # report all observables of the container
        )

e.timestamp = d.Timestamp()

# define the area starting at 'S' and ending at 'E'
a = d.Area(\
        name='A',\
        begins_at=s,\
        ends_at=e,\
        )

feat_a = {}

def RBF(x,y):
    # x,y are 1D index arrays 
    x = x.reshape(-1,1)
    y = y.reshape(1,-1) 
    d = x-y
    return 0.2*np.exp(-0.5*(d**2)/40.)

def Linear(x,y):
    # x,y are 1D index arrays 
    x = x.reshape(-1,1)
    y = y.reshape(1,-1) 
    return x.dot(y)

def Laplace(x,y):
    # x,y are 1D index arrays 
    x = x.reshape(-1,1)
    y = y.reshape(1,-1) 
    d = np.abs(x-y)
    return 0.2*np.exp(-0.5*(d)/20.)

def mean_GP(x):
    return 2.*np.sin(x/100.)

# add a Gaussian Process to Area 'A'
feat_a['Temperature'] = d.CachedGaussianProcess1D(\
        name='Temperature',\
        parent=a,\
        mean=mean_GP,\
        #kernel=lambda x: 0.5*np.exp(-(x[0]-x[1])**2/10.),\
        vectorised_kernel=RBF,\
        sample_period=1.,\
        cachesize=1000,\
        regular=True
        )

feat_a['T_int'] = d.ProcessStatistic(\
        process=feat_a['Temperature'],\
        statistic='int',\
        )

feat_a['T_mean'] = d.ProcessStatistic(\
        process=feat_a['Temperature'],\
        statistic='mean',\
        )

feat_a['T_dmean'] = d.ProcessStatistic(\
        process=feat_a['Temperature'],\
        statistic='dmean',\
        )

feat_a['T_min'] = d.ProcessStatistic(\
        process=feat_a['Temperature'],\
        statistic='min',\
        )

feat_a['T_max'] = d.ProcessStatistic(\
        process=feat_a['Temperature'],\
        statistic='max',\
        )

feat_a['T_amp'] = d.ProcessStatistic(\
        process=feat_a['Temperature'],\
        statistic='amp',\
        )

a.observables = {\
        feat_a['T_int'],\
        feat_a['T_mean'],\
        feat_a['T_amp'],\
        feat_a['T_dmean'],\
        feat_a['T_min'],\
        feat_a['T_max'],\
        }

# create an initial scheduled event and push it to the event queue
initial_event = d.TemporalEvent(\
        time=.1,\
        action=s.execute,\
        push=True,\
        )

# reference the global environment
aenv = d.AsyncEnvironment()

# server interface for the environment
server = d.SocketServer(host='localhost',port='8888')
aenv.interfaces.add(server)

db = d.RWDatabase(\
        db_flavor='postgresql',database='testdb',user='testuser',\
        pw='testpw',url='localhost',port='5432')

s.interfaces.add(db)
e.interfaces.add(db)

# collect tasks: create a server for interaction and start the simulation
tasks = asyncio.gather(\
        aenv.serve(interface=server),\
        aenv.simulate(speed_target=100.),\
        )

# get event loop and starting time
loop = asyncio.get_event_loop()
t0 = time.time()

try:
    loop.run_until_complete(tasks)
except KeyboardInterrupt:
    aenv.print_stats()
    tasks.cancel()
