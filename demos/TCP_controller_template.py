'''
Closed-Loop Controller Template
===============================

This serves as a template for closed-loop control, using SocketClient for connecting 
to a TCP server provided by the simulation.
A template for the callback required by the SocketClient class is given.

Incoming messages will have one of the following types:
    "creation"    - a container entered the simulation
    "deletion"    - a container exited the simulation
    "get"         - visible observables and adjustable parameters
    "application" - observables and their values as applied to some container
    "movement"    - a container has passed a certain waypoint
    "stacking"    - containers have been un-/stacked at a certain waypoint
    "ack set"     - a previous 'set' message was accepted by the simulation

The following message types are accepted by the server:
    "get"   - obtain visible observables and adjustable parameters (returns 'get')
    "set"   - set one or more parameters

In order to send a response, pass a Python dictionary to socket.send
in the callback.
'''

import asyncio
from dtwin.interfaces.network import SocketClient

# server host/port
host = '127.0.0.1'
port = '8888'

async def callback(msg_dic,socket):
    ''' Callback computing the response to incoming event notifications
    '''

    return_dic = {}
    send = False

    msg_type = msg_dic['type'].lower()

    # in the following, define what is supposed to happen in the various cases
    # set the flag 'send' to True in order to send back a response

    print(f'incoming: {msg_type}')

    if msg_type == 'get':
        # keywords: observables, parameters, transmission
        pass

    elif msg_type == 'application':
        # keywords: container, parent, observable(value), timestamp, transmission
        pass

    elif msg_type == 'movement':
        # keywords: container, waypoint, timestamp, transmission
        pass

    elif msg_type == 'stacking':
        # keywords: parent, child, location, stack, position, timestamp, transmission
        pass

    elif msg_type == 'creation':
        # keywords: container, location, stack, position, timestamp, transmission
        pass

    elif msg_type == 'deletion':
        # keywords: parent, child, location, stack, position, timestamp, transmission
        pass

    elif msg_type == 'ack set':
        pass

    if send==True:
        await socket.send(return_dic)

# connect to the simulation and run until keyboard interrupt
try:
    client = SocketClient(callback,host=host,port=port,encoding='utf-8')
    asyncio.run(client.connect())
except KeyboardInterrupt:
    print('KeyboardInterrupt: Closing connection.')
