from dtwin.random.observable_base import ObservableBase
import numpy as np

class QualityModel(ObservableBase):
    ''' Model for quality, depending on a float parameter
    '''
    def __init__(self, name, mu=1., sigma=1.,  arguments=[], parent=None,\
            visible = True):
        super(QualityModel,self).__init__()
        self.name = name
        self.visible = visible

        self.needs = arguments
        self.dict = {}

        self.mu = np.array(mu)
        self.sigma = np.atleast_2d(sigma)
        self.invsigma = np.linalg.inv(self.sigma)

        self._parent = parent

    @property
    def dtype(self):
        return float

    def _probability(self,dic):
        dep_vals = np.array([dic[n.name] for n in self.needs])
        diff = dep_vals - self.mu
        return np.exp( -.5*diff.T * self.invsigma * diff)
            
    async def sample(self,container,**kwargs):
        dic = dict()

        for dep in self.needs:
            dic[dep.name] = container.observables[dep.name]

        prob_at_value = self._probability(dic)
        tmp = np.random.rand()

        return int(tmp <= prob_at_value)
