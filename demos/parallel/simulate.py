'''
short_parallel
==============

In this demo, we simulate the following system:

* Waypoint `Start` randomly creates containers along with some observables.
* It then sends them either to Queue Qa1 or Qb1
* Depending on that, the containers go through
  * Qa1, Wa1, Qa2, Wa2, Qa3, OR
  * Qb1, Wb1, Qb2, Wb2, Qb3
* The lines reunite at waypoint `End`.
* Here, the containers are labelled and removed from the simulation.
* There are Areas `A`, `B`, and `Plant`.
* There are observables, which are associated with Area A or B.
* Finally, waypoint E uses those variables to compute a label Y, given
  a model m = p(Y|X)

* sketch:

    ┌───────────────────── Plant ────────────────────┐
    │                                                │
    │                        ┌──── A ────┐           │
    │               ┌►Qa1──►Wa1──►Qa2──►Wa2──►Qa3─┐  │
  Start──►Q0──►Fork─?                             ├►End
                    └►Qb1──►Wb1──►Qb2──►Wb2──►Qb3─┘ 
                             └──── B ────┘

'''

import dtwin as dtw

import asyncio
import time
import numpy as np

from simpleobservables import QualityModel

#dbg = dtw.Debugger(debug=True,level=1)
_time = dtw.Time()

# the parameters
params = {\
        'mu':dtw.FloatParameter('mu',0.,adjustable=True,visible=True),\
        'sigma':dtw.FloatParameter('sigma',1.,adjustable=False,visible=False),\
        }

# create a waypoint that keeps creating Containers with some observables
start = dtw.RandomCreation(\
        name = 'Start',\
        time_dist = dtw.Exponential(5.,offset=2.)
        )

# build a dictionary of random variables
feat_s = {}

# the categorical 'class' variable has a fixed distribution
feat_s['class'] = dtw.RandomObservable(\
        'class',\
        dtw.Categorical(['a','b','c'],[0.7,0.2,0.1]),\
        )

feat_s['size'] = dtw.ParametrisedNormal(\
        'size',\
        mu=params['mu'],\
        sigma=params['sigma'],\
        )

start.timestamp = dtw.Timestamp()
start.observables = feat_s

q0 = dtw.FIFO(\
        name = 'Q0',\
        prev_waypoint = start,\
        distribution = dtw.Exponential(1.),\
        capacity = 2,\
        )

def decide(in_queue,container):
    if container.observables['class'] == 'a':
        # class a containers take path A (#0 in outqueue list)
        return 0
    else:
        # class a containers take path B (#1 in outqueue list)
        return 1

fork = dtw.Switch(\
        name = 'Fork',\
        decision_function = decide,\
        prev_queue = q0,\
        )

#==============================================================================
# path A (first to enlist as fork's next queue)

qa1 = dtw.FIFO(\
        name = 'Qa1',\
        prev_waypoint = fork,\
        distribution = dtw.Exponential(1.,offset=2.),\
        capacity = 3,\
        )

#print(f'qa1 is {repr(qa1)}')

wa1 = dtw.Transit(\
        name = 'Wa1',\
        prev_queue = qa1,\
        )
        
qa2 = dtw.FIFO(\
        name = 'Qa2',\
        prev_waypoint = wa1,\
        distribution = dtw.Exponential(1.,offset=2.),\
        capacity = 3,\
        )

wa2 = dtw.Transit(\
        name = 'Wa2',\
        prev_queue = qa2,\
        )

qa3 = dtw.FIFO(\
        name = 'Qa3',\
        prev_waypoint = wa2,\
        distribution = dtw.Exponential(1.,offset=2.),\
        capacity = 3,\
        )

#possible future syntax sugar for chaining waypoints and queues:

#start > q0 > fork

#fork > qa1 
#fork > qb1

#qa1 > wa1 > qa2 > wa2 > qa3 > end
#qb1 > wb1 > qb2 > wb2 > qb3 > end
        

#==============================================================================
# path B (second to enlist as fork's next queue)

qb1 = dtw.FIFO(\
        name = 'Qb1',\
        prev_waypoint = fork,\
        distribution = dtw.Exponential(1.,offset=1.),\
        capacity = 5,\
        )

wb1 = dtw.Transit(\
        name = 'Wb1',\
        prev_queue = qb1,\
        )

qb2 = dtw.FIFO(\
        name = 'Qb2',\
        prev_waypoint = wb1,\
        distribution = dtw.Exponential(1.,offset=1.),\
        capacity = 5,\
        )

wb2 = dtw.Transit(\
        name = 'Wb2',\
        prev_queue = qb2,\
        )

qb3 = dtw.FIFO(\
        name = 'Qb3',\
        prev_waypoint = wb2,\
        distribution = dtw.Exponential(1.,offset=1.),\
        capacity = 5,\
        )

#==============================================================================
# waypoint end

quality = QualityModel(\
        name='quality',\
        arguments=[feat_s['size']],\
        )

end = dtw.Label(\
        name = 'end',\
        model = quality,\
        prev_queue = [qa3,qb3],\
        destroy=True,\
        )

#==============================================================================
# areas

# define a kernel
def RBF(amp,sigma):
    def tmp(x,y):
        # x,y are 1D index arrays 
        x = x.reshape(-1,1)
        y = y.reshape(1,-1) 
        d = x-y
        return amp*np.exp(-0.5*(d**2)/(2.*sigma))
    return tmp


# define the area starting at 'S' and ending at 'E'
plant = dtw.Area(\
        name='Plant',\
        begins_at=start,\
        ends_at=end,\
        )

#==============================================================================
# Area A

a = dtw.Area(\
        name='A',\
        begins_at=wa1,\
        ends_at=wa2,\
        )

a_temp = dtw.CachedGaussianProcess1D(\
        name='Temperature_A',\
        parent=a,\
        mean=lambda t: 273. + 5.*np.sin(t/24*60*60)**3,\
        vectorised_kernel=RBF(10.,60),\
        sample_period=1.,\
        cachesize=1800,\
        regular=True,\
        noise_amplitude=1e-6\
        )

a_pres = dtw.CachedGaussianProcess1D(\
        name='Pressure_A',\
        parent=a,\
        mean=lambda t: 100. + np.sin(t),\
        vectorised_kernel=RBF(5.,10.),\
        sample_period=1.,\
        cachesize=1800,\
        regular=True,\
        noise_amplitude=1e-6\
        )

a_temp_mean = dtw.ProcessStatistic(\
        process=a_temp,\
        statistic='mean',\
        )

a_pres_mean = dtw.ProcessStatistic(\
        process=a_pres,\
        statistic='mean',\
        )

a.observables = {\
        a_temp_mean,\
        a_pres_mean,\
        }

#==============================================================================
# Area B

b = dtw.Area(\
        name='B',\
        begins_at=wb1,\
        ends_at=wb2,\
        )

b_temp = dtw.CachedGaussianProcess1D(\
        name='Temperature_B',\
        parent=b,\
        mean=lambda t: 273. + 5.*np.sin(t/24*60*60)**3,\
        vectorised_kernel=RBF(10.,30),\
        sample_period=1.,\
        cachesize=1800,\
        regular=True,\
        noise_amplitude=1e-6\
        )

b_pres = dtw.CachedGaussianProcess1D(\
        name='Pressure_B',\
        parent=b,\
        mean=lambda t: 100.+0.*t,\
        vectorised_kernel=RBF(5.,75.),\
        sample_period=1.,\
        cachesize=1800,\
        regular=True,\
        noise_amplitude=1e-6\
        )

b_temp_mean = dtw.ProcessStatistic(\
        process=b_temp,\
        statistic='mean',\
        )

b_pres_mean = dtw.ProcessStatistic(\
        process=b_pres,\
        statistic='mean',\
        )

b.observables = {\
        b_temp_mean,\
        b_pres_mean,\
        }

#==============================================================================
# initialise environment etc

# create an initial scheduled event and push it to the event queue
initial_event = dtw.TemporalEvent(\
        time=0.,\
        action=start.execute,\
        push=True,\
        )

# reference the global environment
aenv = dtw.AsyncEnvironment()

# create a database interface
db     = dtw.RWDatabase(\
            db_flavor='postgresql',database='testdb',user='testuser',\
            pw='testpw',url='localhost',port='5432')
# add database interface to any Waypoint or Area
db.add_to_objects(filter_=lambda obj:\
                            isinstance(obj,(dtw.WaypointBase,dtw.AreaBase)))

# r/w interface for the environment
server = dtw.SocketServer(host='localhost',port='8888')

# collect tasks: create a server for interaction and start the simulation
tasks = asyncio.gather(\
        aenv.serve(interface=server),\
        aenv.simulate(speed_target=100.),\
        )

# perform checks and draw diagrams before taking time
#aenv.check_consistency(verbose=True)

# get event loop and starting time
loop = asyncio.get_event_loop()
t0 = time.time()

try:
    loop.run_until_complete(tasks)
except KeyboardInterrupt:
    aenv.print_stats()
    tasks.cancel()
