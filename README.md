# dtwin

## Summary

*dtwin* is a discrete event simulation (DES) framework specialised to mimic manufacturing processes.
Its goal is not only to be able to mirror the structural and dynamic elements of the process, but also to model the abstract relationships between its observables and parameters.
The framework's API is defined via base classes.
*dtwin* comes along with basic implementations, but the user is obviously free to implement more specific subclasses.
Parameters can be altered via interfaces during runtime. 
They influence observables due to some model, which has to be given by the user.
The framework gives feedback of what is happening in the simulation during runtime, where the level of detail is set by the user.

*dtwin* was programmed with the intention to use it as a replacement for a class of real manufacturing plants and similar processes in order to design control systems if the interfaces, the structural elements as well as observables and parameters are known.
Because the model connecting observables and parameters is hidden to the outside world, and the user can choose which parameters are adjustable, which observables are visible and which parts of the simulation give feedback at all, *dtwin* may also serve as a toolkit to construct challenges for control algorithms and data scientists in a blinded fashion.

## Installation

### Requirements

* Python 3
* `networkx` is needed for building the environment graphs and subgraphs

### Optional Requirements

* `numpy` and `scipy` are needed for some implementations of the base classes which come along with the package
* `SQLAlchemy` for database interfaces (along with database specific packages, e.g. `psycopg2` for PostgreSQL)
* `eralchemy`: If installed, a visualisation of the database structure will be saved as a PDF file in verbose mode
* `pygraphviz` (requires `graphviz`): With these, the environment graph and several subgraphs can be saved as PDF files in verbose mode

### Installation

* Clone the repository, e.g. via `git clone https://gitlab.mpcdf.mpg.de/ift/public/dtwin.git`
* In the repository directory, install the package e.g. via `pip install .` or `pip install -e .`

## Licensing

*dtwin* is licensed under the terms of the GNU Lesser General Public License version 3.0 or later (LGPLv3+).
