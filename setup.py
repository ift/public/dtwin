from setuptools import setup

setup(
   name='dtwin',
   version='0.1',
   author='Max Newrzella',
   author_email='maxn@mpa-garching.mpg.de',
   description='DES for manufacturing processes',
   license='LGPLv3+',
   packages=['dtwin'],
   install_requires=[
       'networkx',
       ], 
   classifiers=[
       "Development Status :: 4 - Beta",
       "Environment :: Console",
       "Intended Audience :: Manufacturing",
       "License :: OSI Approved :: GNU Lesser General Public License v3 or later (LGPLv3+)",
       "Topic :: Scientific/Engineering",
       "Operating System :: OS Independent",
       "Programming Language :: Python",
       "Intended Audience :: Science/Research",
       ],
)
