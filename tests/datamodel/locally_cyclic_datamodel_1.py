'''
Here, we have a cyclic datamodel at one waypoint. This should result in 
an Inconsistency error when checking the environment's consistency.
'''

import dtwin as dtw

class CyclicObservable(dtw.ObservableBase):
    def __init__(self,name):
        super(CyclicObservable,self).__init__()
        self.name = name
        self.needs = [] 

    def dtype(self):
        return bool

    def sample(self,**kwargs):
        return self.needs[0].sample(**kwargs)

obs1 = CyclicObservable('O1')
obs2 = CyclicObservable('O2')

obs1.needs += [obs2]
obs2.needs += [obs1]

w1 = dtw.Transit(\
        name = 'Waypoint 1',\
        )

q1 = dtw.FIFO(\
        name = 'Queue',\
        prev_waypoint = w1,\
        distribution = dtw.Exponential(5.,offset=1),\
        )

w  = dtw.Transit(\
        name = 'Waypoint 2',\
        prev_queue = q1,\
        observables=[obs1,obs2]
        )

aenv = dtw.AsyncEnvironment()
aenv.check_consistency(verbose=True)
