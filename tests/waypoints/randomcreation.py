'''
Simulates a waypoint creating containers, which are put into a queue.
'''

import dtwin as dtw

import asyncio

# create a waypoint that keeps creating Containers with some 
# randomly chosen categorical observables
o = dtw.RandomObservable(name='feature',\
                     distribution=dtw.Categorical(['a','b'],[0.9,0.1])
                     )

r = dtw.RandomCreation(name = 'Creation',\
                   time_dist = dtw.Exponential(),\
                   observables = o,\
                   )

# create a FIFO queue into which the created Containers are pushed
q = dtw.FIFO(name = 'fifo',\
         prev_waypoint = r,\
         distribution = dtw.Exponential(),\
         capacity = 5,
         )

l = dtw.Transit(name='End',\
        prev_queue = q,\
        )

# create an initial scheduled event and push it to the event queue
initial_event = dtw.TemporalEvent(time=0.,
                              action=r.execute,\
                              push=True)

aenv = dtw.AsyncEnvironment()

futures = asyncio.gather(aenv.simulate(speed_target=1000.,verbose_check=False),\
                         )

loop = asyncio.get_event_loop()
loop.run_until_complete(futures)
