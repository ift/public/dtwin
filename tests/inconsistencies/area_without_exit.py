'''
This should raise an Inconsistency exception.
Area A is entered but not exited
'''

import dtwin as dtw

w1 = dtw.RandomCreation(
        name='Start',\
        time_dist = dtw.Exponential(5.,offset=1.),\
        )

q1 = dtw.FIFO(
        name = 'Queue',\
        prev_waypoint = w1,\
        distribution = dtw.Exponential(5.,offset=1),\
        )

w2 = dtw.Transit(
        name = 'Transit',\
        prev_queue = q1,\
        )

a = dtw.Area(\
        name = 'A',\
        begins_at = w1,\
        )

aenv = dtw.AsyncEnvironment()
aenv.check_consistency(verbose=True)
