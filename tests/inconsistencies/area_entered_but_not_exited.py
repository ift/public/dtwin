'''
This should raise an Inconsistency exception.
Area A is entered but not exited
'''

import dtwin as dtw

switch = dtw.Switch(\
        name = 'Switch',\
        decision_function = lambda inq,cont: 0,\
        )

# Path A
qA1 = dtw.FIFO(\
        name = 'Queue A1',\
        prev_waypoint = switch,\
        distribution = dtw.Exponential(5.,offset=1),\
        )

wA  = dtw.Transit(\
        name = 'Waypoint A',\
        prev_queue = qA1
        )

qA2 = dtw.FIFO(\
        name = 'Queue A2',\
        prev_waypoint = wA,\
        distribution = dtw.Exponential(5.,offset=1),\
        )

# Path B
qB1 = dtw.FIFO(\
        name = 'Queue B1',\
        prev_waypoint = switch,\
        distribution = dtw.Exponential(5.,offset=1),\
        )

wB  = dtw.Transit(\
        name = 'Waypoint B',\
        prev_queue = qB1
        )

qB2 = dtw.FIFO(\
        name = 'Queue B2',\
        prev_waypoint = wB,\
        distribution = dtw.Exponential(5.,offset=1),\
        )

end = dtw.Transit(\
        name = 'End',\
        prev_queue = (qA2,qB2),\
        )

a = dtw.Area(\
        name = 'A',\
        begins_at = wA,\
        ends_at = wB,\
        )

aenv = dtw.AsyncEnvironment()
aenv.check_consistency(verbose=True)
