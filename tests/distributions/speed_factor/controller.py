import dtwin
import asyncio

async def cb(msg_dic,socket):
    print(msg_dic)
    await asyncio.sleep(0)

client = dtwin.SocketClient(cb,host='localhost',port='8888',encoding='utf-8')

try:
    asyncio.run(client.connect())
except KeyboardInterrupt:
    pass
