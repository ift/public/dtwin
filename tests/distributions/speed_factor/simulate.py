'''
Simulates a waypoint creating containers, which are put into a queue.
Gets its speed factor from command line argument.
'''
import dtwin as dtw
import asyncio
import sys

speed = float(sys.argv[1])

r = dtw.RandomCreation(name = 'Creation',\
                   time_dist = dtw.Exponential(offset=1.),\
                   )

# create a FIFO queue into which the created Containers are pushed
q = dtw.FIFO(name = 'fifo',\
         prev_waypoint = r,\
         distribution = dtw.Exponential(offset=1.),\
         )

l = dtw.Label(name='End',\
        prev_queue = q,\
        model = dtw.Constant(name='label',value=1),\
        destroy = True,\
        )

# database connection
#db = dtw.RWDatabase(\
#        db_flavor='postgres',database='testdb',user='testuser',\
#        pw='testpw',url='localhost',port='5432')
#
#db.add_to_objects(filter_=lambda obj: isinstance(obj,dtw.WaypointBase))

#server = dtw.SocketServer(host='localhost',port='8888')
#server.add_to_objects(filter_=lambda obj: isinstance(obj,dtw.WaypointBase))

# create an initial scheduled event and push it to the event queue
initial_event = dtw.TemporalEvent(time=0.,
                              action=r.execute,\
                              push=True)

aenv = dtw.AsyncEnvironment()

tasks = asyncio.gather(#aenv.serve(interface=server),\
                       aenv.simulate(speed=speed,\
                                     n_steps=10000,\
                                     verbose_check=False),\
                       )

loop = asyncio.get_event_loop()
try:
    loop.run_until_complete(tasks)
except KeyboardInterrupt:
    aenv.print_stats()
    tasks.cancel()
