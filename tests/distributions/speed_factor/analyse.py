import dtwin 
import sqlalchemy as sql
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import sys

from dtwin.interfaces.sqlalchemy_tables import tables

speed = float(sys.argv[1])

# use the dtwin database interface to obtain an engine
db = dtwin.RWDatabase(db_flavor='postgres',user='testuser',pw='testpw',
                      database='testdb',url='localhost')
engine = db.engine

# get all tables
dfs = {t[0]:pd.read_sql(t[0],con=engine) for t in tables}

m = dfs['movements']
c = dfs['containers']

# doubles contains a list of containers which occur twice in 'movements'
# i.e. those, which are created and destroyed, hence pass the queue
#doubles = m.groupby('container').count()['id'].reset_index()
#doubles = doubles[doubles['id'] == 2]['container'].values
#
#deltas = np.empty(len(doubles))
#for idx,d in enumerate(doubles):
#    deltas[idx] = m[m['container'] == d]['timestamp'].diff().iloc[1]

# this does the same
dt = m.sort_values('timestamp').groupby('container')['timestamp'].diff()
dtv = dt[dt.notna()].values

#print(deltas.shape)
#print(dtv.shape)
#print(max(deltas-dtv))

dtc = c.sort_values('created_at')['created_at'].diff()
dtcv = dtc[dtc.notna()].values

dtd = c.sort_values('deleted_at')['deleted_at'].diff()
dtdv = dtc[dtc.notna()].values

print(f'# of queue time diffs: {len(dtv)}')
print(f'# of creation time diffs: {len(dtcv)}')

e = dtwin.Exponential(offset=1.)
e = e.sample(1000)

fig, axs = plt.subplots(3)

axs[0].hist(e,bins=50,density=True,label='original distribution',alpha=0.5)
axs[0].hist(dtv,bins=50,density=True,label='queue',alpha=0.5)
axs[0].hist(dtcv,bins=50,density=True,label='creation',alpha=0.5)
axs[0].hist(dtdv,bins=50,density=True,label='destruction',alpha=0.5)

axs[0].set_title(f'Speed: {speed}')
axs[0].legend()

axs[1].plot(range(len(dtv)),dtv)
axs[1].set_title('queue time diffs vs. container')

axs[2].plot(range(len(dtcv)),dtcv)
axs[2].set_title('creation time diffs vs. container')

plt.tight_layout()
plt.show()
