import dtwin.random.gaussian_process as dgp
import matplotlib.pyplot as plt
import numpy as np

def RBF(sigma=1.,amp=1.):
    return lambda x: amp*np.exp(-((x[0]-x[1])**2)/(2*sigma))

def Exp(theta=1,amp=1.):
    return lambda x: amp*np.exp(-np.linalg.norm(x[0]-x[1])/theta)

def sin(T=1.,amp=1.):
    #return lambda x: np.zeros_like(x)
    return lambda x: amp*np.sin(x/T)

def vec_kernel(x,y):
    # x,y are 1D index arrays 
    x = x.reshape(-1,1)
    y = y.reshape(1,-1) 
    d = x-y
    return 0.2*np.exp(-0.5*d**2/40.)

mean   = sin(T=100,amp=2)
kernel = RBF(amp=0.2,sigma=40.)
#kernel = Exp(theta=1.,amp=1.)

print('initialising and generating past')
gp = dgp.CachedGaussianProcess1D(\
        name='gp',\
        mean=mean,\
        #kernel=kernel,\
        vectorised_kernel=vec_kernel,\
        cachesize=500,\
        sample_period=5.,\
        regular=False,\
        )

x0 = gp._cache[:,0]
y0 = gp._cache[:,1]

print('sampling future 1')
x1  = 500*np.random.rand(100)
x1  = x1[np.argsort(x1)]
y1  = gp._sample(x1)

print('sampling future 2')
x2  = np.arange(500,700,0.7)
y2  = gp._sample(x2)

print('sampling future 3')
x3  = np.arange(701,800,5.)
y3  = gp._sample(x3)

x   = np.block([x0,x1,x2,x3])
x   = np.arange(np.min(x),np.max(x),1.)
m   = mean(x)

print('plotting')

fig, ax = plt.subplots()
ax.plot(x0,y0,color='blue',marker='.',label='initial')
ax.plot(x1,y1,color='red',marker='.',label='future 1')
ax.plot(x2,y2,color='green',marker='.',label='future 2')
ax.plot(x3,y3,color='orange',marker='.',label='future 3')
ax.plot(x,m,color='black')
ax.legend()
plt.show()
