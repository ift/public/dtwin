'''
Just one container is created and sent through a tiny environment.
As the environment has no server running, nothing can happen after that
and the simulation should end after few steps.
'''

import dtwin as dtw
import asyncio

# this waypoint only exists because queues need to have a starting waypoint
w1 = dtw.Transit(
        name='Start',\
        )

q1 = dtw.FIFO(
        name = 'Queue',\
        prev_waypoint = w1,\
        distribution = dtw.Exponential(1.,offset=1),\
        )

model = dtw.Constant(name='Label',value=1)
w2 = dtw.Label(
        name = 'Transit',\
        prev_queue = q1,\
        model = model,\
        )

container = dtw.Container()
initial_event = dtw.TemporalEvent(action=lambda: q1.push(container),\
                                  time=0.,push=True)

aenv = dtw.AsyncEnvironment()
aenv.check_consistency(verbose=True)
asyncio.get_event_loop()\
       .run_until_complete(aenv.simulate(verbose_simulation=True))
