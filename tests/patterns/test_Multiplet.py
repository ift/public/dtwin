from dtwin.patterns import Multiplet

print('creating class foo, derived from Multiplet')
class foo(Multiplet):
    def __init__(self,name,value):
        self.name = name
        self.value = value
        print('')
        print(f'initialising object of class {self.__class__.__name__}:')
        print(f'--name: {self.name}, value: {str(self.value)}')
        print(f'--type: {str(type(self))}, class: {str(self.__class__)}')

    def __str__(self):
        s =  (f'--id: {str(self._instance_id)}, '
              f'name: {self.name}, value: {str(self.value)}')
        return s

    @classmethod
    def report_instances(cls):
        s = f'\nreporting instances of {cls.__name__}'
        print(s)
        print('-'*len(s))
        l = len(cls._instances.data)
        tmp = 'are' if l>1 else 'is'
        print(f'--there {tmp} {l} weak references to this class:')
        for _,inst in cls._instances.items():
            print(inst)
        print('--weak dictionary:')
        print(cls._instances.data)

def create_instances():
    a = foo('a_name',42)
    b = foo('b_name',3.14)
    c = foo('c_name',0)

    foo.report_instances()

    return [a,b,c]

def change_instances(tup):
    print('\nincrementing values by 1')
    for inst_name, inst in foo._instances.items():
        inst.value += 1
    # this is important, otherwise a strong reference is kept:
    del inst

    foo.report_instances()

    print('\ndeleting last instance')
    x = tup.pop()
    del x
    foo.report_instances()

    print('\ncreating dictionary (with strong references to objects):')
    dic = {inst_name: inst for inst_name,inst in foo._instances.items()}
    print(dic)

    print('\ndeleting last instance')
    x = tup.pop()
    del x
    foo.report_instances()

    print('\ndeleting dictionary (no more strong references to objects):')
    del dic
    foo.report_instances()

    print('\ndeleting last instance')
    x = tup.pop()
    del x
    foo.report_instances()

if __name__=='__main__':
    l = create_instances()
    change_instances(l)
